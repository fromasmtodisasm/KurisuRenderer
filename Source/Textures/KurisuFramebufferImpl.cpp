#include "KurisuFramebufferImpl.hpp"
#include "KurisuRootImpl.hpp"
#include "Surfaces/KurisuSurfaceImpl.hpp"

namespace {
  KawaiiFramebuffer::AttachmentMode getAttMode(size_t colorAtt_i)
  {
    Q_ASSERT(colorAtt_i < 8);

    switch(colorAtt_i)
      {
      case 0:
        return KawaiiFramebuffer::AttachmentMode::Color0;
      case 1:
        return KawaiiFramebuffer::AttachmentMode::Color1;
      case 2:
        return KawaiiFramebuffer::AttachmentMode::Color2;
      case 3:
        return KawaiiFramebuffer::AttachmentMode::Color3;
      case 4:
        return KawaiiFramebuffer::AttachmentMode::Color4;
      case 5:
        return KawaiiFramebuffer::AttachmentMode::Color5;
      case 6:
        return KawaiiFramebuffer::AttachmentMode::Color6;
      case 7:
        return KawaiiFramebuffer::AttachmentMode::Color7;
      }
    Q_UNREACHABLE();
  }
}

KurisuFramebufferImpl::KurisuFramebufferImpl(KawaiiFramebuffer *model):
  KawaiiFramebufferImpl(model)
{
  getModel()->setClipCorrection(vulkanToGlClipCorrectionOffscr);
}

KurisuFramebufferImpl::~KurisuFramebufferImpl()
{
  preDestruct();
}

VulkanBaseRenderpass &KurisuFramebufferImpl::getRp(size_t device_index) const
{
  return sceneRenderers[device_index]->rp;
}

std::vector<VkFramebuffer> KurisuFramebufferImpl::getFbos(size_t device_index) const
{
  return { sceneRenderers[device_index]->fbo.getFbo() };
}

void KurisuFramebufferImpl::attachTexture(KawaiiTextureImpl *tex, KawaiiFramebuffer::AttachmentMode mode, int layer)
{
  auto texHandle = static_cast<KurisuTextureHandle*>(tex->getHandle());

  auto el = activeAttachements.find(mode);
  if(el == activeAttachements.end())
    {
      cachedAttachements.clear();
      sceneRenderers.clear();
      el = activeAttachements.insert({mode, {texHandle, layer}}).first;
    } else if(el->second.first != texHandle || el->second.second != layer)
    {
      cachedAttachements.clear();
      sceneRenderers.clear();
      el->second = {texHandle, layer};
    }
}

void KurisuFramebufferImpl::attachDepthRenderbuffer([[maybe_unused]] const QSize &sz)
{
}

void KurisuFramebufferImpl::detachTexture(KawaiiFramebuffer::AttachmentMode mode)
{
  auto el = activeAttachements.find(mode);
  if(el != activeAttachements.end())
    {
      cachedAttachements.clear();
      sceneRenderers.clear();
      activeAttachements.erase(el);
    }
}

void KurisuFramebufferImpl::deleteRenderbuffers()
{
//  colorRenderbuffers.clear();
}

void KurisuFramebufferImpl::activate()
{
  if(cachedAttachements.empty())
    {
      size_t n = 1;

      for(const auto &i: activeAttachements)
        switch(i.first)
          {
          case KawaiiFramebuffer::AttachmentMode::Color1:
            n = std::max<size_t>(2, n);
            break;
          case KawaiiFramebuffer::AttachmentMode::Color2:
            n = std::max<size_t>(3, n);
            break;
          case KawaiiFramebuffer::AttachmentMode::Color3:
            n = std::max<size_t>(4, n);
            break;
          case KawaiiFramebuffer::AttachmentMode::Color4:
            n = std::max<size_t>(5, n);
            break;
          case KawaiiFramebuffer::AttachmentMode::Color5:
            n = std::max<size_t>(6, n);
            break;
          case KawaiiFramebuffer::AttachmentMode::Color6:
            n = std::max<size_t>(7, n);
            break;
          case KawaiiFramebuffer::AttachmentMode::Color7:
            n = std::max<size_t>(8, n);
            break;
          default: break;
          }
      ++n;

      cachedAttachements.resize(n);

      auto getColorTexHandle = [this] (size_t i) -> std::pair<KurisuTextureHandle*, int>  {
        auto el = activeAttachements.find(getAttMode(i));
        if(el == activeAttachements.end())
          return std::pair(nullptr, -1);
        return el->second;
      };


      cachedAttachements[0] = getColorTexHandle(0);
      cachedAttachements[1] = ([this] () -> std::pair<KurisuTextureHandle*, int> {
          auto el = activeAttachements.find(KawaiiFramebuffer::AttachmentMode::Depth);
          if(el == activeAttachements.end())
            el = activeAttachements.find(KawaiiFramebuffer::AttachmentMode::DepthStencil);
          if(el == activeAttachements.end())
            return {nullptr, -1};
          return el->second;
      })();

      for(size_t i = 2; i < n - 2; ++i)
        cachedAttachements[i] = getColorTexHandle(i - 1);

      sceneRenderers.reserve(root()->deviceCount());
      root()->forallDevices([this](VulkanDevice &dev, size_t device_index) {
        std::vector<std::pair<VulkanTexture*, int>> attachements(cachedAttachements.size(), {nullptr, -1});
        for(size_t i = 0; i < attachements.size(); ++i)
          if(cachedAttachements[i].first)
            attachements[i] = { &cachedAttachements[i].first->getTexture(device_index), cachedAttachements[i].second };

        sceneRenderers.emplace_back(std::make_unique<SceneRenderer>(dev, getModel()->getSize(), std::move(attachements), root()->getActiveSurface()->imgCount()));
      });
    }
  root()->setOffscreenTarget(this);
}

KurisuRootImpl *KurisuFramebufferImpl::root()
{
  return static_cast<KurisuRootImpl*>(getRoot());
}

KurisuFramebufferImpl::SceneRenderer::SceneRenderer(VulkanDevice &dev, const glm::uvec2 &sz, std::vector<std::pair<VulkanTexture*, int>> &&textures, size_t image_count):
  rp(dev, textures, image_count),
  fbo(rp, QSize(sz.x, sz.y), textures)
{
}
