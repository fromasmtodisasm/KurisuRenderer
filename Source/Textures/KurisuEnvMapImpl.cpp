#include <KawaiiRenderer/Textures/KawaiiTextureImpl.hpp>
#include "Surfaces/KurisuSurfaceImpl.hpp"
#include "KurisuRootImpl.hpp"

#include "KurisuEnvMapImpl.hpp"

KurisuEnvMapImpl::KurisuEnvMapImpl(KawaiiEnvMap *model):
  KawaiiEnvMapImpl(model),
  layer(0)
{
  model->setClipCorrectionMatrix(vulkanToGlClipCorrectionOffscr);
}

KurisuEnvMapImpl::~KurisuEnvMapImpl()
{
  preDestruct();
}

VulkanBaseRenderpass &KurisuEnvMapImpl::getRp(size_t device_index) const
{
  return sceneRenderers[device_index]->rp;
}

std::vector<VkFramebuffer> KurisuEnvMapImpl::getFbos(size_t device_index) const
{
  return { sceneRenderers[device_index]->fbo_layers[layer].getFbo() };
}

void KurisuEnvMapImpl::updateCache(KawaiiEnvMapImpl::DrawCache &c, const QRect &viewport)
{
  auto *cache = static_cast<CachedRenderpass*>(&c);
  cache->reset();
  cache->draw(this, viewport);
}

KawaiiEnvMapImpl::DrawCache *KurisuEnvMapImpl::createDrawCache(const QRect &viewport)
{
  CachedRenderpass *cache = new CachedRenderpass(root());
  cache->drawFunc = std::bind(&CachedRenderpass::exec, cache, this);
  cache->draw(this, viewport);

  return cache;
}

void KurisuEnvMapImpl::attachTexture(KawaiiTextureImpl *tex, KawaiiEnvMap::AttachmentMode mode, int layer)
{
  auto texHandle = static_cast<KurisuTextureHandle*>(tex->getHandle());

  auto el = activeAttachements.find(mode);
  if(el == activeAttachements.end())
    {
      cachedAttachements.clear();
      sceneRenderers.clear();
      el = activeAttachements.insert({mode, {texHandle, layer}}).first;
    } else if(el->second.first != texHandle || el->second.second != layer)
    {
      cachedAttachements.clear();
      sceneRenderers.clear();
      el->second = {texHandle, layer};
    }
}

void KurisuEnvMapImpl::attachDepthRenderbuffer(const QSize &)
{
}

void KurisuEnvMapImpl::detachTexture(KawaiiEnvMap::AttachmentMode mode)
{
  auto el = activeAttachements.find(mode);
  if(el != activeAttachements.end())
    {
      cachedAttachements.clear();
      sceneRenderers.clear();
      activeAttachements.erase(el);
    }
}

void KurisuEnvMapImpl::deleteRenderbuffers()
{
  //  colorRenderbuffers.clear();
}

void KurisuEnvMapImpl::setRenderLayer(size_t layer)
{
  checkSceneRenderers();
  this->layer = layer;
  root()->setOffscreenTarget(this);

  auto &cmd = root()->getActiveCommands();
  if(layer>0 && cmd.isPrimary())
    {
      cmd.endRenderpass();

      size_t device_index = root()->getActiveDevice();
      auto &rp = getRp(device_index);
      cmd.beginRenderpass(rp.getRenderpass(), getFbos(device_index), rp.getClearValues(), VK_SUBPASS_CONTENTS_INLINE);
    }
}

KurisuRootImpl *KurisuEnvMapImpl::root()
{
  return static_cast<KurisuRootImpl*>(getRoot());
}

void KurisuEnvMapImpl::checkSceneRenderers()
{
  if(cachedAttachements.empty())
    {
      cachedAttachements.resize(2);

      cachedAttachements[0] = ([this] () -> std::pair<KurisuTextureHandle*, int> {
                                 auto el = activeAttachements.find(KawaiiEnvMap::AttachmentMode::Color);
                                 if(el == activeAttachements.end())
                                   return {nullptr, -1};
                                 else
                                   return el->second;
                               })();

      cachedAttachements[1] = ([this] () -> std::pair<KurisuTextureHandle*, int> {
                                 auto el = activeAttachements.find(KawaiiEnvMap::AttachmentMode::Depth);
                                 if(el == activeAttachements.end())
                                   el = activeAttachements.find(KawaiiEnvMap::AttachmentMode::DepthStencil);
                                 if(el == activeAttachements.end())
                                   return {nullptr, -1};
                                 return el->second;
                               })();

      sceneRenderers.reserve(root()->deviceCount());
      root()->forallDevices([this](VulkanDevice &dev, size_t device_index) {
        std::vector<std::pair<VulkanTexture*, int>> attachements(cachedAttachements.size(), {nullptr, -1});
        for(size_t i = 0; i < attachements.size(); ++i)
          if(cachedAttachements[i].first)
            attachements[i] = { &cachedAttachements[i].first->getTexture(device_index), cachedAttachements[i].second };

        sceneRenderers.emplace_back(std::make_unique<SceneRenderer>(dev, std::move(attachements), root()->getActiveSurface()->imgCount()));
      });
    }
}

KurisuEnvMapImpl::CachedRenderpass::CachedRenderpass(KurisuRootImpl *root):
  cmd(root->deviceCount())
{
  for(size_t i = 0; i < cmd.size(); ++i)
    cmd[i] = std::make_unique<RenderingCommands>(root->getDevice(i), root->getActiveSurface()->imgCount(), (root->isNowOffscreen() || i != root->getConfig().getComposerGpu()));
}

void KurisuEnvMapImpl::CachedRenderpass::reset()
{
  for(const auto &i: cmd)
    i->reset();
}

void KurisuEnvMapImpl::CachedRenderpass::draw(KurisuEnvMapImpl *envmap, const QRect &viewport)
{
  envmap->checkSceneRenderers();
  for(size_t i = 0; i < cmd.size(); ++i)
    {
      const auto framebuffers = envmap->getFbos(i);
      auto &rp = envmap->getRp(i);

      cmd[i]->setViewport(viewport);
      cmd[i]->startRecording(rp.getRenderpass(), framebuffers, rp.getClearValues(), VK_SUBPASS_CONTENTS_INLINE, 0);
      envmap->root()->activateCommands(*cmd[i], i);
      try {
        envmap->drawScene();
        dirty = false;
      } catch(...)
      { dirty = true; }

      if(cmd[i]->isPrimary())
        cmd[i]->endRenderpass();
      cmd[i]->finishRecording();
    }
  envmap->root()->releaseCommands();

  this->viewport = viewport;
}

void KurisuEnvMapImpl::CachedRenderpass::exec(KurisuEnvMapImpl *envmap)
{
  const size_t i = envmap->root()->getActiveSurface()->getActiveGpu();
  if(cmd[i]->isPrimary())
    {
      auto &rp = envmap->getRp(i);
      rp.exec(*cmd[i], envmap->root()->getActiveSurface()->getImgIndex());
    }
}

namespace {
  std::vector<std::pair<VulkanTexture *, int>> prepareTextures(const std::vector<std::pair<VulkanTexture *, int>> &tex, int face)
  {
    std::vector<std::pair<VulkanTexture *, int>> textures(tex);
    for(auto &i: textures)
      {
        if(i.second < 0)
          i.second = 0;
        i.second = 6 * i.second + face;
      }
    return textures;
  }
}

namespace {
  QSize getFirstExtent(const std::vector<std::pair<VulkanTexture *, int>> &textures)
  {
    for(const auto &i: textures)
      if(i.first)
        return QSize(i.first->getExtent().width, i.first->getExtent().height);
    return QSize(1,1);
  }
}

KurisuEnvMapImpl::SceneRenderer::SceneRenderer(VulkanDevice &dev, std::vector<std::pair<VulkanTexture *, int> > &&textures, size_t image_count):
  rp(dev, textures, image_count),
  extent(getFirstExtent(textures)),
  fbo_layers({ VulkanFbo{ rp, extent, prepareTextures(textures, 0) },
{ rp, extent, prepareTextures(textures, 1) },
{ rp, extent, prepareTextures(textures, 2) },
{ rp, extent, prepareTextures(textures, 3) },
{ rp, extent, prepareTextures(textures, 4) },
{ rp, extent, prepareTextures(textures, 5) }})

{
}
