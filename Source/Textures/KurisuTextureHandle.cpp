#include "KurisuTextureHandle.hpp"
#include "KurisuRootImpl.hpp"

KurisuTextureHandle::KurisuTextureHandle(KurisuRootImpl &root_, KawaiiTexture *model):
  r(root_),
  w(0),
  h(0),
  d(0),
  storageType(StorageType::None),
  mipmap_needed(checkMipmapIsNeeded(model->getMinFilter()))
{
  textures.reserve(r.deviceCount());
  r.forallDevices([this, model] (VulkanDevice &dev) {
      textures.emplace_back(dev, true, false, false);
      textures.back().setMagFilter(model->getMagFilter());
      textures.back().setMinFilter(model->getMinFilter());

      textures.back().setWrapModeS(model->getWrapModeS());
      textures.back().setWrapModeT(model->getWrapModeT());
      textures.back().setWrapModeR(model->getWrapModeR());
    });
}

KurisuTextureHandle::~KurisuTextureHandle()
{
}

VulkanTexture &KurisuTextureHandle::getTexture(size_t device_index)
{
  return textures[device_index];
}

void KurisuTextureHandle::setData1D(int width, int layers, KawaiiTextureFormat fmt, const uint8_t *ptr)
{
  if(!checkStorageCompatibility(width, fmt))
    return invalidate();

  w = width;
  h = 0;
  d = 0;

  storageType = StorageType::Data1D;
  storageFmt = fmt;

  r.forallDevices([this, width, layers, fmt, ptr] (const VulkanDevice&, size_t i) {
      auto &tex = textures[i];
      tex.setExtent(width);
      if(layers > 0)
        tex.setLayers(1, layers);
      tex.setFormat_8bpc(fmt);
      tex.setBits(ptr);
      if(layers < 1)
        tex.createImageView(VkImageViewType::VK_IMAGE_VIEW_TYPE_1D);
      else
        tex.createImageView(VkImageViewType::VK_IMAGE_VIEW_TYPE_1D_ARRAY);
    });
}

void KurisuTextureHandle::setData1D(int width, int layers, KawaiiTextureFormat fmt, const float *ptr)
{
  if(!checkStorageCompatibility(width, fmt))
    return invalidate();

  w = width;
  h = 0;
  d = 0;

  storageType = StorageType::Data1D;
  storageFmt = fmt;

  r.forallDevices([this, width, layers, fmt, ptr] (const VulkanDevice&, size_t i) {
      auto &tex = textures[i];
      tex.setExtent(width);
      if(layers > 0)
        tex.setLayers(1, layers);
      tex.setFormat_32bpc(fmt);
      tex.setBits(ptr);
      if(layers < 1)
        tex.createImageView(VkImageViewType::VK_IMAGE_VIEW_TYPE_1D);
      else
        tex.createImageView(VkImageViewType::VK_IMAGE_VIEW_TYPE_1D_ARRAY);
    });
}

void KurisuTextureHandle::setData2D(int width, int heigh, int layers, KawaiiTextureFormat fmt, const uint8_t *ptr)
{
  if(!checkStorageCompatibility(width, heigh, fmt))
    return invalidate();

  w = width;
  h = heigh;
  d = 0;

  storageType = StorageType::Data2D;
  storageFmt = fmt;

  r.forallDevices([this, width, heigh, layers, fmt, ptr] (const VulkanDevice&, size_t i) {
      auto &tex = textures[i];
      tex.setExtent(width, heigh);
      if(layers > 0)
        tex.setLayers(1, layers);
      tex.setFormat_8bpc(fmt);
      tex.setBits(ptr);
      if(layers < 1)
        tex.createImageView(VkImageViewType::VK_IMAGE_VIEW_TYPE_2D);
      else
        tex.createImageView(VkImageViewType::VK_IMAGE_VIEW_TYPE_2D_ARRAY);
    });
}

void KurisuTextureHandle::setData2D(int width, int heigh, int layers, KawaiiTextureFormat fmt, const float *ptr)
{
  if(!checkStorageCompatibility(width, heigh, fmt))
    return invalidate();

  w = width;
  h = heigh;
  d = 0;

  storageType = StorageType::Data2D;
  storageFmt = fmt;

  r.forallDevices([this, width, heigh, layers, fmt, ptr] (const VulkanDevice&, size_t i) {
      auto &tex = textures[i];
      tex.setExtent(width, heigh);
      if(layers > 0)
        tex.setLayers(1, layers);
      tex.setFormat_32bpc(fmt);
      tex.setBits(ptr);
      if(layers < 1)
        tex.createImageView(VkImageViewType::VK_IMAGE_VIEW_TYPE_2D);
      else
        tex.createImageView(VkImageViewType::VK_IMAGE_VIEW_TYPE_2D_ARRAY);
    });
}

void KurisuTextureHandle::setData3D(int width, int heigh, int depth, KawaiiTextureFormat fmt, const uint8_t *ptr)
{
  if(!checkStorageCompatibility(width, heigh, depth, fmt))
    return invalidate();

  w = width;
  h = heigh;
  d = depth;

  storageType = StorageType::Data3D;
  storageFmt = fmt;

  r.forallDevices([this, width, heigh, depth, fmt, ptr] (const VulkanDevice&, size_t i) {
      auto &tex = textures[i];
      tex.setExtent(width, heigh, depth);
      tex.setFormat_8bpc(fmt);
      tex.setBits(ptr);
      tex.createImageView(VkImageViewType::VK_IMAGE_VIEW_TYPE_3D);
    });
}

void KurisuTextureHandle::setData3D(int width, int heigh, int depth, KawaiiTextureFormat fmt, const float *ptr)
{
  if(!checkStorageCompatibility(width, heigh, depth, fmt))
    return invalidate();

  w = width;
  h = heigh;
  d = depth;

  storageType = StorageType::Data3D;
  storageFmt = fmt;

  r.forallDevices([this, width, heigh, depth, fmt, ptr] (const VulkanDevice&, size_t i) {
      auto &tex = textures[i];
      tex.setExtent(width, heigh, depth);
      tex.setFormat_32bpc(fmt);
      tex.setBits(ptr);
      tex.createImageView(VkImageViewType::VK_IMAGE_VIEW_TYPE_3D);
    });
}

void KurisuTextureHandle::setDataCube(int width, int heigh, int layers, KawaiiTextureFormat fmt, const uint8_t *ptr)
{
  if(!checkStorageCompatibility(width, heigh, 6, fmt))
    return invalidate();

  w = width;
  h = heigh;
  d = 6;

  storageType = StorageType::Data3D;
  storageFmt = fmt;

  r.forallDevices([this, width, heigh, layers, fmt, ptr] (const VulkanDevice&, size_t i) {
      auto &tex = textures[i];
      tex.setExtent(width, heigh);
      if(layers > 0)
        tex.setLayers(1, 6 * layers);
      else
        tex.setLayers(1, 6);
      tex.setCubemapCompatible(true);
      tex.setFormat_8bpc(fmt);
      tex.setBits(ptr);
      if(layers < 1)
        tex.createImageView(VkImageViewType::VK_IMAGE_VIEW_TYPE_CUBE);
      else
        tex.createImageView(VkImageViewType::VK_IMAGE_VIEW_TYPE_CUBE_ARRAY);
    });
}

void KurisuTextureHandle::setDataCube(int width, int heigh, int layers, KawaiiTextureFormat fmt, const float *ptr)
{
  if(!checkStorageCompatibility(width, heigh, 6, fmt))
    return invalidate();

  w = width;
  h = heigh;
  d = 6;

  storageType = StorageType::Data3D;
  storageFmt = fmt;

  r.forallDevices([this, width, heigh, layers, fmt, ptr] (const VulkanDevice&, size_t i) {
      auto &tex = textures[i];
      tex.setExtent(width, heigh);
      if(layers > 0)
        tex.setLayers(1, 6 * layers);
      else
        tex.setLayers(1, 6);
      tex.setCubemapCompatible(true);
      tex.setFormat_32bpc(fmt);
      tex.setBits(ptr);
      if(layers < 1)
        tex.createImageView(VkImageViewType::VK_IMAGE_VIEW_TYPE_CUBE);
      else
        tex.createImageView(VkImageViewType::VK_IMAGE_VIEW_TYPE_CUBE_ARRAY);
    });
}

void KurisuTextureHandle::setMinFilter(KawaiiTextureFilter filter)
{
  bool mipmap = checkMipmapIsNeeded(filter);
  if(mipmap != mipmap_needed)
    {
      if(storageType == StorageType::None)
        mipmap_needed = mipmap;
      else
        return invalidate();
    }

  for(auto &i: textures)
    i.setMinFilter(filter);
}

void KurisuTextureHandle::setMagFilter(KawaiiTextureFilter filter)
{
  for(auto &i: textures)
    i.setMagFilter(filter);
}

void KurisuTextureHandle::setWrapModeS(KawaiiTextureWrapMode mode)
{
  for(auto &i: textures)
    i.setWrapModeS(mode);
}

void KurisuTextureHandle::setWrapModeT(KawaiiTextureWrapMode mode)
{
  for(auto &i: textures)
    i.setWrapModeT(mode);
}

void KurisuTextureHandle::setWrapModeR(KawaiiTextureWrapMode mode)
{
  for(auto &i: textures)
    i.setWrapModeR(mode);
}

void KurisuTextureHandle::setCompareOperation(KawaiiDepthCompareOperation op)
{
  for(auto &i: textures)
    i.setCompareOp(op);
}

bool KurisuTextureHandle::checkMipmapIsNeeded(KawaiiTextureFilter filter) const
{
  switch(filter)
    {
    case KawaiiTextureFilter::LinearMipmapLinear:
    case KawaiiTextureFilter::LinearMipmapNearest:
    case KawaiiTextureFilter::NearestMipmapLinear:
    case KawaiiTextureFilter::NearestMipmapNearest:
      return true;
      break;

    default:
      return false;
      break;
    }
}

bool KurisuTextureHandle::checkStorageCompatibility(int width, KawaiiTextureFormat fmt) const
{
  bool storageTypeMatches = (storageType == StorageType::Data1D);
  bool sizeMatches = (width == h);
  bool fmtMatches = (fmt == storageFmt);
  return (storageType == StorageType::None || (storageTypeMatches && sizeMatches && fmtMatches));
}

bool KurisuTextureHandle::checkStorageCompatibility(int width, int height, KawaiiTextureFormat fmt) const
{
  bool storageTypeMatches = (storageType == StorageType::Data2D);
  bool sizeMatches = (width == h && height == h);
  bool fmtMatches = (fmt == storageFmt);
  return (storageType == StorageType::None || (storageTypeMatches && sizeMatches && fmtMatches));
}

bool KurisuTextureHandle::checkStorageCompatibility(int width, int height, int depth, KawaiiTextureFormat fmt) const
{
  bool storageTypeMatches = (storageType == StorageType::Data3D);
  bool sizeMatches = (width == h && height == h && depth == d);
  bool fmtMatches = (fmt == storageFmt);
  return (storageType == StorageType::None || (storageTypeMatches && sizeMatches && fmtMatches));
}

