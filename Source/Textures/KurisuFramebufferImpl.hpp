#ifndef KURISUFRAMEBUFFERIMPL_HPP
#define KURISUFRAMEBUFFERIMPL_HPP

#include <KawaiiRenderer/Textures/KawaiiFramebufferImpl.hpp>
#include "KurisuOffscrRenderer.hpp"
#include "KurisuTextureHandle.hpp"

class KurisuRootImpl;

class KURISURENDERER_SHARED_EXPORT KurisuFramebufferImpl : public KawaiiFramebufferImpl,
    public KurisuOffscrRenderer
{
public:
  KurisuFramebufferImpl(KawaiiFramebuffer *model);
  ~KurisuFramebufferImpl();

  // KurisuOffscrRenderer interface
public:
  VulkanBaseRenderpass &getRp(size_t device_index) const override final;
  std::vector<VkFramebuffer> getFbos(size_t device_index) const override final;

  // KawaiiFramebufferImpl interface
private:
  void attachTexture(KawaiiTextureImpl *tex, KawaiiFramebuffer::AttachmentMode mode, int layer) override final;
  void attachDepthRenderbuffer(const QSize &sz) override final;
  void detachTexture(KawaiiFramebuffer::AttachmentMode mode) override final;
  void deleteRenderbuffers() override final;
  void activate() override final;



  //IMPLEMENT
private:
  struct SceneRenderer {
    VulkanBaseRenderpass rp;
    VulkanFbo fbo;

    SceneRenderer(VulkanDevice &dev, const glm::uvec2 &sz, std::vector<std::pair<VulkanTexture *, int> > &&textures, size_t image_count);
    SceneRenderer(const SceneRenderer&) = delete;
    SceneRenderer& operator=(const SceneRenderer&) = delete;
  };

  std::vector<std::unique_ptr<SceneRenderer>> sceneRenderers; //one per device

  std::unordered_map<KawaiiFramebuffer::AttachmentMode, std::pair<KurisuTextureHandle*, int>> activeAttachements;
//  std::vector<std::unique_ptr<KurisuTextureHandle>> colorRenderbuffers;
  std::vector<std::pair<KurisuTextureHandle*, int>> cachedAttachements;

  KurisuRootImpl *root();
};

#endif // KURISUFRAMEBUFFERIMPL_HPP
