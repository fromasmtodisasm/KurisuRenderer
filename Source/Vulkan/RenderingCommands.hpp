#ifndef RENDERINGCOMMANDS_HPP
#define RENDERINGCOMMANDS_HPP

#include "VulkanDescriptorPool.hpp"
#include "VulkanSwapchain.hpp"
#include "VulkanSemaphore.hpp"
#include "VulkanBuffer.hpp"
#include <unordered_set>
#include <QColor>
#include <vector>

//belongs to specific swapchain and readerpass

class VulkanPipelineLayout;

class RenderingCommands: public QObject
{
  Q_OBJECT
public:
  enum class State: uint8_t
  {
    Empty,
    WritingCommands,
    Compleate,
    Dirty
  };

  RenderingCommands(VulkanDevice &dev, size_t image_count=1, bool primary=false);
  RenderingCommands(const VulkanSwapchain &swapchain);
  ~RenderingCommands();

  void reset();

  void startRecording(VkRenderPass renderPass, const std::vector<VkFramebuffer> &fbo, const std::vector<VkClearValue> &clearValues, VkSubpassContents subpassContents, uint32_t subpass);
  void beginRenderpass(VkRenderPass renderPass, const std::vector<VkFramebuffer> &fbo, const std::vector<VkClearValue> &clearValues, VkSubpassContents subpassContents);

  void setViewport(const QRect &viewport);

  void bindGraphicsPipeline(VkPipeline pipeline);
  void bindVbos(uint32_t firstBinding, const std::vector<VulkanBuffer*> &buffers, const std::vector<VkDeviceSize> &offsets);
  void bindIndexBuffer(VulkanBuffer &buffer, VkIndexType indexType = VK_INDEX_TYPE_UINT32, VkDeviceSize offset = 0);

  void createDescriptorSet(uint64_t name, const std::vector<VulkanDescriptorPool::BufBinding> &bufBindings, const std::vector<VulkanDescriptorPool::ImgBinding> &imgBindings, VkDescriptorSetLayout layout, const std::unordered_map<VkDescriptorType, uint32_t> &descriptorCount);
  void bindDescriptorSet(uint64_t descrSetName, uint32_t descrSetBinding);
  void bindDescriptorSet(VkDescriptorSet descrSet, VkPipelineLayout pipelineLayout);

  void draw(uint32_t vertexCount, uint32_t instanceCount, uint32_t vertex_i, uint32_t instance_i);
  void drawIndexed(uint32_t vertexCount, uint32_t instanceCount, uint32_t vertex_i, uint32_t instance_i);
  void drawIndexedIndirect(VulkanBuffer &buffer, VkDeviceSize offset);

  void execute(RenderingCommands &commands);

  void nextSubpass(VkSubpassContents subpassContents);

  void endRenderpass();

  void finishRecording();

  void copyTexture(const VulkanTexture &from, const VulkanTexture &to, const std::vector<VkImageCopy> &regions);
  void copyTexture(const VulkanTexture &from, const VulkanTexture &to, VkImageAspectFlags imgAspect);

  void prepare(size_t image_index);
  bool isPrimary() const;
  State getState() const;
  bool isEmpty() const;

  bool hasBindedPipeline();

  void setDefaultPipelineLayout(VulkanPipelineLayout *value);

  void exec(uint32_t imageIndex, std::vector<VkSemaphore> &&waitSemaphores, std::vector<VkPipelineStageFlags> &&waitStages, std::vector<VkSemaphore> &&signalSemaphores, VkFence signalFence);

  inline const QRect& getViewport() const
  { return viewport; }

signals:
  void aboutToReset();



  //IMPLEMENT
private:
  VulkanDevice &dev;
  VkCommandPool commandPool;
  std::vector<VkCommandBuffer> commandBuffers;
  std::vector<std::unique_ptr<VulkanDescriptorPool>> descriptorPools;

  VulkanPipelineLayout *defaultPipelineLayout;

  std::vector<std::function<void(VkCommandBuffer,size_t)>> delayedCommands;

  std::vector<VulkanBuffer*> usedBufers;
  std::vector<RenderingCommands*> usedCommands;
  std::unordered_set<uint64_t> usedDescriptorSetNames;

  std::vector<VulkanSemaphore> resourcesReady;
  std::vector<bool> waitingForResources;

  QRect viewport;

  State state;
  VkPipeline bindedPipeline;
  bool primary;

  VkCommandBuffer getCommandBuffer(size_t image_index) const;

  void rewrite();
  void onDescriptorSetUpdated(uint64_t name);

  void check();
};

#endif // RENDERINGCOMMANDS_HPP
