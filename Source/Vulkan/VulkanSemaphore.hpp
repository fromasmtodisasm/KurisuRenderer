#ifndef VULKANSEMAPHORE_HPP
#define VULKANSEMAPHORE_HPP

#include "VulkanDevice.hpp"
#include <QtGlobal>
#include <optional>

class VulkanSemaphore
{
  VulkanSemaphore(const VulkanSemaphore&) = delete;
  VulkanSemaphore& operator= (const VulkanSemaphore&) = delete;
public:
  VulkanSemaphore(VulkanDevice &dev);
  VulkanSemaphore(VulkanSemaphore &&orig);

  ~VulkanSemaphore();

  VkSemaphore getSemaphore() const;

  std::optional<VulkanDevice::Handle> getOpaqueHandle() const;



  //IMPLEMENT
private:
  VulkanDevice &dev;
  VkSemaphore sem;
};

#endif // VULKANSEMAPHORE_HPP
