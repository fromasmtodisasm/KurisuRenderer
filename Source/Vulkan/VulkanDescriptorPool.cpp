#include "VulkanDescriptorPool.hpp"
#include <set>

bool operator<(const VkDescriptorBufferInfo &a, const VkDescriptorBufferInfo &b)
{
  if(a.buffer != b.buffer)
    return a.buffer < b.buffer;

  if(a.offset != b.offset)
    return a.offset < b.offset;

  if(a.range != b.range)
    return a.range < b.range;

  return false;
}

bool operator<(const VkDescriptorImageInfo &a, const VkDescriptorImageInfo &b)
{
  if(a.sampler != b.sampler)
    return a.sampler < b.sampler;

  if(a.imageView != b.imageView)
    return a.imageView < b.imageView;

  if(a.imageLayout != b.imageLayout)
    return a.imageLayout < b.imageLayout;

  return false;
}


VulkanDescriptorPool::VulkanDescriptorPool(VulkanDevice &dev, size_t frame_index, size_t frame_count):
  dev(dev),
  frame_index(frame_index),
  frame_count(frame_count),
  pool(VK_NULL_HANDLE),
  dirty(false)
{
}

VulkanDescriptorPool::~VulkanDescriptorPool()
{
  destroyPool();
}

namespace {
  template<typename T, typename KeyT>
  bool isMapsEq(const std::unordered_map<KeyT, T> &a, const std::unordered_map<KeyT, T> &b)
  {
    if(a.size() != b.size()) return false;

    for(const auto &i: a)
      if(auto el = b.find(i.first); el == b.end() || el->second != i.second)
        return false;

    return true;
  }

  template<typename T>
  bool isEq(const std::set<T> &b, const std::vector<typename std::list<T>::iterator> &a)
  {
    if(a.size() != b.size()) return false;

    for(const auto &i: a)
      if(b.count(*i) == 0)
        return false;

    return true;
  }
}

void VulkanDescriptorPool::createDescriptorSet(uint64_t name, const std::vector<VulkanDescriptorPool::BufBinding> &bufBindings, const std::vector<VulkanDescriptorPool::ImgBinding> &imgBindings, VkDescriptorSetLayout layout, const std::unordered_map<VkDescriptorType, uint32_t> &descriptorCount)
{
  Q_ASSERT(layout != VK_NULL_HANDLE);

  auto el = descrSets.find(name);

  auto rewrite = [this, name, &el, &bufBindings, &imgBindings, layout, &descriptorCount] {
      return updateDescriptorSet(name, el->second, bufBindings, imgBindings, layout, descriptorCount);
    };

  if(el == descrSets.end())
    {
      el = descrSets.insert({name, DescrSetInfo()}).first;
      return rewrite();
    }

  ++el->second.uses;

  if(el->second.layout != layout)
    return rewrite();

  if(!isMapsEq(el->second.descriptorCount, descriptorCount))
    return rewrite();

  std::set<VkDescriptorBufferInfo> buffers;
  for(const auto &i: bufBindings)
    if(i.buf && !i.buf->empty())
      buffers.insert(*i.buf->descrInfo(parent(), frame_index, frame_count));
  if(!isEq(buffers, el->second.usedBufs))
    return rewrite();

  std::set<VkDescriptorImageInfo> images;
  for(const auto &i: imgBindings)
    images.insert(i.descrImg);
  if(!isEq(images, el->second.usedImgs))
    return rewrite();

  for(const auto &i: bufBindings)
    if(i.buf && !i.buf->empty()
       && std::find(usedBuffers.cbegin(), usedBuffers.cend(), i.buf) == usedBuffers.cend())
      usedBuffers.push_back(i.buf);
}

VkDescriptorSet VulkanDescriptorPool::getDescriptorSet(uint64_t name)
{
  if(dirty)
    recreateDescriptorSets();

  auto el = descrSets.find(name);
  return (el!=descrSets.end())? el->second.descrSet: VK_NULL_HANDLE;
}

void VulkanDescriptorPool::releaseDescriptorSet(uint64_t name)
{
  auto el = descrSets.find(name);
  if(el != descrSets.end())
    if(el->second.uses > 0)
      --el->second.uses;
}

void VulkanDescriptorPool::forgetUsedBuffers()
{
  usedBuffers.clear();
}

void VulkanDescriptorPool::useDescriptorSet(uint64_t name)
{
  auto el = descrSets.find(name);
  if(el != descrSets.end())
    el->second.uses++;
}

namespace {
  bool eq(const VkDescriptorBufferInfo *a, const VkDescriptorBufferInfo &b)
  {
    return a->buffer == b.buffer
        && a->offset == b.offset
        && a->range == b.range;
  }
}

void VulkanDescriptorPool::collectGarbage()
{
  std::vector<VkDescriptorSet> orphanSets;
  for(auto el = descrSets.begin(); el != descrSets.end();)
    {
      if(el->second.uses > 0)
        ++el;
      else
        {
          if(el->second.descrSet)
            orphanSets.push_back(el->second.descrSet);
          el = descrSets.erase(el);
        }
    }
  if(!orphanSets.empty())
    vkFreeDescriptorSets(dev.vkDev, pool, orphanSets.size(), orphanSets.data());

  for(auto i = usedBuffers.cbegin(); i != usedBuffers.cend(); )
    {
      auto *descrInfo = (*i)->descrInfo(parent(), frame_index, frame_count);
      if(std::find_if(descrBufInfo.cbegin(), descrBufInfo.cend(), std::bind(eq, descrInfo, std::placeholders::_1)) != descrBufInfo.cend())
        ++i;
      else
        i = usedBuffers.erase(i);
    }

  for(auto i = descrBufInfo.begin(); i != descrBufInfo.end();)
    {
      bool used = false;
      for(const auto &descrSet: descrSets)
        if(std::find(descrSet.second.usedBufs.cbegin(), descrSet.second.usedBufs.cend(), i) != descrSet.second.usedBufs.cend())
          {
            used = true;
            break;
          }
      if(Q_UNLIKELY(!used))
        i = descrBufInfo.erase(i);
      else
        ++i;
    }

  for(auto i = descrImgInfo.begin(); i != descrImgInfo.end();)
    {
      bool used = false;
      for(const auto &descrSet: descrSets)
        if(std::find(descrSet.second.usedImgs.cbegin(), descrSet.second.usedImgs.cend(), i) != descrSet.second.usedImgs.cend())
          {
            used = true;
            break;
          }
      if(Q_UNLIKELY(!used))
        i = descrImgInfo.erase(i);
      else
        ++i;
    }
}

void VulkanDescriptorPool::prepareResources(size_t frameIndex)
{
  for(auto *i: usedBuffers)
    i->syncDetachedBuffer(parent(), frameIndex);
}

void VulkanDescriptorPool::destroyPool()
{
  if(pool)
    {
      dev.waitIdle();
      vkDestroyDescriptorPool(dev.vkDev, pool, nullptr);
      pool = VK_NULL_HANDLE;
    }
}

void VulkanDescriptorPool::recreateDescriptorSets()
{
  if(!dirty) return;

  if(pool)
    emit recreated();

  destroyPool();

  if(!descrSets.empty())
    {
//      dev.submitTransferCommands();
      std::unordered_map<VkDescriptorType, VkDescriptorPoolSize> poolSizes;
      for(const auto &descrSet: descrSets)
        for(const auto &i: descrSet.second.descriptorCount)
          {
            auto el = poolSizes.find(i.first);
            if(el == poolSizes.end())
              el = poolSizes.insert({i.first, VkDescriptorPoolSize {i.first, i.second} }).first;
            else
              el->second.descriptorCount += i.second;
          }

      std::vector<VkDescriptorPoolSize> poolSizesVec;
      poolSizesVec.reserve(poolSizes.size());
      for(const auto &i: poolSizes)
        poolSizesVec.push_back(i.second);

      const VkDescriptorPoolCreateInfo poolCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT,
        .maxSets = static_cast<uint32_t>(descrSets.size()),
        .poolSizeCount = static_cast<uint32_t>(poolSizesVec.size()),
        .pPoolSizes = poolSizesVec.data()
      };

      VkResult res = vkCreateDescriptorPool(dev.vkDev, &poolCreateInfo, nullptr, &pool);

      std::vector<VkDescriptorSetLayout> setLayouts;
      setLayouts.reserve(descrSets.size());
      for(const auto &i: descrSets)
        setLayouts.push_back(i.second.layout);

      const VkDescriptorSetAllocateInfo allocateInfo = {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        .descriptorPool = pool,
        .descriptorSetCount = static_cast<uint32_t>(descrSets.size()),
        .pSetLayouts = setLayouts.data()
      };
      std::vector<VkDescriptorSet> createdSets(descrSets.size());
      res = vkAllocateDescriptorSets(dev.vkDev, &allocateInfo, createdSets.data());
      if(res != VK_SUCCESS)
        throw std::runtime_error("Can not allocate descriptor sets");

      size_t i = 0;
      std::vector<VkWriteDescriptorSet> writes;
      for(auto &descrSet: descrSets)
        {
          descrSet.second.descrSet = createdSets[i++];
          for(auto &i: descrSet.second.writes)
            i.dstSet = descrSet.second.descrSet;
          if(!descrSet.second.writes.empty())
            writes.insert(writes.end(), descrSet.second.writes.cbegin(), descrSet.second.writes.cend());
//          vkUpdateDescriptorSets(dev.vkDev, descrSet.second.writes.size(), descrSet.second.writes.data(), 0, nullptr);
        }
      if(!writes.empty())
        vkUpdateDescriptorSets(dev.vkDev, writes.size(), writes.data(), 0, nullptr);
    }
  dirty = false;
}

void VulkanDescriptorPool::updateDescriptorSet(uint64_t name, VulkanDescriptorPool::DescrSetInfo &descrSet, const std::vector<VulkanDescriptorPool::BufBinding> &bufBindings, const std::vector<VulkanDescriptorPool::ImgBinding> &imgBindings, VkDescriptorSetLayout layout, const std::unordered_map<VkDescriptorType, uint32_t> &descriptorCount)
{
  descrSet.writes.clear();
  for(const auto &i: descrSet.usedBufs)
    descrBufInfo.erase(i);
  descrSet.usedBufs.clear();
  for(const auto &i: descrSet.usedImgs)
    descrImgInfo.erase(i);
  descrSet.usedImgs.clear();

  descrSet.layout = layout;
  descrSet.descriptorCount = descriptorCount;

  for(const auto &i: bufBindings)
    if(i.buf && !i.buf->empty())
      {
        auto *descrInfo = i.buf->descrInfo(parent(), frame_index, frame_count);
        if(std::find(usedBuffers.cbegin(), usedBuffers.cend(), i.buf) == usedBuffers.cend())
          usedBuffers.push_back(i.buf);

        auto el = descrBufInfo.insert(descrBufInfo.end(), *descrInfo);
        descrSet.usedBufs.push_back(el);
        const VkWriteDescriptorSet descriptorWrite = {
          .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
          .dstBinding = i.bindingPoint,
          .descriptorCount = 1,
          .descriptorType = i.descriptorType,
          .pBufferInfo = &*el,
        };
        descrSet.writes.push_back(descriptorWrite);
      }

  for(const auto &i: imgBindings)
    {
      auto el = descrImgInfo.insert(descrImgInfo.end(), i.descrImg);
      descrSet.usedImgs.push_back(el);
      const VkWriteDescriptorSet descriptorWrite = {
        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        .dstBinding = i.bindingPoint,
        .descriptorCount = 1,
        .descriptorType = i.descriptorType,
        .pImageInfo = &descrImgInfo.back(),
      };
      descrSet.writes.push_back(descriptorWrite);
    }

  if(!dirty && descrSet.descrSet)
    {
      for(auto &i: descrSet.writes)
        i.dstSet = descrSet.descrSet;

      emit descriptorSetUpdated(name);
      vkUpdateDescriptorSets(dev.vkDev, descrSet.writes.size(), descrSet.writes.data(), 0, nullptr);
    } else
    dirty = true;
}
