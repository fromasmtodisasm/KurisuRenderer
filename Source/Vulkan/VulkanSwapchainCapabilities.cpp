#include "VulkanSwapchainCapabilities.hpp"
#include <Kawaii3D/KawaiiConfig.hpp>

namespace {
  VkSurfaceCapabilitiesKHR getPhysicalDeviceSurfaceCapabilities(VkPhysicalDevice dev, VkSurfaceKHR sfc)
  {
    VkSurfaceCapabilitiesKHR result;
    if(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(dev, sfc, &result) != VK_SUCCESS)
      throw std::runtime_error("failed to get physical device surface capabilities");
    return result;
  }

  std::vector<VkSurfaceFormatKHR> getAvailableFormats(VkPhysicalDevice dev, VkSurfaceKHR sfc)
  {
    std::vector<VkSurfaceFormatKHR> result;

    uint32_t formatCount;
    vkGetPhysicalDeviceSurfaceFormatsKHR(dev, sfc, &formatCount, nullptr);

    if (formatCount != 0)
      {
        result.resize(formatCount);
        vkGetPhysicalDeviceSurfaceFormatsKHR(dev, sfc, &formatCount, result.data());
      }

    return result;
  }

  std::vector<VkPresentModeKHR> getAvailablePresentModes(VkPhysicalDevice dev, VkSurfaceKHR sfc)
  {
    std::vector<VkPresentModeKHR> result;

    uint32_t presentModeCount;
    vkGetPhysicalDeviceSurfacePresentModesKHR(dev, sfc, &presentModeCount, nullptr);

    if (presentModeCount != 0)
      {
        result.resize(presentModeCount);
        vkGetPhysicalDeviceSurfacePresentModesKHR(dev, sfc, &presentModeCount, result.data());
      }

    return result;
  }
}

VulkanSwapchainCapabilities::VulkanSwapchainCapabilities(const VulkanDevice &dev, VkSurfaceKHR sfc):
  phDev(dev.phDev),
  capabilities(getPhysicalDeviceSurfaceCapabilities(dev.phDev, sfc)),
  formats(getAvailableFormats(dev.phDev, sfc)),
  presentModes(getAvailablePresentModes(dev.phDev, sfc))
{
}

bool VulkanSwapchainCapabilities::adequate() const
{
  return !formats.empty() && !presentModes.empty();
}

VkSurfaceFormatKHR VulkanSwapchainCapabilities::chooseSwapSurfaceFormat() const
{
  for (const auto& availableFormat : formats)
    if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
      return availableFormat;

  return formats[0];
}

VkPresentModeKHR VulkanSwapchainCapabilities::chooseSwapPresentMode() const
{
  std::vector<VkPresentModeKHR> preferredModes;
  if (KawaiiConfig::getInstance().getPreferredSfcFormat().swapInterval() != 0)
    preferredModes = { VK_PRESENT_MODE_FIFO_KHR };
  else
    preferredModes = { VK_PRESENT_MODE_FIFO_RELAXED_KHR, VK_PRESENT_MODE_MAILBOX_KHR, VK_PRESENT_MODE_IMMEDIATE_KHR };

  for (const auto &preferredMode: preferredModes)
    for (const auto& availablePresentMode : presentModes)
      if (availablePresentMode == preferredMode)
        return availablePresentMode;

  return VK_PRESENT_MODE_FIFO_KHR;
}

VkExtent2D VulkanSwapchainCapabilities::chooseSwapExtent(const QSize &sz) const
{
  const VkExtent2D result = {
    .width = static_cast<uint32_t>(sz.width()),
    .height = static_cast<uint32_t>(sz.height()),
  };
  return result;

//  VkExtent2D actualExtent = {
//    .width = static_cast<uint32_t>(sz.width()),
//    .height = static_cast<uint32_t>(sz.height())
//  };

//  actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
//  actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));

//  return actualExtent;
}
