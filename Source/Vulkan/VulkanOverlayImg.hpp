#ifndef VULKANOVERLAYIMG_HPP
#define VULKANOVERLAYIMG_HPP

#include "VulkanTexture.hpp"
#include "VulkanSemaphore.hpp"
#include <QOpenGLFramebufferObject>
#include <QOpenGLPaintDevice>
#include <QOffscreenSurface>
#include <QOpenGLContext>

#include <memory>

class VulkanOverlayImg
{
public:
  VulkanOverlayImg(VulkanDevice &dev, const QSize &sz, bool zeroCopy = false);
  ~VulkanOverlayImg();

  void init();
  void destroy();

  void resize(const QSize &sz);

  QPaintDevice& qPaintDev();
  VkImageView getVkImageView() const;
  const VkDescriptorImageInfo* descriptorInfo() const;
  VkFormat getVkFormat() const;

  VkSemaphore getCompleteSemaphore() const;

  void flush();
  void wait();

  bool isZeroCopy() const;



  //IMPLEMENT
private:
  VulkanSemaphore completeRendering;

  VulkanTexture texture;

  QOpenGLContext glCtx;
  QOffscreenSurface sfc;

  QSize size;

  void (*glGenSemaphoresEXT) (GLsizei n, GLuint* semaphores);
  void (*glSignalSemaphoreEXT) (GLuint semaphore, GLuint numBufferBarriers, const GLuint *buffers, GLuint numTextureBarriers, const GLuint *textures, const GLenum *dstLayouts);
  void (*glWaitSemaphoreEXT) (GLuint semaphore, GLuint numBufferBarriers, const GLuint *buffers, GLuint numTextureBarriers, const GLuint *textures, const GLenum *srcLayouts);
  void (*glDeleteSemaphoresEXT) (GLsizei n, const GLuint *semaphores);

  void (*glCreateMemoryObjectsEXT) (GLsizei n, GLuint *memoryObjects);

  void (*glImportSemHandleEXT) (GLuint semaphore, GLenum handleType, VulkanDevice::Handle fd);
  void (*glImportMemHandleEXT) (GLuint memory, GLuint64 size, GLenum handleType, VulkanDevice::Handle fd);

  void (*glTexStorageMem2DEXT) (GLenum target, GLsizei levels, GLenum internalFormat, GLsizei width, GLsizei height, GLuint memory, GLuint64 offset);

#ifdef Q_OS_UNIX
  inline static constexpr GLenum opaqueHandleType = GL_HANDLE_TYPE_OPAQUE_FD_EXT;
#elif defined(Q_OS_WINDOWS)
  inline static constexpr GLenum opaqueHandleType = GL_HANDLE_TYPE_OPAQUE_WIN32_KMT_EXT;
#endif

  GLuint fbo;
  GLuint mem;
  GLuint tex;
  GLuint renderbuff;
  GLuint glComplete;
  bool zeroCopy;

  std::unique_ptr<QOpenGLPaintDevice> paintDev;

  void destroyGlFbo();

  void initTexture(const QSize &sz);
  void initFbo(const QSize &sz);

  void makeCurrent();
};

#endif // VULKANOVERLAYIMG_HPP
