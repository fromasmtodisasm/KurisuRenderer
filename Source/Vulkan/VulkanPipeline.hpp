#ifndef VULKANPIPELINE_HPP
#define VULKANPIPELINE_HPP

#include "VulkanDevice.hpp"
#include "VulkanShader.hpp"
#include "VulkanRenderpass.hpp"
#include "VulkanPipelineLayout.hpp"
#include <QSurfaceFormat>
#include <QRectF>
#include <sib_utils/TupleHash.hpp>
#include <sib_utils/QSizeHash.hpp>

class RenderingCommands;

class VulkanPipeline
{
  VulkanPipeline(const VulkanPipeline&) = delete;
  VulkanPipeline& operator=(const VulkanPipeline&) = delete;

public:
  enum class Topology {
    Points,
    Lines,
    LineStrip,
    Triangles,
    TriangleStrip,
    TriangleFan,
    LinesWithAdjacency,
    LineStripWithAdjacency,
    TrianglesWithAdjacency,
    TriangleStripWithAdjacency,
    Patches
  };

  VulkanPipeline(const VulkanDevice &dev, VulkanPipelineLayout &layout);
  ~VulkanPipeline();

  Topology getTopology() const;
  void setTopology(Topology topology);

  void setSurfaceFormat(const QSurfaceFormat &format);

  void setShaderStages(const std::vector<const VulkanShader*> &stages);
  void addShaderStage(const VulkanShader *shader);
  void updateShaderStage(const VulkanShader *shader);
  void removeShaderStage(const VulkanShader *shader);

  void setVertexInputState(const VkPipelineVertexInputStateCreateInfo &state);

  void setDepthTestEnabled(bool value);
  void setDepthWriteEnabled(bool value);

  void setColorBlending(const VkPipelineColorBlendAttachmentState &colorBlending);

  VkPipeline getPipeline(RenderingCommands *commands, VkRenderPass renderpass, uint32_t subpass);

  const VulkanDevice &getDevice() const;

private:
  const VulkanDevice &devInfo;

  VkPipelineVertexInputStateCreateInfo vertices;

  VkPipelineInputAssemblyStateCreateInfo inputAssembly;

  VkPipelineRasterizationStateCreateInfo rasterizer;

  VkPipelineMultisampleStateCreateInfo multisampling;

  VkPipelineDepthStencilStateCreateInfo depthStencil;

  VkPipelineColorBlendAttachmentState colorBlendAttachment;
  VkPipelineColorBlendStateCreateInfo colorBlending;

  struct SpecializedPipeline
  {
    VkPipeline vkPipeline;
    VkRenderPass renderpass;
    QRectF viewport;
    uint32_t subpass;
    bool dirty;
  };
  std::unordered_map<RenderingCommands*, SpecializedPipeline> pipelines;

  std::vector<VkPipelineShaderStageCreateInfo> shaderStagesInfo;
  std::vector<const VulkanShader*> shaderStagesSrc;

  VulkanPipelineLayout &layout;
  VulkanPipelineLayout::OnInvalidateFuncs::const_iterator onLayoutInvalidate;

  void recreate(VkPipeline &pipeline, const QRectF &viewport, VkRenderPass renderpass, uint32_t subpass);

  void markDirty();
};

#endif // VULKANPIPELINE_HPP
