#include "SingleTimeCommands.hpp"
#include <QtConcurrent>

SingleTimeCommands::SingleTimeCommands(const VulkanDevice &dev_info):
  dev(dev_info.vkDev),
  pool(dev_info.getTransferCmdPool()),
  commited(false)
{
}

SingleTimeCommands::~SingleTimeCommands()
{
  if(!commited)
    write();
}

void SingleTimeCommands::copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, std::vector<VkBufferCopy> &&regions)
{
  auto *rg = new std::vector<VkBufferCopy>(std::move(regions));
  commands.push_back([srcBuffer, dstBuffer, rg] (VkCommandBuffer buf) {
      vkCmdCopyBuffer(buf, srcBuffer, dstBuffer, rg->size(), rg->data());
      delete rg;
    });
}

void SingleTimeCommands::transitionImageLayout(VkImage image, VkImageSubresourceRange subresourceRange, VkImageLayout oldLayout, VkImageLayout newLayout, uint32_t srcQFamily, uint32_t dstQFamily)
{
  VkPipelineStageFlags sourceStage = 0;
  VkPipelineStageFlags destinationStage = 0;

  VkImageMemoryBarrier barrier = {
    .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
    .oldLayout = oldLayout,
    .newLayout = newLayout,
    .srcQueueFamilyIndex = srcQFamily,
    .dstQueueFamilyIndex = dstQFamily,
    .image = image,
    .subresourceRange = subresourceRange
  };

  if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED)
    {
      barrier.srcAccessMask = 0;
      sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
    } else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
    {
      barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
      sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    } else if(oldLayout == VK_IMAGE_LAYOUT_PREINITIALIZED)
    {
      barrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT;
      sourceStage = VK_PIPELINE_STAGE_HOST_BIT;
    } else if (oldLayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL)
    {
      barrier.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_MEMORY_WRITE_BIT;
      sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
    } else if (oldLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
    {
      barrier.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
      sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
    } else if(oldLayout != newLayout)
    throw std::invalid_argument("unsupported layout transition!");

  if (newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
    {
      barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
      destinationStage = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;

#     ifndef Q_OS_ANDROID
    } else if (newLayout == VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_OPTIMAL)
    {
      barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
      destinationStage = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
#     endif

    } else if (newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
      barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
      destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    } else if (newLayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL)
    {
      barrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
      destinationStage = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
    } else if (newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
      barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
      destinationStage = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
    } else if(oldLayout != newLayout)
    throw std::invalid_argument("unsupported layout transition!");

  commands.push_back([sourceStage, destinationStage, barrier] (VkCommandBuffer buf) {
      vkCmdPipelineBarrier (
            buf,
            sourceStage, destinationStage,
            0,
            0, nullptr,
            0, nullptr,
            1, &barrier );
    });
}

void SingleTimeCommands::copyBufferToImage(VkBuffer buffer, VkImage image, VkDeviceSize bufOffset, VkOffset3D offset, VkExtent3D size, VkImageSubresourceLayers imgSubresourceLayers)
{
  commands.push_back([offset, bufOffset, size, imgSubresourceLayers, buffer, image] (VkCommandBuffer buf) {
      const VkBufferImageCopy region {
        .bufferOffset = bufOffset,
        .imageSubresource = imgSubresourceLayers,
        .imageOffset = offset,
        .imageExtent = size
      };

      vkCmdCopyBufferToImage(buf, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);
    });
}

QFuture<void> SingleTimeCommands::write()
{
  struct CmdBufOrphan {
    VulkanCommandPool *pool;
    std::vector<std::function<void(VkCommandBuffer)>> commands;
  } cmdBufOrphan = {&pool, std::move(commands)};

  commited = true;

  return QtConcurrent::run([cmdBufOrphan] {
      cmdBufOrphan.pool->writeCommands(cmdBufOrphan.commands);
    });
}
