#ifndef VULKANSWAPCHAINCAPABILITIES_HPP
#define VULKANSWAPCHAINCAPABILITIES_HPP

#include "VulkanDevice.hpp"
#include <QSize>

class VulkanSwapchainCapabilities
{
  VkPhysicalDevice phDev;
public:
  const VkSurfaceCapabilitiesKHR capabilities;
  const std::vector<VkSurfaceFormatKHR> formats;
  const std::vector<VkPresentModeKHR> presentModes;
  const std::optional<uint32_t> presentFamily;

  VulkanSwapchainCapabilities(const VulkanDevice &dev, VkSurfaceKHR sfc);

  bool adequate() const;

  VkSurfaceFormatKHR chooseSwapSurfaceFormat() const;
  VkPresentModeKHR chooseSwapPresentMode() const;
  VkExtent2D chooseSwapExtent(const QSize &sz) const;
};

#endif // VULKANSWAPCHAINCAPABILITIES_HPP
