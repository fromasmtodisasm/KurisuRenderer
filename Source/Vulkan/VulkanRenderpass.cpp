#include "VulkanRenderpass.hpp"
#include <QtConcurrent>

VulkanRenderpass::VulkanRenderpass(VulkanSwapchain &swapchain):
  VulkanBaseRenderpass(swapchain.getDevice(), swapchain.getImageFormat(), swapchain.getImageViews().size()),
  viewport(QPoint(0,0), QSize(swapchain.width(), swapchain.height())),
  swapchain(swapchain),
  imageAvailableSemaphore(swapchain.getDevice())
{
  getInternalAttachment(0).finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
}

VulkanRenderpass::~VulkanRenderpass()
{
  destroyResources();
}

bool VulkanRenderpass::prepareFrame()
{
  bool outdatedSwapchain = false;

  imageIndex = swapchain.acquireNextImage(imageAvailableSemaphore.getSemaphore(), &outdatedSwapchain);
  if(outdatedSwapchain)
    {
      imageIndex = std::nullopt;
      return false;
    }
  return true;
}

const std::optional<uint32_t> &VulkanRenderpass::getImageIndex() const
{
  return imageIndex;
}

VulkanSwapchain &VulkanRenderpass::getSwapchain()
{
  return swapchain;
}

void VulkanRenderpass::setViewport(const QRect &val)
{
  if(viewport != val)
    {
      viewport = val;
      if(!dirty)
        recreateFramebuffers();
      else
        getRenderpass();
    }
}

const QRect &VulkanRenderpass::getViewport()
{
  return viewport;
}

const std::vector<VkFramebuffer> &VulkanRenderpass::getFramebuffers() const
{
  return framebuffers;
}

void VulkanRenderpass::recreateFramebuffers()
{
  if(!framebuffers.empty()) {
      destroyFramebuffers();
      createFramebuffers();
    }
}

void VulkanRenderpass::exec(RenderingCommands &commands, std::vector<Dependency> &&dependencies)
{
  std::vector<VkSemaphore> waitSemaphores;
  std::vector<VkPipelineStageFlags> waitStages;
  waitSemaphores.reserve(dependencies.size() + 1);
  waitStages.reserve(dependencies.size() + 1);

  waitSemaphores.push_back(imageAvailableSemaphore.getSemaphore());
  waitStages.push_back(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT);

  for(auto &i: dependencies)
    {
      waitStages.push_back(i.stage);
      waitSemaphores.push_back(i.sem);
    }

  const uint32_t imgIndex = *imageIndex;

  wait(imgIndex);
  vkResetFences(dev.vkDev, 1, &fences[imgIndex]);
  commands.prepare(imgIndex);

  VkSemaphore sem = renderingCompleatedSem.getSemaphore();
  {
    QMutexLocker l(&dev.graphicsQueueM);

    commands.exec(imgIndex, std::move(waitSemaphores), std::move(waitStages), {sem}, fences[imgIndex]);
    swapchain.present(sem, imgIndex);
  }

  imageIndex = std::nullopt;
}

void VulkanRenderpass::createFramebuffers()
{
  const auto &images = swapchain.getImageViews();
  VkImageView depthImgView = swapchain.getDepthImgView();

  framebuffers.reserve(images.size());
  for(size_t i = 0; i < images.size(); ++i)
    {
      std::vector<VkImageView> attachements = { images[i], depthImgView };
      attachements.reserve(attachements.size() + advAttachmentImages.size());
      attachements.insert(attachements.end(), advAttachmentImages.cbegin(), advAttachmentImages.cend());

      VkFramebufferCreateInfo createInfo {
        VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO, // sType
        nullptr, // pNext
        0, // flags
        renderPass, // renderPass
        static_cast<uint32_t>(attachements.size()), // attachmentCount
        attachements.data(), // pAttachments
        swapchain.width(), // width
        swapchain.height(), // height
        1 // layers
      };
      VkFramebuffer framebuffer;
      if (vkCreateFramebuffer(dev.vkDev, &createInfo, nullptr, &framebuffer) != VK_SUCCESS)
        throw std::runtime_error("failed to create framebuffer!");
      framebuffers.push_back(framebuffer);
    }
}

void VulkanRenderpass::createAdvResources()
{
  createFramebuffers();
}

void VulkanRenderpass::destroyFramebuffers()
{
  dev.waitIdle();
  for(const auto &i: framebuffers)
    vkDestroyFramebuffer(dev.vkDev, i, nullptr);
  framebuffers.clear();
}

void VulkanRenderpass::destroyAdvResources()
{
  destroyFramebuffers();
}
