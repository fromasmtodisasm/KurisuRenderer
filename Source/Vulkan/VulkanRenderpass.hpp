#ifndef VULKANRENDERPASS_HPP
#define VULKANRENDERPASS_HPP

#include "RenderingCommands.hpp"
#include "VulkanBaseRenderpass.hpp"
#include <QColor>
#include <vector>

class VulkanRenderpass: public VulkanBaseRenderpass
{
public:
  struct Dependency
  {
    VkSemaphore sem;
    VkPipelineStageFlags stage;
  };

  VulkanRenderpass(VulkanSwapchain &swapchain);
  ~VulkanRenderpass();

  bool prepareFrame();
  const std::optional<uint32_t> &getImageIndex() const;

  VulkanSwapchain& getSwapchain();
  void setViewport(const QRect &val);
  const QRect& getViewport();

  const std::vector<VkFramebuffer>& getFramebuffers() const;

  void recreateFramebuffers();

  void exec(RenderingCommands &commands, std::vector<Dependency> &&dependencies);

private:
  QRect viewport;

  VulkanSwapchain &swapchain;

  std::vector<VkFramebuffer> framebuffers;

  VulkanSemaphore imageAvailableSemaphore;

  std::optional<uint32_t> imageIndex;

  void createFramebuffers();
  void createAdvResources() override final;

  void destroyFramebuffers();
  void destroyAdvResources() override final;
};

#endif // VULKANRENDERPASS_HPP
