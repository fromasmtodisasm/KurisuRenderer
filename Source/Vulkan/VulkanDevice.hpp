#ifndef VULKANDEVICE_HPP
#define VULKANDEVICE_HPP

#include "VulkanCommandPool.hpp"
#include <string>
#ifdef Q_OS_WINDOWS
#include <windows.h>
#include <vulkan/vulkan_win32.h>
#endif

class VulkanStagingBuffer;

class VulkanDevice
{
  VulkanDevice(const VulkanDevice&) = delete;
  VulkanDevice& operator=(const VulkanDevice&) = delete;

public:
#ifdef Q_OS_UNIX
  using Handle = int; //fd
#elif defined(Q_OS_WINDOWS)
  using Handle = HANDLE; //HANDLE
#else
  using Handle = std::nullopt_t; //stub
#endif

  struct QueueFamilyIndices
  {
    std::optional<uint32_t> graphicsFamily;
    std::optional<uint32_t> transferFamily;
    uint32_t transferQCount;
    uint32_t familiesCount;
  };

  const QueueFamilyIndices queueFamilies;

  const VkDevice vkDev;
  const VkPhysicalDevice phDev;
  const VkPhysicalDeviceProperties properties;
  const VkQueue graphicsQueue;
  const VkQueue transferQueue;
  const VkPhysicalDeviceMemoryProperties memProperties;
  const VkFormat depthFormat;
  const VkFormat depthStencilFormat;
  const VkPipelineCache pipelineCache;

  QMutex graphicsQueueM;

  VulkanDevice(const QueueFamilyIndices &queueFamilies, const VkDevice vkDev, const VkPhysicalDevice phDev);
  ~VulkanDevice();

  VulkanDevice(VulkanDevice &&orig);

  std::optional<uint32_t> getPresentQueueFamily(VkSurfaceKHR sfc) const;
  std::optional<VkQueue> getPresentQueue(VkSurfaceKHR sfc) const;

  uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties) const;

  //<memory, is_device_local>
  std::pair<VkDeviceMemory, bool> allocateMemory(const VkMemoryRequirements &memReq, VkMemoryPropertyFlags properties, VkExternalMemoryHandleTypeFlags exportTypes = 0, std::optional<VkMemoryDedicatedAllocateInfo> &&dedicatedAllocateInfo = std::nullopt) const;

  VkDeviceMemory importMemory(const VkMemoryRequirements &memReq, VkMemoryPropertyFlags properties, Handle handle, VkExternalMemoryHandleTypeFlagBits type, VkExternalMemoryHandleTypeFlags exportTypes, std::optional<VkMemoryDedicatedAllocateInfo> &&dedicatedAllocateInfo);

  std::optional<Handle> getMemoryHandle(VkDeviceMemory mem, VkExternalMemoryHandleTypeFlagBits handleType);

  VulkanStagingBuffer& getStagingBuffer(const void *data, size_t dataSize);
  VulkanStagingBuffer& getStagingBuffer(const std::function<void(void*)> &dataSrc, size_t dataSize);

  VulkanCommandPool& getTransferCmdPool() const;
  bool submitTransferCommands();
  bool submitTransferCommandsAsync(VkSemaphore finished = VK_NULL_HANDLE);

  std::optional<Handle> getSemHandle(VkSemaphore sem, VkExternalSemaphoreHandleTypeFlagBits handleType);

  void waitIdle() const;

private:
#ifdef Q_OS_UNIX
  PFN_vkGetSemaphoreFdKHR fnGetSemaphoreFd;
  PFN_vkGetMemoryFdKHR fnGetMemoryFd;
  PFN_vkGetMemoryFdPropertiesKHR fnGetMemoryFdProperties;
#elif defined(Q_OS_WINDOWS)
  PFN_vkGetSemaphoreWin32HandleKHR fnGetSemaphoreHandle;
  PFN_vkGetMemoryWin32HandleKHR fnGetMemoryHandle;
#endif

  std::vector<VulkanStagingBuffer*> cachedStagingBuffers;
  decltype(cachedStagingBuffers)::iterator cachedStagingBuffer_i;

  std::unique_ptr<VulkanCommandPool> transferCmdPool;

  bool owner;

  VkQueue getQueue(const std::optional<uint32_t> &family, uint32_t index) const;

  VkFormat getSupportedDepthStencilFormat(const std::initializer_list<VkFormat> &formats);
};

#endif // VULKANDEVICE_HPP
