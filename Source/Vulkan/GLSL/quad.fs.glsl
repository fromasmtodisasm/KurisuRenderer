#version 450 core

//https://www.saschawillems.de/blog/2018/07/19/vulkan-input-attachments-and-sub-passes/

layout (input_attachment_index = 0, set = 0, binding = 0) uniform subpassInput inputTex;

layout (location = 0) out vec4 outColor;

void main(void)
{
    outColor = subpassLoad(inputTex);
}
