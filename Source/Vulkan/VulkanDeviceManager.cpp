#include "VulkanDeviceManager.hpp"
#include <QProcessEnvironment>
#include <iostream>
#include <variant>
#include <vector>
#include <limits>

namespace {
  std::vector<const char*> get_cstrs(const std::vector<std::string> &strs)
  {
    std::vector<const char*> result;
    result.reserve(strs.size());
    for(const auto &i: strs)
      result.push_back(i.c_str());
    return result;
  }

  std::vector<const char*> get_cstrs(const QByteArrayList &strs)
  {
    std::vector<const char*> result;
    result.reserve(strs.size());
    for(const auto &i: strs)
      result.push_back(i.data());
    return result;
  }
}

VulkanDeviceManager::VulkanDeviceManager(VkInstance instance, const QByteArrayList &layers, const std::vector<std::string> &device_extensions):
  inst(instance),

  layers(layers),
  extensions(device_extensions),

  layers_cstr(get_cstrs(this->layers)),
  extensions_cstr(get_cstrs(this->extensions))
{
  createDevices();
}

VulkanDeviceManager::~VulkanDeviceManager()
{
  devices.clear();
}

VulkanDevice &VulkanDeviceManager::device(size_t i)
{
  return devices[i];
}

const VulkanDevice &VulkanDeviceManager::device(size_t i) const
{
  return devices[i];
}

const std::vector<VulkanDevice> &VulkanDeviceManager::allDevices() const
{
  return devices;
}

VulkanDevice::QueueFamilyIndices VulkanDeviceManager::findQueueFamilies(VkPhysicalDevice device)
{
  VulkanDevice::QueueFamilyIndices indices;

  vkGetPhysicalDeviceQueueFamilyProperties(device, &indices.familiesCount, nullptr);

  std::vector<VkQueueFamilyProperties> queueFamilies(indices.familiesCount);
  vkGetPhysicalDeviceQueueFamilyProperties(device, &indices.familiesCount, queueFamilies.data());

  indices.transferQCount = 0;

  for(uint32_t i = 0; i < queueFamilies.size(); ++i)
    if (queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
      {
        indices.graphicsFamily = i;
        break;
      }

  for(uint32_t i = 0; i < queueFamilies.size(); ++i)
    if (queueFamilies[i].queueFlags & VK_QUEUE_TRANSFER_BIT && queueFamilies[i].queueCount > indices.transferQCount)
      {
        indices.transferFamily = i;
        indices.transferQCount = queueFamilies[i].queueCount;
      }
  return indices;
}

void VulkanDeviceManager::createDevices()
{
  static const std::vector<std::variant<size_t, std::string>> deviceFilters = ([] {
      const QStringList lst = QProcessEnvironment::systemEnvironment().value("SIB3D_GPU_FILTER", "0").split(',');

      std::vector<std::variant<size_t, std::string>> result;
      for(const auto &i: lst)
      {
        bool isNumber;
        const auto num = i.toULong(&isNumber);
        if(isNumber)
          result.push_back(num);
        else
          result.push_back(i.toStdString());
      }
      return result;
    })();

  uint32_t deviceCount = 0;

  // Query the number of devices
  if(vkEnumeratePhysicalDevices(inst, &deviceCount, nullptr) != VK_SUCCESS)
    return;

  std::vector<VkPhysicalDevice> phDevs(deviceCount);

  if(vkEnumeratePhysicalDevices(inst, &deviceCount, phDevs.data()) != VK_SUCCESS)
    return;

  std::cout << "Using devices:" << std::endl;

  for(size_t p = 0; p < phDevs.size(); ++p)
    {
      const auto &phDev = phDevs[p];

      const VulkanDevice::QueueFamilyIndices indices = findQueueFamilies(phDev);

      if(!indices.graphicsFamily.has_value())
        continue;

      VkPhysicalDeviceProperties props;
      vkGetPhysicalDeviceProperties(phDev, &props);

      if (!deviceFilters.empty())
        {
          bool match = false;
          for (const auto &i: deviceFilters)
            {
              if (std::holds_alternative<size_t>(i))
                match += p == std::get<size_t>(i);
              else if (std::holds_alternative<std::string>(i))
                if (std::string(props.deviceName).find(std::get<std::string>(i)) != std::string::npos)
                  {
                    match = true;
                    break;
                  }
            }
          if(!match) continue;
        }

      const std::array<float, 2> queuePriorities = {1, 1};

      std::array<VkDeviceQueueCreateInfo, 2> queueCreateInfo = {
        VkDeviceQueueCreateInfo {
          .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
          .queueFamilyIndex = indices.graphicsFamily.value(),
          .queueCount = 1,
          .pQueuePriorities = queuePriorities.data()
        },
        VkDeviceQueueCreateInfo {
          .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
          .queueFamilyIndex = indices.transferFamily.value(),
          .queueCount = 1,
          .pQueuePriorities = queuePriorities.data()
        }
      };
      bool singleQueue = (indices.graphicsFamily == indices.transferFamily);
      if(singleQueue && indices.transferQCount > 1)
        ++queueCreateInfo[0].queueCount;

      const VkPhysicalDeviceFeatures deviceFeatures = {
        .imageCubeArray = VK_TRUE,
        .samplerAnisotropy = VK_TRUE
      };

      const VkDeviceCreateInfo devCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
        .queueCreateInfoCount = static_cast<uint32_t>(singleQueue? 1: queueCreateInfo.size()),
        .pQueueCreateInfos = queueCreateInfo.data(),
        .enabledLayerCount = static_cast<uint32_t>(layers_cstr.size()),
        .ppEnabledLayerNames = layers_cstr.data(),
        .enabledExtensionCount = static_cast<uint32_t>(extensions_cstr.size()),
        .ppEnabledExtensionNames = extensions_cstr.data(),
        .pEnabledFeatures = &deviceFeatures
      };

      VkDevice dev;
      if(vkCreateDevice(phDev, &devCreateInfo, nullptr, &dev) == VK_SUCCESS)
        {
          devices.emplace_back(indices, dev, phDev);
          std::cout << '\t' << props.deviceName;
        }
    }

  std::cout << std::endl;
}
