#ifndef VULKANSWAPCHAIN_HPP
#define VULKANSWAPCHAIN_HPP

#include "VulkanTexture.hpp"
#include <QWindow>

class VulkanSwapchain
{
public:
  VulkanSwapchain(QWindow *wnd, VkSurfaceKHR sfc, VulkanDevice &dev);
  ~VulkanSwapchain();

  VkFormat getImageFormat() const;
  VulkanDevice &getDevice() const;

  bool checkExtent() const;
  void recreate();

  const std::vector<VkImageView> &getImageViews() const;

  VkImageView getDepthImgView();

  uint32_t width() const;
  uint32_t height() const;

  size_t getAsyncFrames() const;

  //returns the acquired image index
  uint32_t acquireNextImage(VkSemaphore semaphore, bool *outdatedSwapchain);

  void present(VkSemaphore waitSemaphore, uint32_t imageIndex);

private:
  VkSwapchainKHR swapchain;
  VkSurfaceKHR sfc;
  VkFormat imageFormat;
  VulkanDevice &dev;
  QWindow *wnd;

  VulkanTexture depthImg;

  std::vector<VkImage> images;
  std::vector<VkImageView> imageViews;

  VkQueue presentQ;

  size_t asyncFrames;

  uint32_t w;
  uint32_t h;

  void createDepthImg();
};

#endif // VULKANSWAPCHAIN_HPP
