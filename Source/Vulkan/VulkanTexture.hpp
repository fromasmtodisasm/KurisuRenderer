#ifndef VULKANTEXTURE_HPP
#define VULKANTEXTURE_HPP

#include <KawaiiRenderer/Textures/KawaiiTextureFormat.hpp>
#include <Kawaii3D/Textures/KawaiiTextureFilter.hpp>
#include <Kawaii3D/Textures/KawaiiTextureWrapMode.hpp>
#include <Kawaii3D/Textures/KawaiiDepthCompareOperation.hpp>

#include "VulkanStagingBuffer.hpp"
#include "VulkanCommandPool.hpp"

#include <QOpenGLFunctions>
#include <QtGlobal>

class VulkanTexture
{
  bool isInExclusiveMode() const;

  VulkanTexture(const VulkanTexture &) = delete;
  VulkanTexture& operator=(const VulkanTexture &) = delete;

public:
  class Layer2D {
    friend class  ::VulkanTexture;

    Layer2D(const Layer2D &orig) = delete;
    Layer2D& operator=(const Layer2D &orig) = delete;
  public:
    Layer2D(VulkanTexture &tex, int layer, VkImageAspectFlags usage);
    Layer2D(VulkanTexture &tex, int layer);
    ~Layer2D();

    Layer2D(Layer2D &&orig);
    Layer2D& operator=(Layer2D &&orig);

    inline VkImageView getImgView() const
    { return imgView; }


  private:
    VulkanTexture *tex;
    VkImageView imgView;

    void destroy();
  };

#ifdef Q_OS_LINUX
# ifdef Q_OS_ANDROID
  inline static constexpr VkExternalMemoryHandleTypeFlags imgExternalMemoryHandleTypes = VK_EXTERNAL_MEMORY_HANDLE_TYPE_ANDROID_HARDWARE_BUFFER_BIT_ANDROID | VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT;
  inline static constexpr VkExternalMemoryHandleTypeFlagBits imgMultiGpuMemoryHandleType = VK_EXTERNAL_MEMORY_HANDLE_TYPE_ANDROID_HARDWARE_BUFFER_BIT_ANDROID;
  inline static constexpr VkExternalMemoryHandleTypeFlagBits imgSameGpuMemoryHandleType = VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT;
# else
  inline static constexpr VkExternalMemoryHandleTypeFlags imgExternalMemoryHandleTypes = VK_EXTERNAL_MEMORY_HANDLE_TYPE_DMA_BUF_BIT_EXT | VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT;
  inline static constexpr VkExternalMemoryHandleTypeFlagBits imgMultiGpuMemoryHandleType = VK_EXTERNAL_MEMORY_HANDLE_TYPE_DMA_BUF_BIT_EXT;
  inline static constexpr VkExternalMemoryHandleTypeFlagBits imgSameGpuMemoryHandleType = VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT;
# endif
#elif defined(Q_OS_WINDOWS)
  inline static constexpr VkExternalMemoryHandleTypeFlags imgExternalMemoryHandleTypes = VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT;
  inline static constexpr VkExternalMemoryHandleTypeFlagBits imgMultiGpuMemoryHandleType = VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT;
  inline static constexpr VkExternalMemoryHandleTypeFlagBits imgSameGpuMemoryHandleType = VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT;
#elif defined(Q_OS_UNIX)
  inline static constexpr VkExternalMemoryHandleTypeFlags imgExternalMemoryHandleTypes = VK_EXTERNAL_MEMORY_HANDLE_TYPE_HOST_ALLOCATION_BIT_EXT | VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT;
  inline static constexpr VkExternalMemoryHandleTypeFlagBits imgMultiGpuMemoryHandleType = VK_EXTERNAL_MEMORY_HANDLE_TYPE_HOST_ALLOCATION_BIT_EXT;
  inline static constexpr VkExternalMemoryHandleTypeFlagBits imgSameGpuMemoryHandleType = VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT;
#endif

  VulkanTexture(VulkanDevice &dev, bool allowedSampler = true, bool allowedAttachement = false, bool allowInputAttachment = false);
  ~VulkanTexture();

  VulkanTexture(VulkanTexture &&orig);
  VulkanTexture& operator=(VulkanTexture &&orig);

  void setFormat_8bpc(KawaiiTextureFormat fmt);
  void setFormat_32bpc(KawaiiTextureFormat fmt);

  void setExtent(uint32_t width);
  void setExtent(uint32_t width, uint32_t height);
  void setExtent(uint32_t width, uint32_t height, uint32_t depth);

  void setLayers(uint32_t mipMapLayers, uint32_t arrayLayers);
  void setCompareOp(KawaiiDepthCompareOperation op);
  void setCubemapCompatible(bool compatible);
  bool isCubemapCompatible() const;

  void setLinear(bool linear);
  bool isLinear() const;

  void importImage(const VulkanTexture &texture);
  void pointToImage(const VulkanTexture &texture);

  void createImage(const void *imageData, bool sharedMemory=false);
  void setBits(const void *imageData);
  void copyBitsFromGl(QOpenGLFunctions &gl);
  void createImageView(VkImageViewType type);

  void setMinFilter(KawaiiTextureFilter filter);
  void setMagFilter(KawaiiTextureFilter filter);

  void setWrapModeS(KawaiiTextureWrapMode mode);
  void setWrapModeT(KawaiiTextureWrapMode mode);
  void setWrapModeR(KawaiiTextureWrapMode mode);

  void createSampler();

  void setLayout(VkImageLayout layout);

  VkDescriptorImageInfo *descriptorInfo();
  const VkDescriptorImageInfo *descriptorInfo() const;

  VkImage getVkImage() const;
  VkImageView getVkImageView() const;

  size_t getAllocatedSize() const;
  uint32_t bytesPerPixel() const;

  const VulkanDevice& getDevice() const;

  VkFormat getVkFormat() const;
  KawaiiTextureFormat getKawaiiFormat() const;

  const VkExtent3D& getExtent() const;

  VkImageAspectFlags getAspectFlagsUsage() const;

  std::optional<VulkanDevice::Handle> getMemHandle(VkExternalMemoryHandleTypeFlagBits type) const;

private:
  std::vector<Layer2D*> layers2d;

  std::array<uint32_t, 2> queueFalilyIndices;

  VkImageCreateInfo imageInfo;

  KawaiiTextureFilter minFilter;
  KawaiiTextureFilter magFilter;

  KawaiiTextureWrapMode uWrap;
  KawaiiTextureWrapMode vWrap;
  KawaiiTextureWrapMode wWrap;

  KawaiiDepthCompareOperation compareOp;

  VulkanDevice *dev;
  VulkanCommandPool::OnSubmitElement onTransferCmdSubmit;

  VkImage img;
  VkDeviceMemory mem;
  VkImageView imgView;
  VkImageViewType imgViewType;
  VkSampler sampler;

  VkDeviceSize memorySize;
  VkDeviceSize allocatedSize;
  VkDeviceSize memoryAlignment;

  QFuture<void> writeTransferCmdTask;

  VkDescriptorImageInfo descrInfo;

  VkImageAspectFlags imgAspectFlagsFull;
  VkImageAspectFlags imgAspectFlagsUsage;

  VkImageLayout finalLayout;

  bool allowedSampler;
  bool allowedAttachement;
  bool allowedInputAttachement;
  bool sharedMemory;

  bool dirty;
  bool dirty_sampler;

  bool waitsForTransfer;
  bool ownsMem;

  void importImage(VulkanDevice::Handle handle, VkExternalMemoryHandleTypeFlagBits type);

  void prepareCreateImage(VkMemoryRequirements &memReq, VkExternalMemoryHandleTypeFlags exportTypes);

  void destroyImgView();
  void destroyImg();
};

#endif // VULKANTEXTURE_HPP
