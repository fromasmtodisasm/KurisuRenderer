#include "VulkanOverlayImg.hpp"
#include "SingleTimeCommands.hpp"
//#include "RenderingCommands.hpp"
//#include "VulkanFbo.hpp"
//#include "VulkanOffscreenRenderpass.hpp"

#include <Kawaii3D/KawaiiConfig.hpp>
//#include <QPaintEngine>
#include <QOpenGLFunctions>
#include <QOpenGLExtraFunctions>
#include <QDebug>

namespace
{
#if !defined(Q_OS_ANDROID) && !defined(Q_OS_IOS)
  constexpr GLenum gl_debug_source_api = GL_DEBUG_SOURCE_API;
  constexpr GLenum gl_debug_source_window_system = GL_DEBUG_SOURCE_WINDOW_SYSTEM;
  constexpr GLenum gl_debug_source_shader_compiler = GL_DEBUG_SOURCE_SHADER_COMPILER;
  constexpr GLenum gl_debug_source_third_party = GL_DEBUG_SOURCE_THIRD_PARTY;
  constexpr GLenum gl_debug_source_application = GL_DEBUG_SOURCE_APPLICATION;
  constexpr GLenum gl_debug_source_other = GL_DEBUG_SOURCE_OTHER;

  constexpr GLenum gl_debug_type_error = GL_DEBUG_TYPE_ERROR;
  constexpr GLenum gl_debug_type_deprecated_behavior = GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR;
  constexpr GLenum gl_debug_type_undefined_behavior = GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR;
  constexpr GLenum gl_debug_type_portability = GL_DEBUG_TYPE_PORTABILITY;
  constexpr GLenum gl_debug_type_performance = GL_DEBUG_TYPE_PERFORMANCE;
  constexpr GLenum gl_debug_type_marker = GL_DEBUG_TYPE_MARKER;
  constexpr GLenum gl_debug_type_push_group = GL_DEBUG_TYPE_PUSH_GROUP;
  constexpr GLenum gl_debug_type_pop_group = GL_DEBUG_TYPE_POP_GROUP;
  constexpr GLenum gl_debug_type_other = GL_DEBUG_TYPE_OTHER;

  constexpr GLenum gl_debug_severity_high = GL_DEBUG_SEVERITY_HIGH;
  constexpr GLenum gl_debug_severity_medium = GL_DEBUG_SEVERITY_MEDIUM;

  constexpr GLenum gl_debug_output = GL_DEBUG_OUTPUT;
  constexpr GLenum gl_debug_output_synchronous = GL_DEBUG_OUTPUT_SYNCHRONOUS;
#else
  constexpr GLenum gl_debug_source_api = GL_DEBUG_SOURCE_API_KHR;
  constexpr GLenum gl_debug_source_window_system = GL_DEBUG_SOURCE_WINDOW_SYSTEM_KHR;
  constexpr GLenum gl_debug_source_shader_compiler = GL_DEBUG_SOURCE_SHADER_COMPILER_KHR;
  constexpr GLenum gl_debug_source_third_party = GL_DEBUG_SOURCE_THIRD_PARTY_KHR;
  constexpr GLenum gl_debug_source_application = GL_DEBUG_SOURCE_APPLICATION_KHR;
  constexpr GLenum gl_debug_source_other = GL_DEBUG_SOURCE_OTHER_KHR;

  constexpr GLenum gl_debug_type_error = GL_DEBUG_TYPE_ERROR_KHR;
  constexpr GLenum gl_debug_type_deprecated_behavior = GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_KHR;
  constexpr GLenum gl_debug_type_undefined_behavior = GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_KHR;
  constexpr GLenum gl_debug_type_portability = GL_DEBUG_TYPE_PORTABILITY_KHR;
  constexpr GLenum gl_debug_type_performance = GL_DEBUG_TYPE_PERFORMANCE_KHR;
  constexpr GLenum gl_debug_type_marker = GL_DEBUG_TYPE_MARKER_KHR;
  constexpr GLenum gl_debug_type_push_group = GL_DEBUG_TYPE_PUSH_GROUP_KHR;
  constexpr GLenum gl_debug_type_pop_group = GL_DEBUG_TYPE_POP_GROUP_KHR;
  constexpr GLenum gl_debug_type_other = GL_DEBUG_TYPE_OTHER_KHR;

  constexpr GLenum gl_debug_severity_high = GL_DEBUG_SEVERITY_HIGH_KHR;
  constexpr GLenum gl_debug_severity_medium = GL_DEBUG_SEVERITY_MEDIUM_KHR;

  constexpr GLenum gl_debug_output = GL_DEBUG_OUTPUT_KHR;
  constexpr GLenum gl_debug_output_synchronous = GL_DEBUG_OUTPUT_SYNCHRONOUS_KHR;
#endif

  template<size_t N>
  constexpr QLatin1String latin1Str(const char (&str)[N])
  {
    return str[N-1] == '\0'? QLatin1String(str, N-1): QLatin1String(str, N);
  }

  void receiveGLDebug(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *)
  {
    const uint16_t dbg = KawaiiConfig::getInstance().getDebugLevel();

    static const std::unordered_map<GLenum, QLatin1String> sourceNames = {
      {gl_debug_source_api,             latin1Str("calls to the OpenGL API")},
      {gl_debug_source_window_system,   latin1Str("calls to a window-system API")},
      {gl_debug_source_shader_compiler, latin1Str("compiler for a shading language")},
      {gl_debug_source_third_party,     latin1Str("application associated with OpenGL")},
      {gl_debug_source_application,     latin1Str("user of this application")},
      {gl_debug_source_other,           latin1Str("Unknown")}
    };

    static const std::unordered_map<GLenum, QLatin1String> typeNames = {
      {gl_debug_type_error,               latin1Str("Error")},
      {gl_debug_type_deprecated_behavior, latin1Str("Warning (deprecated)")},
      {gl_debug_type_undefined_behavior,  latin1Str("Warning (undefined behavior)")},
      {gl_debug_type_portability,         latin1Str("Warning (functionality is not portable)")},
      {gl_debug_type_performance,         latin1Str("Warning (performance issues)")},
      {gl_debug_type_marker,              latin1Str("Command stream annotation")},
      {gl_debug_type_push_group,          latin1Str("Group pushed")},
      {gl_debug_type_pop_group,           latin1Str("Group poped")},
      {gl_debug_type_other,               latin1Str("Unknown")}
    };

    if(length < 0 || strlen(reinterpret_cast<const char*>(message)) < static_cast<size_t>(length))
      return;

    switch(severity)
      {
      case gl_debug_severity_high:
        qCritical().setAutoInsertSpaces(true);
        qCritical() << "OpenGL debug message #" << id << '{';
        qCritical() << "\tFrom " << sourceNames.at(source);
        qCritical() << "\tType: " << typeNames.at(type);
        qCritical() << "\tText: " << reinterpret_cast<const char*>(message);
        qCritical().nospace() << '}';
        break;

      case gl_debug_severity_medium:
        if(dbg < 2) break;
        qWarning() << "OpenGL debug message #" << id << '{';
        qWarning() << "\tFrom " << sourceNames.at(source);
        qWarning() << "\tType: " << typeNames.at(type);
        qWarning() << "\tText: " << reinterpret_cast<const char*>(message);
        qWarning().nospace() << '}';
        break;

      default:
        if(dbg < 3) break;
        qInfo() << "OpenGL debug message #" << id << '{';
        qInfo() << "\tFrom " << sourceNames.at(source);
        qInfo() << "\tType: " << typeNames.at(type);
        qInfo() << "\tText: " << reinterpret_cast<const char*>(message);
        qInfo().nospace() << '}';
        break;
      }
  }

  const QSurfaceFormat sfcFmt = ([]{
      QSurfaceFormat sfcFmt = KawaiiConfig::getInstance().getPreferredSfcFormat();
      sfcFmt.setRedBufferSize(0);
      sfcFmt.setGreenBufferSize(0);
      sfcFmt.setBlueBufferSize(0);
      sfcFmt.setAlphaBufferSize(0);
    #if !defined(Q_OS_ANDROID) && !defined(Q_OS_IOS)
      sfcFmt.setProfile(QSurfaceFormat::CoreProfile);
    # endif
      return sfcFmt;
    })();

//  class VulkanPaintEngine: public QPaintEngine
//  {
//    VulkanOffscreenRenderpass &renderpass;
//    RenderingCommands cmd;

//    // QPaintEngine interface
//  public:
//    VulkanPaintEngine(VulkanOffscreenRenderpass &renderpass):
//      renderpass(renderpass),
//      cmd(renderpass.getDevice(), 1, true)
//    {}

//    bool begin(QPaintDevice *dev) override
//    {
//      cmd.reset();
//      cmd.startRecording(renderpass.getRenderpass(), renderpass.getFramebuffers(), QRect(0,0, dev->width(),dev->height()));
//      return true;
//    }

//    bool end() override
//    {
//      return renderpass.exec(cmd);
//    }

//    void updateState(const QPaintEngineState &state) override
//    {
//    }

//    void drawPolygon(const QPointF *points, int pointCount, PolygonDrawMode mode) override
//    {
//    }

//    void drawPixmap(const QRectF &r, const QPixmap &pm, const QRectF &sr) override
//    {

//    }

//    Type type() const override
//    {
//      return QPaintEngine::Type::User;
//    }
//  };

//  class VulkanPaintDevice: public QPaintDevice
//  {
//    VulkanOffscreenRenderpass rp;
//    std::unique_ptr<VulkanFbo> fbo;
//    std::unique_ptr<VulkanPaintEngine> dev;
//    VkCommandPool transferCmdPool;
//    QSize sz;

//    // QPaintDevice interface
//  public:
//    VulkanPaintDevice(VulkanDevice &dev, const QSize &sz, VkCommandPool transferCmdPool):
//      rp(dev),
//      fbo(std::make_unique<VulkanFbo>(rp, sz, transferCmdPool)),
//      dev(std::make_unique<VulkanPaintEngine>(rp)),
//      transferCmdPool(transferCmdPool),
//      sz(sz)
//    {
//      rp.setFbo(*fbo);
//    }

//    QPaintEngine *paintEngine() const override
//    {
//      return dev.get();
//    }

//    void setSize(const QSize &sz)
//    {
//      fbo = std::make_unique<VulkanFbo>(rp, sz, transferCmdPool);
//      rp.setFbo(*fbo);
//    }

//    const VkDescriptorImageInfo* descriptorInfo() const
//    {
//      return fbo->getColorImgDescriptor();
//    }

//    VkImageView getImgView() const
//    {
//      return descriptorInfo()->imageView;
//    }

//    VkFormat getImgFormat() const
//    {
//      return fbo->getColorVkFormat();
//    }

//    // QPaintDevice interface
//  protected:
//    int metric(PaintDeviceMetric metric) const override
//    {
//      switch(metric)
//        {
//        case PdmWidth: return sz.width();
//        case PdmHeight: return sz.height();
//        default: return QPaintDevice::metric(metric);
//        }
//    }
//  };
}

VulkanOverlayImg::VulkanOverlayImg(VulkanDevice &dev, const QSize &sz, bool zeroCopy):
  completeRendering(dev),
  texture(dev, false, true, true),
  size(sz),
  glGenSemaphoresEXT(nullptr),
  glSignalSemaphoreEXT(nullptr),
  glWaitSemaphoreEXT(nullptr),
  glDeleteSemaphoresEXT(nullptr),
  glCreateMemoryObjectsEXT(nullptr),
  glImportSemHandleEXT(nullptr),
  glImportMemHandleEXT(nullptr),
  glTexStorageMem2DEXT(nullptr),
  fbo(0),
  mem(0),
  tex(0),
  renderbuff(0),
  glComplete(0),
  zeroCopy(zeroCopy)
{
  initTexture(sz);
}

VulkanOverlayImg::~VulkanOverlayImg()
{
  destroy();
}

void VulkanOverlayImg::init()
{
  sfc.setFormat(sfcFmt);
  sfc.create();
  Q_ASSERT(sfc.isValid());

  glCtx.setFormat(sfcFmt);
  glCtx.create();
  glCtx.makeCurrent(&sfc);

  if(zeroCopy)
    {
      const auto glExtensions = glCtx.extensions();
      if(!glExtensions.contains("GL_EXT_memory_object") || !glExtensions.contains("GL_EXT_semaphore"))
        {
          qWarning().noquote() << "KurisuRenderer: Disabling zero-copy 2d overlays";
          this->zeroCopy = false;
        }
    }

  if(this->zeroCopy)
    {
      glGenSemaphoresEXT = reinterpret_cast<decltype(glGenSemaphoresEXT)>(glCtx.getProcAddress("glGenSemaphoresEXT"));
      glSignalSemaphoreEXT = reinterpret_cast<decltype(glSignalSemaphoreEXT)>(glCtx.getProcAddress("glSignalSemaphoreEXT"));
      glWaitSemaphoreEXT = reinterpret_cast<decltype(glWaitSemaphoreEXT)>(glCtx.getProcAddress("glWaitSemaphoreEXT"));
      glDeleteSemaphoresEXT = reinterpret_cast<decltype(glDeleteSemaphoresEXT)>(glCtx.getProcAddress("glDeleteSemaphoresEXT"));
      glCreateMemoryObjectsEXT = reinterpret_cast<decltype(glCreateMemoryObjectsEXT)>(glCtx.getProcAddress("glCreateMemoryObjectsEXT"));
      glTexStorageMem2DEXT = reinterpret_cast<decltype(glTexStorageMem2DEXT)>(glCtx.getProcAddress("glTexStorageMem2DEXT"));

#     ifdef Q_OS_UNIX
      glImportSemHandleEXT = reinterpret_cast<decltype(glImportSemHandleEXT)>(glCtx.getProcAddress("glImportSemaphoreFdEXT"));
      glImportMemHandleEXT = reinterpret_cast<decltype(glImportMemHandleEXT)>(glCtx.getProcAddress("glImportMemoryFdEXT"));
#     elif defined(Q_OS_WINDOWS)
      glImportSemHandleEXT = reinterpret_cast<decltype(glImportSemHandleEXT)>(glCtx.getProcAddress("glImportSemaphoreWin32HandleEXT"));
      glImportMemHandleEXT = reinterpret_cast<decltype(glImportMemHandleEXT)>(glCtx.getProcAddress("glImportMemoryWin32HandleEXT"));
#     endif
    }

  if(KawaiiConfig::getInstance().getDebugLevel() > 0)
    {
      glCtx.functions()->glEnable(gl_debug_output);
      glCtx.functions()->glEnable(gl_debug_output_synchronous);
      glCtx.extraFunctions()->glDebugMessageCallback(&receiveGLDebug, nullptr);
    }

  glCtx.functions()->glClearColor(0,0,0,0);
  initFbo(size);
  paintDev = std::make_unique<QOpenGLPaintDevice>(size);
  paintDev->setPaintFlipped(true);

  if(zeroCopy)
    {
      auto completeSemFd = completeRendering.getOpaqueHandle();
      Q_ASSERT(completeSemFd.has_value());
      // Import semaphores
      glGenSemaphoresEXT(1, &glComplete);
      glImportSemHandleEXT(glComplete, opaqueHandleType, *completeSemFd);
    }
}

void VulkanOverlayImg::destroy()
{
  makeCurrent();

  destroyGlFbo();
  if(zeroCopy)
    {
      glDeleteSemaphoresEXT(1, &glComplete);
      glComplete = 0;
    }

  paintDev.reset();
  glCtx.doneCurrent();
}

void VulkanOverlayImg::resize(const QSize &sz)
{
  initTexture(sz);

  makeCurrent();
  initFbo(sz);
  paintDev->setSize(sz);
  glCtx.functions()->glFlush();
  glCtx.functions()->glFinish();

  size = sz;
}

QPaintDevice &VulkanOverlayImg::qPaintDev()
{
  return *paintDev;
}

VkImageView VulkanOverlayImg::getVkImageView() const
{
  return texture.getVkImageView();
}

const VkDescriptorImageInfo *VulkanOverlayImg::descriptorInfo() const
{
  return texture.descriptorInfo();
}

VkFormat VulkanOverlayImg::getVkFormat() const
{
  return texture.getVkFormat();
}

VkSemaphore VulkanOverlayImg::getCompleteSemaphore() const
{
  return completeRendering.getSemaphore();
}

//#include <chrono>

void VulkanOverlayImg::flush()
{
//  const auto t0 = std::chrono::steady_clock::now();
  if(zeroCopy)
    {
      GLenum dstLayout = GL_LAYOUT_SHADER_READ_ONLY_EXT;
      glSignalSemaphoreEXT(glComplete, 0, nullptr, 1, &tex, &dstLayout);

      glCtx.functions()->glFlush();
    } else
    texture.copyBitsFromGl(*glCtx.functions());

//  const auto t1 = std::chrono::steady_clock::now();
//  printf("VulkanOverlayImg::flush: %g ms\n", 0.001 * static_cast<float>(std::chrono::duration_cast<std::chrono::microseconds>(t1-t0).count()));
}

void VulkanOverlayImg::wait()
{
  if(!glCtx.isValid())
    init();

  makeCurrent();

  glCtx.functions()->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

bool VulkanOverlayImg::isZeroCopy() const
{
  return zeroCopy;
}

void VulkanOverlayImg::destroyGlFbo()
{
  if(fbo)
    {
      glCtx.functions()->glBindFramebuffer(GL_FRAMEBUFFER, 0);
      glCtx.functions()->glBindRenderbuffer(GL_RENDERBUFFER, 0);

      glCtx.functions()->glDeleteFramebuffers(1, &fbo);
      glCtx.functions()->glDeleteTextures(1, &tex);
      glCtx.functions()->glDeleteRenderbuffers(1, &renderbuff);

      fbo = 0;
      mem = 0;
      tex = 0;
      renderbuff = 0;
    }
}

void VulkanOverlayImg::initTexture(const QSize &sz)
{
  texture.setExtent(sz.width(), sz.height());
  texture.setFormat_8bpc(KawaiiTextureFormat::RedGreenBlueAlpha);
  texture.setLinear(isZeroCopy());
  try {
    texture.createImage(nullptr, isZeroCopy());
  } catch (const std::runtime_error&) {
    texture.setLinear(false);
    texture.createImage(nullptr, isZeroCopy());
    qWarning("KurisuRenderer: VulkanOverlayImg: using non linear overlay texture!");
  }
  texture.createImageView(VkImageViewType::VK_IMAGE_VIEW_TYPE_2D);
}

void VulkanOverlayImg::initFbo(const QSize &sz)
{
  destroyGlFbo();

  if(zeroCopy)
    {
      auto texMemHandle = texture.getMemHandle(VulkanTexture::imgSameGpuMemoryHandleType);
      Q_ASSERT(texMemHandle.has_value());

      // Import texture
      glCtx.functions()->glGenTextures(1, &tex);
      glCtx.functions()->glBindTexture(GL_TEXTURE_2D, tex);
      glCreateMemoryObjectsEXT(1, &mem);
      glImportMemHandleEXT(mem, texture.getAllocatedSize(), opaqueHandleType, *texMemHandle);
      glTexStorageMem2DEXT(GL_TEXTURE_2D, 1, GL_RGBA8, sz.width(), sz.height(), mem, 0);
      glCtx.functions()->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_TILING_EXT, texture.isLinear()? GL_LINEAR_TILING_EXT: GL_OPTIMAL_TILING_EXT);
      glCtx.functions()->glBindTexture(GL_TEXTURE_2D, 0);
    } else
    {
      glCtx.functions()->glGenTextures(1, &tex);
      glCtx.functions()->glBindTexture(GL_TEXTURE_2D, tex);
      glCtx.functions()->glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, sz.width(), sz.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
      glCtx.functions()->glBindTexture(GL_TEXTURE_2D, 0);
    }

  Q_ASSERT(glCtx.functions()->glIsTexture(tex));

  glCtx.functions()->glGenRenderbuffers(1, &renderbuff);
  glCtx.functions()->glBindRenderbuffer(GL_RENDERBUFFER, renderbuff);
  glCtx.functions()->glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, sz.width(), sz.height());

  Q_ASSERT(glCtx.functions()->glIsRenderbuffer(renderbuff));

  glCtx.functions()->glGenFramebuffers(1, &fbo);
  glCtx.functions()->glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  glCtx.functions()->glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);
  glCtx.functions()->glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, renderbuff);

  Q_ASSERT(glCtx.functions()->glIsFramebuffer(fbo));
//  Q_ASSERT(glCtx.functions()->glCheckFramebufferStatus(fbo) == GL_FRAMEBUFFER_COMPLETE);

  glCtx.functions()->glViewport(0,0, sz.width(), sz.height());
}

void VulkanOverlayImg::makeCurrent()
{
  if(QOpenGLContext::currentContext() != &glCtx)
    glCtx.makeCurrent(&sfc);
}
