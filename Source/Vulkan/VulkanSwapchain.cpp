#include "VulkanSwapchain.hpp"
#include "VulkanSwapchainCapabilities.hpp"
#include <QDebug>
#include <Kawaii3D/KawaiiConfig.hpp>

VulkanSwapchain::VulkanSwapchain(QWindow *wnd, VkSurfaceKHR sfc, VulkanDevice &dev):
  swapchain(VK_NULL_HANDLE),
  sfc(sfc),
  dev(dev),
  wnd(wnd),

  depthImg(dev, false, true),
  presentQ(VK_NULL_HANDLE),
  asyncFrames(0),
  w(0),
  h(0)
{
  recreate();
}

VulkanSwapchain::~VulkanSwapchain()
{
  dev.waitIdle();

  for(const auto &i: imageViews)
    vkDestroyImageView(dev.vkDev, i, nullptr);

  vkDestroySwapchainKHR(dev.vkDev, swapchain, nullptr);
}

VkFormat VulkanSwapchain::getImageFormat() const
{
  return imageFormat;
}

VulkanDevice &VulkanSwapchain::getDevice() const
{
  return dev;
}

bool VulkanSwapchain::checkExtent() const
{
  const auto extent = wnd->size();

  return static_cast<uint32_t>(extent.width()) == w
      && static_cast<uint32_t>(extent.height()) == h;
}

void VulkanSwapchain::recreate()
{
  presentQ = VK_NULL_HANDLE;
  if(swapchain)
    {
      if(checkExtent()) return;

      dev.waitIdle();

      vkDestroySwapchainKHR(dev.vkDev, swapchain, nullptr);
      for(const auto &i: imageViews)
        vkDestroyImageView(dev.vkDev, i, nullptr);
      imageViews.clear();
    }

  VulkanSwapchainCapabilities swapChainSupport(dev, sfc);

  VkSurfaceFormatKHR surfaceFormat = swapChainSupport.chooseSwapSurfaceFormat();
  imageFormat = surfaceFormat.format;

  VkPresentModeKHR presentMode = swapChainSupport.chooseSwapPresentMode();

  VkExtent2D extent = swapChainSupport.chooseSwapExtent(wnd->size());
  w = extent.width;
  h = extent.height;

  uint32_t imageCount = swapChainSupport.capabilities.minImageCount
      + std::max<uint32_t>(1, KawaiiConfig::getInstance().getRenderbuffersCount().value_or(2)) - 1;

  if (swapChainSupport.capabilities.maxImageCount > 0 && imageCount > swapChainSupport.capabilities.maxImageCount)
    imageCount = swapChainSupport.capabilities.maxImageCount;

  asyncFrames = imageCount - swapChainSupport.capabilities.minImageCount + 1;

  uint32_t queueFamilyIndices[] = { dev.queueFamilies.graphicsFamily.value(), dev.getPresentQueueFamily(sfc).value() };

  VkSwapchainCreateInfoKHR createInfo = {
    .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
    .surface = sfc,
    .minImageCount = imageCount,
    .imageFormat = imageFormat,
    .imageColorSpace = surfaceFormat.colorSpace,
    .imageExtent = extent,
    .imageArrayLayers = 1,
    .imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
    .imageSharingMode = VK_SHARING_MODE_EXCLUSIVE,
    .queueFamilyIndexCount = 1,
    .pQueueFamilyIndices = queueFamilyIndices,
    .preTransform = swapChainSupport.capabilities.currentTransform,
    .compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
    .presentMode = presentMode,
    .clipped = VK_TRUE
  };

  if (dev.queueFamilies.graphicsFamily != dev.getPresentQueueFamily(sfc))
    {
      createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
      createInfo.queueFamilyIndexCount = 2;
      createInfo.pQueueFamilyIndices = queueFamilyIndices;
    }

  if (vkCreateSwapchainKHR(dev.vkDev, &createInfo, nullptr, &swapchain) != VK_SUCCESS)
    throw std::runtime_error("failed to create swap chain!");

  vkGetSwapchainImagesKHR(dev.vkDev, swapchain, &imageCount, nullptr);
  images.resize(imageCount);
  vkGetSwapchainImagesKHR(dev.vkDev, swapchain, &imageCount, images.data());

  imageViews.reserve(images.size());
  for(const auto &i: images)
    {
      const VkImageViewCreateInfo createInfo = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .image = i,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .format = imageFormat,

        .components = {
          .r = VK_COMPONENT_SWIZZLE_IDENTITY,
          .g = VK_COMPONENT_SWIZZLE_IDENTITY,
          .b = VK_COMPONENT_SWIZZLE_IDENTITY,
          .a = VK_COMPONENT_SWIZZLE_IDENTITY
        },

        .subresourceRange = {
          .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
          .levelCount = 1,
          .layerCount = 1
        }
      };

      VkImageView imageView;
      if (vkCreateImageView(dev.vkDev, &createInfo, nullptr, &imageView) != VK_SUCCESS)
        throw std::runtime_error("failed to create image views!");

      imageViews.push_back(imageView);
    }

  createDepthImg();
}

const std::vector<VkImageView> &VulkanSwapchain::getImageViews() const
{
  return imageViews;
}

VkImageView VulkanSwapchain::getDepthImgView()
{
  return depthImg.getVkImageView();
}

uint32_t VulkanSwapchain::width() const
{
  return w;
}

uint32_t VulkanSwapchain::height() const
{
  return h;
}

size_t VulkanSwapchain::getAsyncFrames() const
{
  return asyncFrames;
}

uint32_t VulkanSwapchain::acquireNextImage(VkSemaphore semaphore, bool *outdatedSwapchain)
{
  uint32_t imageIndex;
  auto result = vkAcquireNextImageKHR(dev.vkDev, swapchain, UINT64_MAX, semaphore, nullptr, &imageIndex);

  switch(result) {
    case VK_SUBOPTIMAL_KHR:
    case VK_SUCCESS:
      return imageIndex;

    case VK_ERROR_OUT_OF_DATE_KHR:
      if(outdatedSwapchain != nullptr)
        *outdatedSwapchain = true;
      return 0;
      break;

    default:
      throw std::runtime_error("failed to acquire image");
    }
}

void VulkanSwapchain::present(VkSemaphore waitSemaphore, uint32_t imageIndex)
{
  const VkPresentInfoKHR presentInfo = {
    VK_STRUCTURE_TYPE_PRESENT_INFO_KHR, // sType
    nullptr, // pNext
    static_cast<uint32_t>(waitSemaphore? 1: 0), // waitSemaphoreCount
    &waitSemaphore, // pWaitSemaphores
    1, // swapchainCount
    &swapchain, // pSwapchains
    &imageIndex, // pImageIndices
    nullptr // pResults
  };

  if(!presentQ)
    {
      if(auto q = dev.getPresentQueue(sfc); q.has_value())
        presentQ = q.value();
      else
        throw std::runtime_error("the device is not able to present to the surface!");
    }

  vkQueuePresentKHR(presentQ, &presentInfo);
}

void VulkanSwapchain::createDepthImg()
{
  depthImg.setFormat_32bpc(KawaiiTextureFormat::Depth);
  depthImg.setExtent(w, h);
  depthImg.createImage(nullptr);

  depthImg.createImageView(VK_IMAGE_VIEW_TYPE_2D);
}
