#ifndef VULKANSTAGINGBUFFER_HPP
#define VULKANSTAGINGBUFFER_HPP

#include <vulkan/vulkan.h>
#include "VulkanDevice.hpp"
#include <functional>
#include <QFuture>

class VulkanStagingBuffer
{
public:
  VulkanStagingBuffer(const VulkanDevice &dev, const void *data, size_t dataSize);
  VulkanStagingBuffer(const VulkanDevice &dev, const std::function<void(void*)> &dataSrc, size_t dataSize);

  ~VulkanStagingBuffer();

  VkBuffer getBuf() const;

  size_t getAllocatedSize() const;

  bool setData(const void *data, size_t dataSize);
  bool setData(const std::function<void(void*)> &dataSrc, size_t dataSize);

  bool isReady() const;
  void waitForTask();
  void setAsyncTask(const QFuture<void> &future, VkFence fence);



private:
  QFuture<void> cpuTask;
  VkFence gpuTask;

  VkDevice dev;
  VkBuffer buf;
  VkDeviceMemory mem;

  size_t allocatedSize;

  void *mappedMemory;
};

#endif // VULKANSTAGINGBUFFER_HPP
