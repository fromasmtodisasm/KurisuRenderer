#include "VulkanSemaphore.hpp"

namespace {
# if defined(Q_OS_UNIX)
  constexpr VkExternalSemaphoreHandleTypeFlagBits externalSemaphoreBits = VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_FD_BIT;
# elif defined(Q_OS_WINDOWS)
  constexpr VkExternalSemaphoreHandleTypeFlagBits externalSemaphoreBits = VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT;
# endif
}

VulkanSemaphore::VulkanSemaphore(VulkanDevice &dev):
  dev(dev)
{
  const VkExportSemaphoreCreateInfo exportInfo = {
    .sType = VK_STRUCTURE_TYPE_EXPORT_SEMAPHORE_CREATE_INFO,
    .handleTypes = externalSemaphoreBits
  };

  const VkSemaphoreCreateInfo createInfo = {
    .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
    .pNext = &exportInfo
  };

  if(vkCreateSemaphore(dev.vkDev, &createInfo, nullptr, &sem) != VK_SUCCESS)
    throw std::runtime_error("failed to create semaphore!");
}

VulkanSemaphore::VulkanSemaphore(VulkanSemaphore &&orig):
  dev(orig.dev),
  sem(orig.sem)
{
  orig.sem = VK_NULL_HANDLE;
}

VulkanSemaphore::~VulkanSemaphore()
{
  if(sem)
    vkDestroySemaphore(dev.vkDev, sem, nullptr);
}

VkSemaphore VulkanSemaphore::getSemaphore() const
{
  return sem;
}

std::optional<VulkanDevice::Handle> VulkanSemaphore::getOpaqueHandle() const
{
  return dev.getSemHandle(sem, externalSemaphoreBits);
}
