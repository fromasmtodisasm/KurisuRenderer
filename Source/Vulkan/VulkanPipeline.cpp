#include "VulkanPipeline.hpp"
#include "RenderingCommands.hpp"

#include <Kawaii3D/Shaders/KawaiiShader.hpp>
#include <QVector4D>
#include <QVector3D>
#include <stdexcept>
#include <array>

namespace {
  constexpr std::array<VkVertexInputAttributeDescription, 3> baseAtributes = {
    VkVertexInputAttributeDescription {
      0, // location
      0, // binding
      VK_FORMAT_R32G32B32A32_SFLOAT, // format
      0 // offset
    },
    VkVertexInputAttributeDescription {
      1, // location
      0, // binding
      VK_FORMAT_R32G32B32_SFLOAT, // format
      sizeof(QVector4D) // offset
    },
    VkVertexInputAttributeDescription {
      2, // location
      0, // binding
      VK_FORMAT_R32G32B32_SFLOAT, // format
      sizeof(QVector4D) + sizeof(QVector3D) // offset
    }
  };

  constexpr VkVertexInputBindingDescription vertexBindingDescription = {
    0, // binding
    sizeof(QVector4D) + sizeof(QVector3D) + sizeof(QVector3D), // stride
    VK_VERTEX_INPUT_RATE_VERTEX // inputRate
  };
}

VulkanPipeline::VulkanPipeline(const VulkanDevice &dev, VulkanPipelineLayout &layout):
  devInfo(dev),

  vertices {
    VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO, // sType
    nullptr, // pNext
    0, // flags
    1, // vertexBindingDescriptionCount
    &vertexBindingDescription, // pVertexBindingDescriptions
    baseAtributes.size(), // vertexAttributeDescriptionCount
    baseAtributes.data() // pVertexAttributeDescriptions
    },

  inputAssembly {
    VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO, // sType
    nullptr, // pNext
    0, // flags
    VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST, // topology
    VK_FALSE // primitiveRestartEnable
    },

  rasterizer {
    VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO, // sType
    nullptr, // pNext
    0, // flags
    VK_FALSE, // depthClampEnable
    VK_FALSE, // rasterizerDiscardEnable
    VK_POLYGON_MODE_FILL, // polygonMode
    VK_CULL_MODE_NONE, // cullMode
    VK_FRONT_FACE_COUNTER_CLOCKWISE, // frontFace
    VK_FALSE, // depthBiasEnable
    0, // depthBiasConstantFactor
    0, // depthBiasClamp
    0, // depthBiasSlopeFactor
    1 // lineWidth
    },

  multisampling {
    VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO, // sType
    nullptr, // pNext
    0, // flags
    VK_SAMPLE_COUNT_1_BIT, // rasterizationSamples
    VK_FALSE, // sampleShadingEnable
    1, // minSampleShading
    nullptr, // pSampleMask
    VK_FALSE, // alphaToCoverageEnable
    VK_FALSE // alphaToOneEnable
    },

  depthStencil {
    VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO, // sType
    nullptr, // pNext
    0, // flags
    VK_TRUE, // depthTestEnable
    VK_TRUE, // depthWriteEnable
    VK_COMPARE_OP_LESS, // depthCompareOp
    VK_FALSE, // depthBoundsTestEnable
    VK_FALSE, // stencilTestEnable
    {}, // front
    {}, // back
    0, // minDepthBounds
    1 // maxDepthBounds
    },

  colorBlendAttachment {
    VK_FALSE, // blendEnable
    VK_BLEND_FACTOR_ONE, // srcColorBlendFactor
    VK_BLEND_FACTOR_ZERO, // dstColorBlendFactor
    VK_BLEND_OP_ADD, // colorBlendOp
    VK_BLEND_FACTOR_ONE, // srcAlphaBlendFactor
    VK_BLEND_FACTOR_ZERO, // dstAlphaBlendFactor
    VK_BLEND_OP_ADD, // alphaBlendOp
    VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT // colorWriteMask
    },

  colorBlending {
    VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO, // sType
    nullptr, // pNext
    0, // flags
    VK_FALSE, // logicOpEnable
    VK_LOGIC_OP_COPY, // logicOp
    1, // attachmentCount
    &colorBlendAttachment, // pAttachments
    {0, 0, 0, 0}// blendConstants[4]
    },

  layout(layout)
{
  onLayoutInvalidate = layout.addOnInvalidate([this] { markDirty(); });
}

VulkanPipeline::~VulkanPipeline()
{
  layout.removeOnInvalidate(onLayoutInvalidate);

  devInfo.waitIdle();
  for(const auto &i: pipelines)
    vkDestroyPipeline(devInfo.vkDev, i.second.vkPipeline, nullptr);
}

VulkanPipeline::Topology VulkanPipeline::getTopology() const
{
  switch(inputAssembly.topology) {
    case VK_PRIMITIVE_TOPOLOGY_POINT_LIST:
      return Topology::Points;

    case VK_PRIMITIVE_TOPOLOGY_LINE_LIST:
      return Topology::Lines;

    case VK_PRIMITIVE_TOPOLOGY_LINE_STRIP:
      return Topology::LineStrip;

    case VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST:
      return Topology::Triangles;

    case VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP:
      return Topology::TriangleStrip;

    case VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN:
      return Topology::TriangleFan;

    case VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY:
      return Topology::LinesWithAdjacency;

    case VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY:
      return Topology::LineStripWithAdjacency;

    case VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY:
      return Topology::TrianglesWithAdjacency;

    case VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY:
      return Topology::TriangleStripWithAdjacency;

    case VK_PRIMITIVE_TOPOLOGY_PATCH_LIST:
      return Topology::Patches;

    default:
      throw std::runtime_error("VulkanPipeline::getTopology: illegal state");
    }
}

void VulkanPipeline::setTopology(VulkanPipeline::Topology topology)
{
  switch(topology)
    {
    case Topology::Points:
      inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
      break;
    case Topology::Lines:
      inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
      break;
    case Topology::LineStrip:
      inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_LINE_STRIP;
      break;
    case Topology::Triangles:
      inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
      break;
    case Topology::TriangleStrip:
      inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
      break;
    case Topology::TriangleFan:
      inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN;
      break;
    case Topology::LinesWithAdjacency:
      inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY;
      break;
    case Topology::LineStripWithAdjacency:
      inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY;
      break;
    case Topology::TrianglesWithAdjacency:
      inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY;
      break;
    case Topology::TriangleStripWithAdjacency:
      inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY;
      break;
    case Topology::Patches:
      inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_PATCH_LIST;
      break;
    }
  markDirty();
}

void VulkanPipeline::setSurfaceFormat(const QSurfaceFormat &format)
{
  //todo: implement
  throw std::runtime_error("VulkanPipeline::setSurfaceFormat not_implemented");
}

void VulkanPipeline::setShaderStages(const std::vector<const VulkanShader *> &stages)
{
  shaderStagesInfo.resize(stages.size());
  for(size_t i = 0; i < stages.size(); ++i)
    shaderStagesInfo[i] = stages[i]->stageCreateInfo();

  markDirty();
}

void VulkanPipeline::addShaderStage(const VulkanShader *shader)
{
  shaderStagesSrc.push_back(shader);
  shaderStagesInfo.push_back(shader->stageCreateInfo());
  markDirty();
}

void VulkanPipeline::updateShaderStage(const VulkanShader *shader)
{
  auto el = std::find(shaderStagesSrc.cbegin(), shaderStagesSrc.cend(), shader);
  if(el != shaderStagesSrc.end()) {
      auto el1 = shaderStagesInfo.begin() + (el - shaderStagesSrc.cbegin());
      *el1 = shader->stageCreateInfo();
      markDirty();
    } else
    addShaderStage(shader);
}

void VulkanPipeline::removeShaderStage(const VulkanShader *shader)
{
  auto el = std::find(shaderStagesSrc.cbegin(), shaderStagesSrc.cend(), shader);
  if(el != shaderStagesSrc.end()) {
      shaderStagesInfo.erase(shaderStagesInfo.cbegin() + (el - shaderStagesSrc.cbegin()));
      shaderStagesSrc.erase(el);
      markDirty();
    }
}

void VulkanPipeline::setVertexInputState(const VkPipelineVertexInputStateCreateInfo &state)
{
  vertices = state;
  markDirty();
}

void VulkanPipeline::setDepthTestEnabled(bool value)
{
  depthStencil.depthTestEnable = value? VK_TRUE: VK_FALSE;
  depthStencil.depthCompareOp = value? VK_COMPARE_OP_LESS: VK_COMPARE_OP_ALWAYS;
  markDirty();
}

void VulkanPipeline::setDepthWriteEnabled(bool value)
{
  depthStencil.depthWriteEnable = value? VK_TRUE: VK_FALSE;
  markDirty();
}

void VulkanPipeline::setColorBlending(const VkPipelineColorBlendAttachmentState &val)
{
  colorBlendAttachment = val;
  markDirty();
}

VkPipeline VulkanPipeline::getPipeline(RenderingCommands *commands, VkRenderPass renderpass, uint32_t subpass)
{
  const QRectF viewport = commands->getViewport();

  auto el = pipelines.find(commands);
  if(el == pipelines.end())
    el = pipelines.insert(std::pair(commands, SpecializedPipeline {.dirty = true})).first;

  if(el->second.dirty
     || el->second.renderpass != renderpass
     || el->second.subpass != subpass
     || el->second.viewport != viewport)
    {
      recreate(el->second.vkPipeline, viewport, renderpass, subpass);
      el->second.renderpass = renderpass;
      el->second.subpass = subpass;
      el->second.viewport = viewport;
      el->second.dirty = false;
    }

  return el->second.vkPipeline;
}

const VulkanDevice &VulkanPipeline::getDevice() const
{
  return devInfo;
}

void VulkanPipeline::recreate(VkPipeline &pipeline, const QRectF &viewport, VkRenderPass renderpass, uint32_t subpass)
{
  if(pipeline) {
      devInfo.waitIdle();
      vkDestroyPipeline(devInfo.vkDev, pipeline, nullptr);
      pipeline = VK_NULL_HANDLE;
    }

  const VkViewport vkViewport = {
    .x        = static_cast<float>(viewport.x()),
    .y        = static_cast<float>(viewport.y()),
    .width    = static_cast<float>(viewport.width()),
    .height   = static_cast<float>(viewport.height()),
    .minDepth = 0,
    .maxDepth = 1.0f
  };

  const VkRect2D scissor = {
    .offset = VkOffset2D {
      .x = static_cast<int32_t>(vkViewport.x),
      .y = static_cast<int32_t>(vkViewport.y)
    },
    .extent = VkExtent2D {
      .width = static_cast<uint32_t>(vkViewport.width),
      .height = static_cast<uint32_t>(vkViewport.height)
    }
  };

  const VkPipelineViewportStateCreateInfo viewportState = {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO, // sType
    .viewportCount = 1, // viewportCount
    .pViewports = &vkViewport, // pViewports
    .scissorCount = 1, // scissorCount
    .pScissors = &scissor // pScissors
  };

  const VkGraphicsPipelineCreateInfo createInfo = {
    .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
    .stageCount = static_cast<uint32_t>(shaderStagesInfo.size()),
    .pStages = shaderStagesInfo.data(),
    .pVertexInputState = &vertices,
    .pInputAssemblyState = &inputAssembly,
    .pTessellationState = nullptr,
    .pViewportState = &viewportState,
    .pRasterizationState = &rasterizer,
    .pMultisampleState = &multisampling,
    .pDepthStencilState = &depthStencil,
    .pColorBlendState = &colorBlending,
    .layout = layout.getPipelineLayout(),
    .renderPass = renderpass,
    .subpass = subpass,
  };

  if (vkCreateGraphicsPipelines(devInfo.vkDev, devInfo.pipelineCache, 1, &createInfo, nullptr, &pipeline) != VK_SUCCESS)
    throw std::runtime_error("failed to create pipeline!");
}

void VulkanPipeline::markDirty()
{
  for(auto &i: pipelines)
    i.second.dirty = true;
}

