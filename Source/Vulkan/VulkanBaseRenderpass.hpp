#ifndef VULKANBASERENDERPASS_HPP
#define VULKANBASERENDERPASS_HPP

#include "RenderingCommands.hpp"
#include "VulkanSemaphore.hpp"
#include <QColor>
#include <vector>
#include <QRect>

class VulkanBaseRenderpass
{
  VulkanBaseRenderpass(const VulkanBaseRenderpass &orig) = delete;
  VulkanBaseRenderpass& operator= (const VulkanBaseRenderpass &orig) = delete;

public:
  VulkanBaseRenderpass(VulkanDevice &dev, VkFormat colorFmt, size_t image_count);
  VulkanBaseRenderpass(VulkanDevice &dev, const std::vector<std::pair<VulkanTexture*, int>> &renderBufs, size_t image_count);
  VulkanBaseRenderpass(VulkanBaseRenderpass &&orig);
  virtual ~VulkanBaseRenderpass();

  VulkanDevice &getDevice() const;

  VkRenderPass getRenderpass();

  uint32_t addAttachement(const VkAttachmentDescription &att, VkImageView img, const VkClearValue &clearValue);
  void addInputAttachment(const VkAttachmentDescription &att, VkImageView img, const VkClearValue &clearValue, VkImageLayout imgLayout, size_t subpass);
  void addInputAttachment(uint32_t attachmentIndex, VkImageLayout imgLayout, size_t subpass);
  void addColorAttachment(uint32_t attachmentIndex, VkImageLayout imgLayout, size_t subpass);
  void addPreserveAttachment(uint32_t attachmentIndex, size_t subpass);
  void updateAttachmentImage(size_t index, VkImageView new_value);

  void appendSubpass(const VkAttachmentReference &colorAttachment, const VkAttachmentReference &depthAttachment);
  void prependSubpass(const VkAttachmentReference &colorAttachment, const VkAttachmentReference &depthAttachment);
  void addSubpassDependency(const VkSubpassDependency &dependency);

  VkAttachmentDescription& getInternalAttachment(size_t i);
  const VkAttachmentDescription& getInternalAttachment(size_t i) const;

  VkAttachmentDescription& getAttachment(size_t i);
  const VkAttachmentDescription& getAttachment(size_t i) const;

  void exec(RenderingCommands &commands, uint32_t imageIndex);

  const std::vector<VkClearValue> &getClearValues() const;

  void wait(size_t img_index) const;

protected:
  VulkanDevice &dev;
  VkRenderPass renderPass;

  std::vector<VkSubpassDependency> dependencies;
  std::vector<VkAttachmentDescription> attachments;
  std::vector<VkImageView> advAttachmentImages;
  std::vector<VkClearValue> clearValues;

  std::vector<std::vector<VkAttachmentReference>> inputAttachments;
  std::vector<std::vector<VkAttachmentReference>> colorAttachments;
  std::vector<std::vector<uint32_t>> preserveAttachments;
  std::vector<VkAttachmentReference> depthAttachments;

  std::vector<VkSubpassDescription> subpasses;

  std::vector<VkFence> fences;

  VkRenderPassCreateInfo createInfo;

  VulkanSemaphore renderingCompleatedSem;

  bool dirty;

  void onSubpassesRelocated();
  void onAttachementsRelocated();
  void onDependenciesRelocated();

  void destroyResources();

  inline virtual void createAdvResources() {}
  inline virtual void destroyAdvResources() {}
};

#endif // VULKANBASERENDERPASS_HPP
