#ifndef VULKANHELPER_HPP
#define VULKANHELPER_HPP

#include "VulkanDevice.hpp"
#include <QByteArrayList>
#include <functional>
#include <optional>
#include <vector>
#include <string>
#include <memory>
#include <QSize>
#include <list>

class VulkanDeviceManager
{
public:
  VulkanDeviceManager(VkInstance instance, const QByteArrayList &layers, const std::vector<std::string> &device_extensions);
  ~VulkanDeviceManager();

  VulkanDevice &device(size_t i);
  const VulkanDevice &device(size_t i) const;

  const std::vector<VulkanDevice> &allDevices() const;

private:
  static VulkanDevice::QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device);

  VkInstance inst;

  std::vector<VulkanDevice> devices;

  const QByteArrayList layers;
  const std::vector<std::string> extensions;

  const std::vector<const char*> layers_cstr;
  const std::vector<const char*> extensions_cstr;


  void createDevices();
};

#endif // VULKANHELPER_HPP
