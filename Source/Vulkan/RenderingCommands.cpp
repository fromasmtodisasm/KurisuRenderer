#include "RenderingCommands.hpp"
#include "VulkanPipelineLayout.hpp"
#include <Kawaii3D/KawaiiConfig.hpp>
#include <unordered_map>

namespace {
  template<size_t N>
  void printTraceCommand(const char (&str) [N])
  {
    if(KawaiiConfig::getInstance().getDebugLevel() > 2)
      qDebug().noquote() << QLatin1String(str, N);
  }
}

RenderingCommands::RenderingCommands(VulkanDevice &dev, size_t image_count, bool primary):
  dev(dev),
  commandPool(VK_NULL_HANDLE),
  commandBuffers(image_count),
  descriptorPools(image_count),
  defaultPipelineLayout(VK_NULL_HANDLE),
  waitingForResources(image_count, false),
  state(State::Empty),
  bindedPipeline(VK_NULL_HANDLE),
  primary(primary)
{
  const VkCommandPoolCreateInfo poolInfo = {
    VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO, // sType
    nullptr, // pNext
    VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT, // flags
    dev.queueFamilies.graphicsFamily.value() // queueFamilyIndex
  };

  if (vkCreateCommandPool(dev.vkDev, &poolInfo, nullptr, &commandPool) != VK_SUCCESS)
    throw std::runtime_error("failed to create command pool!");

  const VkCommandBufferAllocateInfo allocInfo = {
    VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO, // sType
    nullptr, // pNext
    commandPool, // commandPool
    primary? VK_COMMAND_BUFFER_LEVEL_PRIMARY: VK_COMMAND_BUFFER_LEVEL_SECONDARY, // level
    static_cast<uint32_t>(commandBuffers.size()) // commandBufferCount
  };

  if (vkAllocateCommandBuffers(dev.vkDev, &allocInfo, commandBuffers.data()) != VK_SUCCESS)
    throw std::runtime_error("failed to allocate command buffers!");

  resourcesReady.reserve(image_count);
  for(size_t i = 0; i < image_count; ++i)
    {
      resourcesReady.emplace_back(dev);
      descriptorPools[i] = std::make_unique<VulkanDescriptorPool>(dev, i, image_count);
      descriptorPools[i]->setParent(this);

      connect(descriptorPools[i].get(), &VulkanDescriptorPool::recreated, this, &RenderingCommands::rewrite);
      connect(descriptorPools[i].get(), &VulkanDescriptorPool::descriptorSetUpdated, this, &RenderingCommands::onDescriptorSetUpdated);
    }
}

RenderingCommands::RenderingCommands(const VulkanSwapchain &swapchain):
  RenderingCommands(swapchain.getDevice(), swapchain.getImageViews().size(), true)
{
}

RenderingCommands::~RenderingCommands()
{
  dev.waitIdle();
  vkDestroyCommandPool(dev.vkDev, commandPool, nullptr);
  resourcesReady.clear();
  descriptorPools.clear();
}

void RenderingCommands::reset()
{
  if(state != State::Empty)
    {
      dev.waitIdle();
      emit aboutToReset();

      delayedCommands.clear();
      usedBufers.clear();
      for(auto *cmd: usedCommands)
        disconnect(cmd, &RenderingCommands::aboutToReset, this, &RenderingCommands::rewrite);
      usedCommands.clear();

      for(const auto &i: commandBuffers)
        vkResetCommandBuffer(i, 0);
      state = State::Empty;
      bindedPipeline = VK_NULL_HANDLE;

      for(const auto &pool: descriptorPools)
        {
          for(const auto i: usedDescriptorSetNames)
            pool->releaseDescriptorSet(i);
          pool->forgetUsedBuffers();
        }
      usedDescriptorSetNames.clear();
    }
}

void RenderingCommands::startRecording(VkRenderPass renderPass, const std::vector<VkFramebuffer> &fbo, const std::vector<VkClearValue> &clearValues, VkSubpassContents subpassContents, uint32_t subpass)
{
  delayedCommands.push_back([this, renderPass, fbo, subpass] (VkCommandBuffer cmdBuf, size_t i) {
      const VkCommandBufferInheritanceInfo inheritanceInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO,
        .renderPass = renderPass,
        .subpass = subpass,
        .framebuffer = fbo[i % fbo.size()],
      };

      const VkCommandBufferBeginInfo beginInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .flags = static_cast<VkCommandBufferUsageFlags>(isPrimary()? VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT: VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT | VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT),
        .pInheritanceInfo = isPrimary()? nullptr: &inheritanceInfo,
      };

      if (vkBeginCommandBuffer(cmdBuf, &beginInfo) != VK_SUCCESS)
        throw std::runtime_error("failed to begin recording command buffer!");
    });

  state = State::WritingCommands;
  if(primary)
    beginRenderpass(renderPass, fbo, clearValues, subpassContents);

  printTraceCommand("=======");
}

void RenderingCommands::beginRenderpass(VkRenderPass renderPass, const std::vector<VkFramebuffer> &fbo, const std::vector<VkClearValue> &clearValues, VkSubpassContents subpassContents)
{
  delayedCommands.push_back([this, renderPass, fbo, clearValues, subpassContents] (VkCommandBuffer cmd, size_t i) {
      const VkRenderPassBeginInfo renderPassInfo = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
        .renderPass = renderPass,
        .framebuffer = fbo[i % fbo.size()],

        .renderArea = VkRect2D {
          .offset = VkOffset2D {
            .x = viewport.x(),
            .y = viewport.y()
          },
          .extent = VkExtent2D {
            .width = static_cast<uint32_t>(viewport.width()),
            .height = static_cast<uint32_t>(viewport.height())}
        },

        .clearValueCount = static_cast<uint32_t>(clearValues.size()),
        .pClearValues = clearValues.empty()? nullptr: clearValues.data()
      };
      vkCmdBeginRenderPass(cmd, &renderPassInfo, subpassContents);
    });
}

void RenderingCommands::setViewport(const QRect &vp)
{
  if(viewport == vp) return;
  viewport = vp;
  rewrite();
}

void RenderingCommands::bindGraphicsPipeline(VkPipeline pipeline)
{
  if(bindedPipeline != pipeline)
    {
      bindedPipeline = pipeline;
      delayedCommands.push_back([pipeline] (VkCommandBuffer i, size_t) {
        vkCmdBindPipeline(i, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
      });
    printTraceCommand("vkCmdBindPipeline");
    }
}

void RenderingCommands::bindVbos(uint32_t firstBinding, const std::vector<VulkanBuffer*> &buffers, const std::vector<VkDeviceSize> &offsets)
{
  Q_ASSERT(buffers.size() == offsets.size());

  usedBufers.insert(usedBufers.end(), buffers.cbegin(), buffers.cend());

  delayedCommands.push_back([this, buffers, offsets, firstBinding] (VkCommandBuffer i, size_t imgIndex) {
      std::vector<VkBuffer> v;
      v.reserve(buffers.size());
      for(auto *i: buffers)
        v.push_back(i->getDetachedBuffer(this, imgIndex, commandBuffers.size()));

      vkCmdBindVertexBuffers(i, firstBinding, v.size(), v.data(), offsets.data());
    });
  printTraceCommand("vkCmdBindVertexBuffers");
}

void RenderingCommands::bindIndexBuffer(VulkanBuffer &buffer, VkIndexType indexType, VkDeviceSize offset)
{
  usedBufers.push_back(&buffer);
  delayedCommands.push_back([this, &buffer, indexType, offset] (VkCommandBuffer i, size_t imgIndex) {
      vkCmdBindIndexBuffer(i, buffer.getDetachedBuffer(this, imgIndex, commandBuffers.size()), offset, indexType);
    });
  printTraceCommand("vkCmdBindIndexBuffer");
}

void RenderingCommands::createDescriptorSet(uint64_t name, const std::vector<VulkanDescriptorPool::BufBinding> &bufBindings, const std::vector<VulkanDescriptorPool::ImgBinding> &imgBindings, VkDescriptorSetLayout layout, const std::unordered_map<VkDescriptorType, uint32_t> &descriptorCount)
{
  usedDescriptorSetNames.insert(name);
  for(const auto &pool: descriptorPools)
    pool->createDescriptorSet(name, bufBindings, imgBindings, layout, descriptorCount);
}

void RenderingCommands::bindDescriptorSet(uint64_t descrSetName, uint32_t descrSetBinding)
{
  delayedCommands.push_back([this, descrSetName, descrSetBinding] (VkCommandBuffer i, size_t imageIndex) {
      VkDescriptorSet descrSet = descriptorPools[imageIndex]->getDescriptorSet(descrSetName);
      vkCmdBindDescriptorSets(i, VK_PIPELINE_BIND_POINT_GRAPHICS, defaultPipelineLayout->getPipelineLayout(), descrSetBinding, 1, &descrSet, 0, nullptr);
  });
  printTraceCommand("vkCmdBindDescriptorSets");
}

void RenderingCommands::bindDescriptorSet(VkDescriptorSet descrSet, VkPipelineLayout pipelineLayout)
{
  delayedCommands.push_back([descrSet, pipelineLayout] (VkCommandBuffer i, size_t) {
    vkCmdBindDescriptorSets(i, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1, &descrSet, 0, nullptr);
  });
  printTraceCommand("vkCmdBindDescriptorSets");
}

void RenderingCommands::draw(uint32_t vertexCount, uint32_t instanceCount, uint32_t vertex_i, uint32_t instance_i)
{
  delayedCommands.push_back([vertexCount, instanceCount, vertex_i, instance_i] (VkCommandBuffer i, size_t) {
      vkCmdDraw(i, vertexCount,instanceCount, vertex_i,instance_i);
    });
  printTraceCommand("vkCmdDraw");
}

void RenderingCommands::drawIndexed(uint32_t vertexCount, uint32_t instanceCount, uint32_t vertex_i, uint32_t instance_i)
{
  delayedCommands.push_back([vertexCount, instanceCount, vertex_i, instance_i] (VkCommandBuffer i, size_t) {
      vkCmdDrawIndexed(i, vertexCount,instanceCount, vertex_i,0,instance_i);
    });
  printTraceCommand("vkCmdDrawIndexed");
}

void RenderingCommands::drawIndexedIndirect(VulkanBuffer &buffer, VkDeviceSize offset)
{
  usedBufers.push_back(&buffer);
  delayedCommands.push_back([this, &buffer, offset] (VkCommandBuffer i, size_t imgIndex) {
      vkCmdDrawIndexedIndirect(i, buffer.getDetachedBuffer(this, imgIndex, commandBuffers.size()), offset, 1, 0);
    });
  printTraceCommand("vkCmdDrawIndexedIndirect");
}

void RenderingCommands::execute(RenderingCommands &commands)
{
  Q_ASSERT(primary);
  Q_ASSERT(!commands.isPrimary());

  usedCommands.push_back(&commands);
  connect(&commands, &RenderingCommands::aboutToReset, this, &RenderingCommands::rewrite);
  delayedCommands.push_back([&commands] (VkCommandBuffer i, size_t imageIndex) {
    commands.check();
    const VkCommandBuffer cmdBuf = commands.getCommandBuffer(imageIndex);
    vkCmdExecuteCommands(i, 1, &cmdBuf);
  });
}

void RenderingCommands::nextSubpass(VkSubpassContents subpassContents)
{
  delayedCommands.push_back([subpassContents](VkCommandBuffer cmdBuf, size_t) {
    vkCmdNextSubpass(cmdBuf, subpassContents);
  });
}

void RenderingCommands::endRenderpass()
{
  if(primary)
    delayedCommands.push_back([] (VkCommandBuffer cmdBuf, size_t) { vkCmdEndRenderPass(cmdBuf); });
}

void RenderingCommands::finishRecording()
{
  for(size_t i = 0; i < commandBuffers.size(); ++i)
    {
      VkCommandBuffer cmdBuf = commandBuffers[i];
      for(const auto &j: delayedCommands)
        j(cmdBuf, i);

      if (vkEndCommandBuffer(cmdBuf) != VK_SUCCESS)
        throw std::runtime_error("failed to record command buffer!");
    }

  for(const auto &pool: descriptorPools)
    pool->collectGarbage();

  if(KawaiiConfig::getInstance().getDebugLevel() > 1)
    qDebug("KurisuRenderer: backed command buffer(s): %lu commands", delayedCommands.size());

  state = State::Compleate;
}

void RenderingCommands::copyTexture(const VulkanTexture &from, const VulkanTexture &to, const std::vector<VkImageCopy> &regions)
{
  delayedCommands.push_back([&from, &to, regions] (VkCommandBuffer cmdBuf, size_t) {
      VkPipelineStageFlags sourceStage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
      VkPipelineStageFlags destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;

      std::array<VkImageMemoryBarrier, 2> barriers = {
        VkImageMemoryBarrier {
          .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
          .oldLayout = from.descriptorInfo()->imageLayout,
          .newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
          .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
          .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
          .image = from.getVkImage(),
          .subresourceRange = VkImageSubresourceRange {
            .aspectMask = regions[0].srcSubresource.aspectMask,
            .levelCount = 1,
            .layerCount = 1,
          }
        },
        VkImageMemoryBarrier {
          .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
          .oldLayout = to.descriptorInfo()->imageLayout,
          .newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
          .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
          .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
          .image = to.getVkImage(),
          .subresourceRange = VkImageSubresourceRange {
            .aspectMask = regions[0].dstSubresource.aspectMask,
            .levelCount = 1,
            .layerCount = 1,
          }
        }
      };

      vkCmdPipelineBarrier ( cmdBuf,
                             sourceStage, destinationStage,
                             0,
                             0, nullptr,
                             0, nullptr,
                             2, barriers.data() );

      vkCmdCopyImage(cmdBuf, from.getVkImage(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, to.getVkImage(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, regions.size(), regions.data());

      std::swap(barriers[0].oldLayout, barriers[0].newLayout);
      std::swap(barriers[1].oldLayout, barriers[1].newLayout);
      std::swap(sourceStage, destinationStage);
//      barriers[0] = barriers[1];
//      barriers[0].image = from.getVkImage();
//      barriers[0].oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
//      barriers[0].newLayout = from.descriptorInfo()->imageLayout;
//      barriers[0].subresourceRange.aspectMask = regions[0].srcSubresource.aspectMask;

      vkCmdPipelineBarrier ( cmdBuf,
                             sourceStage, destinationStage,
                             0,
                             0, nullptr,
                             0, nullptr,
                             2, barriers.data() );
    });
}

void RenderingCommands::copyTexture(const VulkanTexture &from, const VulkanTexture &to, VkImageAspectFlags imgAspect)
{
  Q_ASSERT(from.getExtent().width == to.getExtent().width
           && from.getExtent().height == to.getExtent().height
           && from.getExtent().depth == to.getExtent().depth
           );

  const VkImageCopy rg = {
    .srcSubresource = {
      .aspectMask = imgAspect,
      .layerCount = 1
    },
    .dstSubresource = {
      .aspectMask = imgAspect,
      .layerCount = 1
    },
    .extent = from.getExtent()
  };

  copyTexture(from, to, {rg});
}

void RenderingCommands::prepare(size_t image_index)
{
  for(auto *i: usedCommands)
    {
      for(auto *j: i->usedBufers)
        j->syncDetachedBuffer(i, image_index);

      i->descriptorPools[image_index]->prepareResources(image_index);
      i->check();
    }

  for(auto *i: usedBufers)
    i->syncDetachedBuffer(this, image_index);

  descriptorPools[image_index]->prepareResources(image_index);
  check();

  waitingForResources[image_index] = dev.submitTransferCommandsAsync(resourcesReady[image_index].getSemaphore());
}

bool RenderingCommands::isPrimary() const
{
  return primary;
}

RenderingCommands::State RenderingCommands::getState() const
{
  return state;
}

bool RenderingCommands::isEmpty() const
{
  return (state == State::Empty);
}

bool RenderingCommands::hasBindedPipeline()
{
  return bindedPipeline != VK_NULL_HANDLE;
}

void RenderingCommands::setDefaultPipelineLayout(VulkanPipelineLayout *value)
{
  Q_ASSERT(!defaultPipelineLayout || defaultPipelineLayout == value);
  defaultPipelineLayout = value;
}

void RenderingCommands::exec(uint32_t imageIndex, std::vector<VkSemaphore> &&waitSemaphores, std::vector<VkPipelineStageFlags> &&waitStages, std::vector<VkSemaphore> &&signalSemaphores, VkFence signalFence)
{
  Q_ASSERT(state == State::Compleate);

  Q_ASSERT(waitStages.size() == waitSemaphores.size());

  if(waitingForResources[imageIndex])
    {
      waitStages.push_back(VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT);
      waitSemaphores.push_back(resourcesReady[imageIndex].getSemaphore());
    }

  VkCommandBuffer cmdBuf = getCommandBuffer(imageIndex);
  const VkSubmitInfo submitInfo = {
    .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
    .waitSemaphoreCount = static_cast<uint32_t>(waitSemaphores.size()),
    .pWaitSemaphores = waitSemaphores.empty()? nullptr: waitSemaphores.data(),
    .pWaitDstStageMask = waitStages.empty()? nullptr: waitStages.data(),
    .commandBufferCount = 1,
    .pCommandBuffers = &cmdBuf,
    .signalSemaphoreCount = static_cast<uint32_t>(signalSemaphores.size()),
    .pSignalSemaphores = signalSemaphores.empty()? nullptr: signalSemaphores.data()
  };

  waitingForResources[imageIndex] = false;

  if (vkQueueSubmit(dev.graphicsQueue, 1, &submitInfo, signalFence) != VK_SUCCESS)
    throw std::runtime_error("failed to submit draw command buffer!");
}

VkCommandBuffer RenderingCommands::getCommandBuffer(size_t image_index) const
{
  return commandBuffers[image_index];
}

void RenderingCommands::rewrite()
{
  if(state == State::Compleate)
    {
      dev.waitIdle();
      emit aboutToReset();
      for(VkCommandBuffer cmdBuf: commandBuffers)
        vkResetCommandBuffer(cmdBuf, 0);
      state = State::Dirty;
    }
}

void RenderingCommands::onDescriptorSetUpdated(uint64_t name)
{
  if(usedDescriptorSetNames.count(name) > 0)
    rewrite();
}

void RenderingCommands::check()
{
  if(state == State::Dirty)
    {
      dev.waitIdle();
      for(auto i = waitingForResources.begin(); i != waitingForResources.end(); ++i)
        if(*i)
          *i = false;
      //for(VkCommandBuffer cmdBuf: commandBuffers)
      //  vkResetCommandBuffer(cmdBuf, 0);
      finishRecording();
    }
}
