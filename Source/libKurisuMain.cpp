#include <Kawaii3D/KawaiiConfig.hpp>
#include <QWindow>

#include "KawaiiKurisuFactory.hpp"
#include "KurisuRootImpl.hpp"

extern "C" KURISURENDERER_SHARED_EXPORT KawaiiRendererImpl* createKurisuRoot(KawaiiRoot *root)
{
  return KawaiiKurisuFactory::getInstance()->createRenderer(*root);
}

extern "C" KURISURENDERER_SHARED_EXPORT bool checkKurisu()
{
  return KurisuRootImpl::checkSystem();
}
