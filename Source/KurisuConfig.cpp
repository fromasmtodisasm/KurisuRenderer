#include "KurisuConfig.hpp"
#include <QProcessEnvironment>

namespace {
  const QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
}

KurisuConfig::StaticConfig KurisuConfig::sharedCfg(env.value("SIB3D_GPU", "0").toULongLong(),
                                                   std::max<uint64_t>(env.value("SIB3D_CACHED_STAGING_BUFS", "8").toULongLong(), 1),
                                                   env.value("SIB3D_VULKAN_GL_INTEROP", "1").toUShort()
                                                   );

KurisuConfig::KurisuConfig(const VulkanDeviceManager &devices)
{
  composerGpu = std::min<uint64_t>(sharedCfg.preferredComposerGpu, devices.allDevices().size()-1);
}

KurisuConfig::~KurisuConfig()
{
}

uint64_t KurisuConfig::getComposerGpu() const
{
  return composerGpu;
}

uint64_t KurisuConfig::getMaxCachedStagingBuffers()
{
  return sharedCfg.maxCachedStagingBuffers;
}

bool KurisuConfig::isGlInteropAllowed()
{
  return sharedCfg.glInterop;
}

KurisuConfig::StaticConfig::StaticConfig(uint64_t composerGpu, uint64_t maxCachedStagingBuffers, bool glInterop):
  preferredComposerGpu(composerGpu),
  maxCachedStagingBuffers(maxCachedStagingBuffers),
  glInterop(glInterop)
{
}
