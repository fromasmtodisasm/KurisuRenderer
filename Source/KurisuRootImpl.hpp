#ifndef KURISUROOTIMPL_HPP
#define KURISUROOTIMPL_HPP

#include <KawaiiRenderer/KawaiiRootImpl.hpp>
#include "Textures/KurisuOffscrRenderer.hpp"
#include "Textures/KurisuTextureHandle.hpp"
#include "KurisuBufferHandle.hpp"
#include "KurisuBufferBindings.hpp"
#include "KurisuConfig.hpp"

#include "Vulkan/VulkanBaseRenderpass.hpp"
#include "Vulkan/VulkanDeviceManager.hpp"
#include "Vulkan/VulkanPipelineLayout.hpp"
#include "Vulkan/VulkanDescriptorPool.hpp"
#include "Vulkan/RenderingCommands.hpp"
#include "Vulkan/VulkanCommandPool.hpp"
#include "Vulkan/VulkanInstance.hpp"

#include <QThreadPool>

#include <sib_utils/PairHash.hpp>

class KurisuSurfaceImpl;
class KurisuFramebufferImpl;

class KURISURENDERER_SHARED_EXPORT KurisuRootImpl: public KawaiiRootImpl
{
  friend class KurisuBufferHandle;
public:
  struct Binding {
    uint32_t binding;
    uint32_t set;
  };

  KurisuRootImpl(KawaiiRoot *model);
  ~KurisuRootImpl();

  KurisuBufferBindings bufBindings;

  VkSurfaceKHR getSurface(QWindow *wnd);

  void forallDevices(const std::function<void(VulkanDevice&)> &func);
  void forallDevices(const std::function<void(VulkanDevice&, size_t)> &func);

  VulkanDevice &getDevice(size_t device_index) const;
  uint32_t deviceCount() const;

  VulkanPipelineLayout& getPipelineLayout(size_t device_index);

  void activateCommands(RenderingCommands &cmd, size_t device_index);
  RenderingCommands& getActiveCommands();
  bool hasActiveCommands() const;
  void releaseCommands();

  void activateSurface(KurisuSurfaceImpl *surface);
  KurisuSurfaceImpl* getActiveSurface();
  void releaseSurface();

  std::vector<VkFramebuffer> getActiveFramebuffers(size_t device_index);
  VulkanBaseRenderpass& getActiveRenderpass(size_t device_index);

  bool isNowOffscreen() const;
  void setOffscreenTarget(KurisuOffscrRenderer *offscrRender);

  size_t getActiveDevice() const;

  KurisuConfig& getConfig() const;

  Binding getTextureLocation(const std::string &textureName, uint32_t blockBinding);
  Binding getGlobUboLocation(uint32_t number);
  uint32_t getDescrSetIndex(uint32_t binding);

  static bool checkSystem();

  // KawaiiRootImpl interface
  KawaiiBufferHandle *createBuffer(const void *ptr, size_t n, const std::initializer_list<KawaiiBufferTarget> &availableTargets) override final;
  KawaiiTextureHandle *createTexture(KawaiiTexture *model) override final;

private:
  void beginOffscreen() override final;
  void endOffscreen() override final;



  //IMPLEMENT
private:
  std::unique_ptr<VulkanInstance> inst;
  std::unique_ptr<VulkanDeviceManager> devices;
  std::vector<VulkanPipelineLayout> pipelineLayouts;

  KurisuSurfaceImpl *currentSfc;
  RenderingCommands* currentCmd;

  KurisuOffscrRenderer *currentOffscr;

  size_t current_device;

  std::unique_ptr<KurisuConfig> config;

  std::unordered_map<std::pair<std::string /*name*/, uint32_t /*block binding*/>, Binding,
  sib_utils::PairHash<std::string, uint32_t>> textureLocations;

  std::vector<VulkanDescriptorPool::ImgBinding> globImgBindings;

  std::unordered_map<uint32_t, Binding> globUboLocations;

//  std::vector<KurisuFramebufferImpl*> offscreenTasks;

  bool offscreen;

  struct BufferBinding {
    VkDescriptorType target;
    uint32_t block;
    KurisuBufferHandle *buf;
  };

  void bindBuffersToDescriptor(std::vector<BufferBinding> &&buffers, RenderingCommands &cmd, size_t device_index);

  void onChangedBlockBufBinding(KawaiiBufferTarget target, uint32_t block, KurisuBufferHandle *buf);
};

#endif // KURISUROOTIMPL_HPP
