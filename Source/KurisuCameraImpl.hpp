#ifndef KURISUCAMERAIMPL_HPP
#define KURISUCAMERAIMPL_HPP

#include <KawaiiRenderer/KawaiiCameraImpl.hpp>
#include "KurisuRenderer_global.hpp"

#include "Vulkan/RenderingCommands.hpp"

class KurisuRootImpl;
class KURISURENDERER_SHARED_EXPORT KurisuCameraImpl : public KawaiiCameraImpl
{
public:
  KurisuCameraImpl(KawaiiCamera *model);

  RenderingCommands& getCommands(const KawaiiGpuBuf *sfcUbo, size_t device_index) const;

  // KawaiiCameraImpl interface
protected:
  struct CachedRenderpass: KawaiiCameraImpl::DrawCache {
    std::vector<std::unique_ptr<RenderingCommands>> cmd;

    CachedRenderpass(KurisuRootImpl *root);
    ~CachedRenderpass() = default;

    void reset(KurisuCameraImpl *cam);
    void draw(KurisuCameraImpl *cam, KawaiiGpuBufImpl *sfcUbo, const QRect &viewport);

    void exec(KurisuCameraImpl *cam);
  };

  void updateCache(DrawCache &cache, KawaiiGpuBufImpl *sfcUbo, const QRect &viewport) override;
  DrawCache *createDrawCache(KawaiiGpuBufImpl *sfcUbo, const QRect &viewport) override final;



  //IMPLEMENT
private:
  KurisuRootImpl *root();
};

#endif // KURISUCAMERAIMPL_HPP
