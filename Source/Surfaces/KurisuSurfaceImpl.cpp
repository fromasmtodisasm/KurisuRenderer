#include "KurisuSurfaceImpl.hpp"
#include "KurisuCameraImpl.hpp"
#include "KurisuRootImpl.hpp"
#include "KurisuConfig.hpp"

#include "Vulkan/GlslCompiler.hpp"
#include <sib_utils/ioReadAll.hpp>

namespace {
  inline constexpr glm::mat4 vulkanToGlClipCorrection(1.0f,  0.0f, 0.0f, 0.0f,
                                                      0.0f, -1.0f, 0.0f, 0.0f,
                                                      0.0f,  0.0f, 0.5f, 0.0f,
                                                      0.0f,  0.0f, 0.5f, 1.0f);

  inline constexpr bool allowOverlay = true;
}

KurisuSurfaceImpl::KurisuSurfaceImpl(KawaiiSurface *model):
  KawaiiSurfaceImpl(model),
  presentDescriptorPool(VK_NULL_HANDLE),
  presentDescriptorSet(VK_NULL_HANDLE),
  currentGpu(0),
  renderingReady(false)
{
  connect(model, &KawaiiSurface::cameraChanged, this, &KurisuSurfaceImpl::updateRenderingCommands);
  getModel()->setClipCorrectionMatrix(vulkanToGlClipCorrection);

  const auto wndStates = getModel()->getWindow().windowStates();
  bool wasVisible = getModel()->getWindow().isVisible();

  getModel()->getWindow().setSurfaceType(QSurface::VulkanSurface);
  getModel()->getWindow().destroy();
  getModel()->getWindow().create();
  getModel()->getWindow().setWindowStates(wndStates);
  getModel()->getWindow().setVisible(wasVisible);
}

KurisuSurfaceImpl::~KurisuSurfaceImpl()
{
  invalidate();
}

VulkanBaseRenderpass &KurisuSurfaceImpl::getRenderpass(size_t gpu_index) const
{
  if(gpu_index == getRoot()->getConfig().getComposerGpu())
    return *renderpass;
  else
    return sceneRenderers[gpu_index]->getRenderpass();
}

std::vector<VkFramebuffer> KurisuSurfaceImpl::getFramebuffers(size_t gpu_index) const
{
  if(gpu_index == getRoot()->getConfig().getComposerGpu())
    {
      renderpass->getRenderpass();
      return renderpass->getFramebuffers();
    } else
    return { sceneRenderers[gpu_index]->getVkFbo() };
}

const VulkanTexture &KurisuSurfaceImpl::getColorStagingTex(size_t gpu_index)
{
  return sceneRenderers[gpu_index]->getColorStagingTex();
}

const VulkanTexture &KurisuSurfaceImpl::getColorFboTex(size_t gpu_index)
{
  return sceneRenderers[gpu_index]->getColorFboTex();
}

void KurisuSurfaceImpl::updateRenderingCommands()
{
//  waitForRenderFinished();

  for(const auto &i: presentCommands)
    if(i)
      i->reset();

  for(const auto &i: overlayCommands)
    if(i)
      i->reset();
}

void KurisuSurfaceImpl::waitForRenderFinished()
{
  getRoot()->forallDevices([] (VulkanDevice &dev) {
    dev.waitIdle();
  });
}

uint32_t KurisuSurfaceImpl::imgCount()
{
  return swapchain? swapchain->getImageViews().size(): 0;
}

bool KurisuSurfaceImpl::isReady() const
{
  return renderingReady;
}

uint32_t KurisuSurfaceImpl::getImgIndex() const
{
  return *renderpass->getImageIndex();
}

size_t KurisuSurfaceImpl::getActiveGpu() const
{
  return currentGpu;
}

KurisuRootImpl *KurisuSurfaceImpl::getRoot() const
{
  return static_cast<KurisuRootImpl*>(getModel()->getRoot()->getRendererImpl());
}

QPaintDevice *KurisuSurfaceImpl::getPaintDevice()
{
  return allowOverlay? &overlayTexture->qPaintDev(): nullptr;
}

void KurisuSurfaceImpl::resize(const QSize &sz)
{
  static_cast<void>(sz);
  forceResize();
}

void KurisuSurfaceImpl::startRendering()
{
  if(!swapchain)
    initSwapchain();

  if(!swapchain->checkExtent())
    forceResize();

  if(overlayTexture)
    overlayTexture->wait();

  getRoot()->activateSurface(this);
  renderingReady = renderpass->prepareFrame();
}

void KurisuSurfaceImpl::finishRendering()
{
  if(overlayTexture)
    overlayTexture->flush();

  getRoot()->releaseSurface();

  if(renderingReady)
    {
      if(presentCommands[currentGpu]->isEmpty())
        writeCommands();

      std::vector<VulkanRenderpass::Dependency> depencencies;

      if(allowOverlay && overlayTexture->isZeroCopy())
        depencencies.push_back( VulkanRenderpass::Dependency { .sem = overlayTexture->getCompleteSemaphore(), .stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT } );

      renderpass->exec(*presentCommands[currentGpu], std::move(depencencies));
    }
  renderingReady = false;
  currentGpu = (currentGpu+1) % getRoot()->deviceCount();
}

void KurisuSurfaceImpl::invalidate()
{
  uint64_t composerGpu = getRoot()->getConfig().getComposerGpu();
  VkDevice vkDev = getRoot()->getDevice(composerGpu).vkDev;

  sceneRenderers.clear();
  renderpass.reset();

  overlayCommands.clear();
  presentCommands.clear();
  swapchain.reset();
  overlayTexture.reset();

  if(presentDescriptorPool)
    {
      vkDestroyDescriptorPool(vkDev, presentDescriptorPool, nullptr);
      presentDescriptorPool = nullptr;
    }

  quadVertShader.reset();
  quadFragShader.reset();
  overlayPipeline.reset();
  scenePipeline.reset();
  overlayPipelineLayout.reset();
}

void KurisuSurfaceImpl::initSwapchain()
{
  uint64_t composerGpu = getRoot()->getConfig().getComposerGpu();
  auto &dev = getRoot()->getDevice(composerGpu);

  swapchain = std::make_unique<VulkanSwapchain>(&getModel()->getWindow(),
            getRoot()->getSurface(&getModel()->getWindow()),
            dev);

  renderpass = std::make_unique<VulkanRenderpass>(*swapchain);
  renderpass->appendSubpass({
                              .attachment = 0,
                              .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
                            },
                            {
                              .attachment = 1,
                              .layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
                            });
  const VkSubpassDependency dependency = {
    .srcSubpass = 0,
    .dstSubpass = 1,
    .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
    .dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
    .srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
    .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
    .dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT
  };
  renderpass->addSubpassDependency(dependency);

  if(allowOverlay)
    overlayTexture = std::make_unique<VulkanOverlayImg>(dev, QSize(swapchain->width(), swapchain->height()), getRoot()->getConfig().isGlInteropAllowed());

  sceneRenderers.resize(getRoot()->deviceCount());
  for(size_t i = 0; i < sceneRenderers.size(); ++i)
    if(i != composerGpu)
      sceneRenderers[i] = std::make_unique<SceneRenderer>(getRoot()->getDevice(i), getModel()->getSize(), getRoot()->getDevice(composerGpu), swapchain->getImageViews().size());

  if(allowOverlay)
    {
      VkAttachmentDescription attachmentDescr {
        .format = overlayTexture->getVkFormat(),
            .samples = VK_SAMPLE_COUNT_1_BIT,
            .loadOp = VK_ATTACHMENT_LOAD_OP_LOAD,
            .storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
            .finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
      };

      const uint32_t attachmentIndex =  renderpass->addAttachement(attachmentDescr, overlayTexture->getVkImageView(), VkClearValue { .color = { .float32 = {0,0,0,0} } });
      renderpass->addPreserveAttachment(attachmentIndex, 0);
      renderpass->addInputAttachment(attachmentIndex, overlayTexture->descriptorInfo()->imageLayout, 1);
    }

  renderpass->getRenderpass();

  //input texture
  VkDescriptorSetLayoutBinding setLayoutBinding = {
    .binding = 0,
    .descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT,
    .descriptorCount = 1,
    .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
  };
  VkDevice vkDev = dev.vkDev;
  overlayPipelineLayout = std::make_unique<VulkanPipelineLayout>(vkDev, std::vector<std::vector<VkDescriptorSetLayoutBinding>> { {setLayoutBinding} });

  const VkDescriptorPoolSize poolSize = { VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, static_cast<uint32_t>(sceneRenderers.size()) };
  const VkDescriptorPoolCreateInfo poolCreateInfo = {
    .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
    .maxSets = static_cast<uint32_t>(sceneRenderers.size()),
    .poolSizeCount = 1,
    .pPoolSizes = &poolSize
  };

  vkCreateDescriptorPool(vkDev, &poolCreateInfo, nullptr, &presentDescriptorPool); //todo: check error code

  Q_ASSERT(overlayPipelineLayout->getDescriptorSetLayouts().size() == 1);

  const std::vector<VkDescriptorSetLayout> descriptorSetLayouts(sceneRenderers.size(), overlayPipelineLayout->getDescriptorSetLayouts().front());

  const VkDescriptorSetAllocateInfo allocateInfo = {
    .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
    .descriptorPool = presentDescriptorPool,
    .descriptorSetCount = static_cast<uint32_t>(sceneRenderers.size()),
    .pSetLayouts = descriptorSetLayouts.data()
  };

  presentDescriptorSet.resize(sceneRenderers.size());
  if(vkAllocateDescriptorSets(vkDev, &allocateInfo, presentDescriptorSet.data()) != VK_SUCCESS)
    throw std::runtime_error("Can not allocate compose descriptor sets");

  updateComposeDescriptorSet();

  quadFragShader = std::make_unique<VulkanShader>(vkDev, VkShaderStageFlagBits::VK_SHADER_STAGE_FRAGMENT_BIT);
  quadFragShader->setCode(GlslCompiler().glslToSpirv(sib_utils::ioReadAll(QFile(":/kurisu-glsl/quad.fs.glsl")).data(), EShLangFragment, ":/kurisu-glsl/quad.fs.glsl"));

  quadVertShader = std::make_unique<VulkanShader>(vkDev, VkShaderStageFlagBits::VK_SHADER_STAGE_VERTEX_BIT);
  quadVertShader->setCode(GlslCompiler().glslToSpirv(sib_utils::ioReadAll(QFile(":/kurisu-glsl/quad.vs.glsl")).data(), EShLangVertex, ":/kurisu-glsl/quad.vs.glsl"));

  static constexpr VkPipelineVertexInputStateCreateInfo pipelineVertexInputState = {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
    .vertexBindingDescriptionCount = 0,
    .pVertexBindingDescriptions = nullptr,
    .vertexAttributeDescriptionCount = 0,
    .pVertexAttributeDescriptions = nullptr
  };

  scenePipeline = std::make_unique<VulkanPipeline>(dev, *overlayPipelineLayout);
  scenePipeline->setVertexInputState(pipelineVertexInputState);
//  scenePipeline->setSurfaceFormat(KawaiiConfig::getInstance().getPreferredSfcFormat());
  scenePipeline->addShaderStage(quadVertShader.get());
  scenePipeline->addShaderStage(quadFragShader.get());
  scenePipeline->setDepthTestEnabled(false);
  scenePipeline->setDepthWriteEnabled(false);

  overlayPipeline = std::make_unique<VulkanPipeline>(dev, *overlayPipelineLayout);
  overlayPipeline->setVertexInputState(pipelineVertexInputState);
//  overlayPipeline->setSurfaceFormat(KawaiiConfig::getInstance().getPreferredSfcFormat());
  overlayPipeline->addShaderStage(quadVertShader.get());
  overlayPipeline->addShaderStage(quadFragShader.get());
  overlayPipeline->setDepthTestEnabled(false);
  overlayPipeline->setDepthWriteEnabled(false);
  overlayPipeline->setColorBlending(VkPipelineColorBlendAttachmentState {
                                      .blendEnable = VK_TRUE,
                                      .srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA,
                                      .dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
                                      .colorBlendOp = VK_BLEND_OP_ADD,
                                      .srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
                                      .dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
                                      .alphaBlendOp = VK_BLEND_OP_ADD,
                                      .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT /*| VK_COLOR_COMPONENT_A_BIT*/
                                    });

  overlayCommands.resize(sceneRenderers.size());
  presentCommands.resize(sceneRenderers.size());
  for(size_t i = 0; i < sceneRenderers.size(); ++i)
    {
      overlayCommands[i] = std::make_unique<RenderingCommands>(dev, swapchain->getImageViews().size(), false);
      presentCommands[i] = std::make_unique<RenderingCommands>(*swapchain);
    }
}

void KurisuSurfaceImpl::updateComposeDescriptorSet()
{
  uint64_t composerGpu = getRoot()->getConfig().getComposerGpu();
  VkDevice dev = getRoot()->getDevice(composerGpu).vkDev;

  std::vector<VkWriteDescriptorSet> writeDescriptorSets(sceneRenderers.size(), VkWriteDescriptorSet { .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET });

  static const constexpr VkDescriptorImageInfo nullImgDescr = {
    .sampler = VK_NULL_HANDLE,
    .imageView = VK_NULL_HANDLE,
    .imageLayout = VK_IMAGE_LAYOUT_UNDEFINED
  };
  for(size_t i = 0; i < sceneRenderers.size(); ++i)
    {
      writeDescriptorSets[i].dstSet = presentDescriptorSet[i];
      writeDescriptorSets[i].descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
      writeDescriptorSets[i].descriptorCount = 1;
      writeDescriptorSets[i].dstBinding = 0;
      if(i != composerGpu)
        writeDescriptorSets[i].pImageInfo = sceneRenderers[i]->getColorImgDescriptor();
      else
        writeDescriptorSets[i].pImageInfo = allowOverlay? overlayTexture->descriptorInfo(): &nullImgDescr;
    }
  if(allowOverlay)
    vkUpdateDescriptorSets(dev, writeDescriptorSets.size(), writeDescriptorSets.data(), 0, nullptr);
  else
    {
      if(composerGpu > 0)
        vkUpdateDescriptorSets(dev, composerGpu, writeDescriptorSets.data(), 0, nullptr);

      if(writeDescriptorSets.size() - composerGpu - 1 > 0)
        vkUpdateDescriptorSets(dev, writeDescriptorSets.size() - composerGpu - 1, writeDescriptorSets.data() + composerGpu + 1, 0, nullptr);
    }
}

void KurisuSurfaceImpl::writeCommands()
{
  const size_t composerGpu = getRoot()->getConfig().getComposerGpu();

  for(size_t i = 0; i < overlayCommands.size(); ++i)
    {
      if(i == composerGpu && !allowOverlay)
        continue;
      overlayCommands[i]->setViewport(renderpass->getViewport());
      overlayCommands[i]->startRecording(renderpass->getRenderpass(), renderpass->getFramebuffers(), renderpass->getClearValues(), VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS, 1);
      if(i != composerGpu)
        overlayCommands[i]->bindGraphicsPipeline(scenePipeline->getPipeline(overlayCommands[0].get(), renderpass->getRenderpass(), 1));
      else
        overlayCommands[i]->bindGraphicsPipeline(overlayPipeline->getPipeline(overlayCommands[0].get(), renderpass->getRenderpass(), 1));
      overlayCommands[i]->bindDescriptorSet(presentDescriptorSet[i], overlayPipelineLayout->getPipelineLayout());
      overlayCommands[i]->draw(6,1,0,0);
      overlayCommands[i]->finishRecording();
    }

  for(size_t i = 0; i < presentCommands.size(); ++i)
    {
      presentCommands[i]->setViewport(renderpass->getViewport());
      presentCommands[i]->startRecording(renderpass->getRenderpass(), renderpass->getFramebuffers(), renderpass->getClearValues(), VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS, 0);
      if(i == composerGpu)
        {
          for(size_t tile_i = 0; tile_i < getModel()->tileCount(); ++tile_i)
            presentCommands[i]->execute(static_cast<KurisuCameraImpl*>(getModel()->getTile(tile_i).getCamera()->getRendererImpl())->getCommands(&getModel()->getTile(tile_i).getSurfaceGpuVar(), i) );
        }
      presentCommands[i]->nextSubpass(VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS);
      if(i != composerGpu)
        presentCommands[i]->execute(*overlayCommands[i]);
      if(allowOverlay)
        presentCommands[i]->execute(*overlayCommands[composerGpu]);

      presentCommands[i]->endRenderpass();
      presentCommands[i]->finishRecording();
    }
}

void KurisuSurfaceImpl::forceResize()
{
  if(!swapchain) return;

  waitForRenderFinished();
//  for(size_t i = 0; i < renderpass->getAsyncFrames(); ++i)
//    renderpass->waitForFinished(i);

  swapchain->recreate();
  const QSize swapchainSz(swapchain->width(), swapchain->height());

  if(overlayTexture)
    {
      overlayTexture->resize(swapchainSz);
      renderpass->updateAttachmentImage(0, overlayTexture->getVkImageView());
    }

  for(const auto &i: sceneRenderers)
    if(i)
      i->resize(swapchainSz);

  renderpass->setViewport(QRect(QPoint(0,0), swapchainSz));

  updateComposeDescriptorSet();
  updateRenderingCommands();
}

KurisuSurfaceImpl::SceneRenderer::SceneRenderer(VulkanDevice &dev, const QSize &sz, VulkanDevice &composerDev, size_t image_count):
  renderpass(dev, VK_FORMAT_B8G8R8A8_UNORM, image_count),
  colorStaging(dev, false, false, true),
  colorComposeTex(composerDev, false, true, true)
{
  Q_ASSERT(dev.vkDev != composerDev.vkDev);
  resize(sz);
}

KurisuSurfaceImpl::SceneRenderer::~SceneRenderer()
{
  if(fbo)
    fbo->getDevice().waitIdle();
}

VkFormat KurisuSurfaceImpl::SceneRenderer::getColorVkFormat() const
{
  return colorComposeTex.getVkFormat();
}

const VkDescriptorImageInfo *KurisuSurfaceImpl::SceneRenderer::getColorImgDescriptor()
{
  return colorImgDescr;
}

VulkanBaseRenderpass &KurisuSurfaceImpl::SceneRenderer::getRenderpass()
{
  return renderpass;
}

VkFramebuffer KurisuSurfaceImpl::SceneRenderer::getVkFbo() const
{
  return fbo? fbo->getFbo(): VK_NULL_HANDLE;
}

const VulkanTexture &KurisuSurfaceImpl::SceneRenderer::getColorStagingTex() const
{
  return colorStaging;
}

const VulkanTexture &KurisuSurfaceImpl::SceneRenderer::getColorFboTex() const
{
  return fbo->getColorTexture();
}

void KurisuSurfaceImpl::SceneRenderer::resize(const QSize &sz)
{
  if(fbo)
    fbo->getDevice().waitIdle();

  fbo = std::make_unique<VulkanFbo>(renderpass, sz, KawaiiTextureFormat::BlueGreenRedAlpha, false, false);

  colorStaging.setExtent(sz.width(), sz.height());
  colorStaging.setFormat_8bpc(KawaiiTextureFormat::BlueGreenRedAlpha);
  colorStaging.setLinear(true);
  colorStaging.createImage(nullptr, true);
  colorStaging.setLayout(VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
  colorComposeTex.importImage(colorStaging);
  colorComposeTex.createImageView(VK_IMAGE_VIEW_TYPE_2D);
  colorImgDescr = colorComposeTex.descriptorInfo();
}
