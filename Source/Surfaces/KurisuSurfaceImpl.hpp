#ifndef KURISUSURFACEIMPL_HPP
#define KURISUSURFACEIMPL_HPP

#include <KawaiiRenderer/Surfaces/KawaiiSurfaceImpl.hpp>
#include "../KurisuRenderer_global.hpp"
#include "../Vulkan/VulkanRenderpass.hpp"
#include "../Vulkan/VulkanOverlayImg.hpp"
#include "../Vulkan/VulkanSwapchain.hpp"
#include "../Vulkan/VulkanPipeline.hpp"
#include "../Vulkan/VulkanFbo.hpp"
#include "../Vulkan/VulkanBuffer.hpp"

class KurisuRootImpl;
class KURISURENDERER_SHARED_EXPORT KurisuSurfaceImpl : public KawaiiSurfaceImpl
{
public:
  KurisuSurfaceImpl(KawaiiSurface *model);
  ~KurisuSurfaceImpl();

  VulkanBaseRenderpass& getRenderpass(size_t gpu_index) const;
  std::vector<VkFramebuffer> getFramebuffers(size_t gpu_index) const;
  const VulkanTexture& getColorStagingTex(size_t gpu_index);
  const VulkanTexture& getColorFboTex(size_t gpu_index);
  void updateRenderingCommands();
  void waitForRenderFinished();

  uint32_t imgCount();

  bool isReady() const;
  uint32_t getImgIndex() const;

  size_t getActiveGpu() const;

  // KawaiiSurfaceImpl interface
private:
  KurisuRootImpl* getRoot() const;

  QPaintDevice* getPaintDevice() override final;
  void resize(const QSize &sz) override final;

  void startRendering() override final;
  void finishRendering() override final;

  void invalidate() override final;



  //IMPLEMENT
  std::unique_ptr<VulkanSwapchain> swapchain;
  std::unique_ptr<VulkanRenderpass> renderpass;
  std::unique_ptr<VulkanOverlayImg> overlayTexture;

  //One per gpu
  class SceneRenderer {
    VulkanBaseRenderpass renderpass;
    std::unique_ptr<VulkanFbo> fbo;

    VulkanTexture colorStaging;
    VulkanTexture colorComposeTex;
    const VkDescriptorImageInfo *colorImgDescr;
  public:
    SceneRenderer(VulkanDevice &dev, const QSize &sz, VulkanDevice &composerDev, size_t image_count);
    ~SceneRenderer();

    VkFormat getColorVkFormat() const;

    const VkDescriptorImageInfo *getColorImgDescriptor();

    VulkanBaseRenderpass& getRenderpass();
    VkFramebuffer getVkFbo() const;

    const VulkanTexture &getColorStagingTex() const;
    const VulkanTexture &getColorFboTex() const;

    void resize(const QSize &sz);
  };

  std::vector<std::unique_ptr<SceneRenderer>> sceneRenderers;

  VkDescriptorPool presentDescriptorPool;
  std::vector<VkDescriptorSet> presentDescriptorSet; //One per GPU
  std::unique_ptr<VulkanPipelineLayout> overlayPipelineLayout;
  std::unique_ptr<VulkanPipeline> overlayPipeline;
  std::unique_ptr<VulkanPipeline> scenePipeline;
  std::unique_ptr<VulkanShader> quadVertShader;
  std::unique_ptr<VulkanShader> quadFragShader;
  std::vector<std::unique_ptr<RenderingCommands>> presentCommands; //One per GPU; Primary command buffer on composer GPU
  std::vector<std::unique_ptr<RenderingCommands>> overlayCommands; //One per GPU; For composer GPU renders overlay, for other -- scene

  size_t currentGpu;

  bool renderingReady;

  void initSwapchain();
  void updateComposeDescriptorSet();
  void writeCommands();
  void forceResize();
};

#endif // KURISUSURFACEIMPL_HPP
