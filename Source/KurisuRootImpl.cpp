#include "KurisuRootImpl.hpp"
#include "Geometry/KurisuMeshInstanceImpl.hpp"
#include "Textures/KurisuFramebufferImpl.hpp"
#include "Surfaces/KurisuSurfaceImpl.hpp"
#include "KurisuBufferHandle.hpp"

#include <Kawaii3D/KawaiiConfig.hpp>
#include <QLoggingCategory>
#include <iostream>

namespace {
  const auto deviceExtensions = std::vector<std::string>
      ({"VK_KHR_swapchain"
        , "VK_KHR_external_semaphore"
        , "VK_KHR_external_memory"
        //, "VK_EXT_external_memory_host"
      #ifdef Q_OS_UNIX
        , "VK_KHR_external_semaphore_fd"
        , "VK_KHR_external_memory_fd"
      #elif defined(Q_OS_WINDOWS)
        , "VK_KHR_external_semaphore_win32"
        , "VK_KHR_external_memory_win32"
      #endif
       });
}

KurisuRootImpl::KurisuRootImpl(KawaiiRoot *model):
  KawaiiRootImpl(model),
  currentSfc(nullptr),
  currentCmd(nullptr),
  current_device(0),
  offscreen(false)
{
  VulkanInstanceFactory instFactory;

  if(KawaiiConfig::getInstance().getDebugLevel() > 1)
    {
      instFactory.extensions = { VK_EXT_DEBUG_UTILS_EXTENSION_NAME };
      instFactory.layers = {"VK_LAYER_KHRONOS_validation", "VK_LAYER_LUNARG_standard_validation"};
      QLoggingCategory::setFilterRules(QStringLiteral("qt.vulkan=true"));
      if(KawaiiConfig::getInstance().getDebugLevel() > 2)
        instFactory.layers.push_back("VK_LAYER_MESA_overlay");
    } else
    QLoggingCategory::setFilterRules(QStringLiteral("qt.vulkan=false"));
  instFactory.extensions << QByteArrayList ({"VK_KHR_external_memory_capabilities", "VK_KHR_external_semaphore_capabilities", "VK_KHR_get_physical_device_properties2"});

  inst = instFactory.create();

  if(!inst)
    throw std::runtime_error("KurisuRootImpl::KurisuRootImpl could not initialize vulkan instance");

  devices = std::make_unique<VulkanDeviceManager>(inst->getVkInstance(), instFactory.layers, deviceExtensions);
  if(devices->allDevices().empty())
    throw std::runtime_error("KurisuRootImpl::KurisuRootImpl could not initialize any vulkan device");

  config = std::make_unique<KurisuConfig>(*devices);

  pipelineLayouts.reserve(deviceCount());
  forallDevices([this] (VulkanDevice &i) {
    if(!i.queueFamilies.transferFamily.has_value())
      throw std::runtime_error("Can not create transfer command pool");

    pipelineLayouts.emplace_back(i.vkDev);
    pipelineLayouts.back().addOnInvalidate([this] {
        if(hasActiveCommands() && (getActiveCommands().hasBindedPipeline() || getActiveCommands().getState() == RenderingCommands::State::Compleate))
          throw std::runtime_error("Pipeline layout invalidated");
      });
  });

  connect(&bufBindings, &KurisuBufferBindings::changedBlockBinding, this, &KurisuRootImpl::onChangedBlockBufBinding);
}

KurisuRootImpl::~KurisuRootImpl()
{
  pipelineLayouts.clear();
  devices.reset();
  inst.reset();
}

VkSurfaceKHR KurisuRootImpl::getSurface(QWindow *wnd)
{
  auto sfc = inst->getSurfaceForWindow(wnd);
  if(sfc == VK_NULL_HANDLE)
    throw std::runtime_error("Can not get VkSurface");
  return sfc;
}

void KurisuRootImpl::forallDevices(const std::function<void(VulkanDevice &)> &func)
{
  for(size_t i = 0; i < devices->allDevices().size(); ++i)
    func(devices->device(i));
}

void KurisuRootImpl::forallDevices(const std::function<void(VulkanDevice &, size_t)> &func)
{
  for(size_t i = 0; i < devices->allDevices().size(); ++i)
    func(devices->device(i), i);
}

VulkanDevice &KurisuRootImpl::getDevice(size_t device_index) const
{
  return devices->device(device_index);
}

uint32_t KurisuRootImpl::deviceCount() const
{
  return devices->allDevices().size();
}

VulkanPipelineLayout& KurisuRootImpl::getPipelineLayout(size_t device_index)
{
  return pipelineLayouts[device_index];
}

void KurisuRootImpl::activateCommands(RenderingCommands &cmd, size_t device_index)
{
  currentCmd = &cmd;
  current_device = device_index;

  if(cmd.getState() == RenderingCommands::State::Compleate) return;

  auto &pipelineLayout = getPipelineLayout(device_index);
  cmd.setDefaultPipelineLayout(&pipelineLayout);

  const auto bindedUserBlocks = bufBindings.bindedBufferBlocksUser();
  std::vector<BufferBinding> bindings;
  bindings.reserve(bindedUserBlocks.size());

  for(const auto &i: bindedUserBlocks)
    {
      const uint32_t realBlock = getGlobUboLocation(std::get<uint32_t>(i)).binding;
      bindings.push_back(BufferBinding {std::get<VkDescriptorType>(i), realBlock, std::get<KurisuBufferHandle*>(i)});
    }

  bindBuffersToDescriptor(std::move(bindings), cmd, device_index);
}

RenderingCommands &KurisuRootImpl::getActiveCommands()
{
  Q_ASSERT(currentCmd);
  return *currentCmd;
}

bool KurisuRootImpl::hasActiveCommands() const
{
  return (currentCmd != nullptr);
}

void KurisuRootImpl::releaseCommands()
{
  currentCmd = nullptr;
}

void KurisuRootImpl::activateSurface(KurisuSurfaceImpl *surface)
{
  currentSfc = surface;
}

KurisuSurfaceImpl *KurisuRootImpl::getActiveSurface()
{
  return currentSfc;
}

void KurisuRootImpl::releaseSurface()
{
  currentSfc = nullptr;
}

std::vector<VkFramebuffer> KurisuRootImpl::getActiveFramebuffers(size_t device_index)
{
  Q_ASSERT(currentSfc);
  if(!isNowOffscreen())
    return currentSfc->getFramebuffers(device_index);
  else
    return currentOffscr->getFbos(device_index);
}

VulkanBaseRenderpass &KurisuRootImpl::getActiveRenderpass(size_t device_index)
{
  Q_ASSERT(currentSfc);
  if(!isNowOffscreen())
    return currentSfc->getRenderpass(device_index);
  else
    return currentOffscr->getRp(device_index);
}

bool KurisuRootImpl::isNowOffscreen() const
{
  return offscreen;
}

void KurisuRootImpl::setOffscreenTarget(KurisuOffscrRenderer *offscrRender)
{
  Q_ASSERT(offscreen);
  currentOffscr = offscrRender;
//  offscreenTasks.push_back(target);
}

size_t KurisuRootImpl::getActiveDevice() const
{
  return current_device;
}

KurisuConfig &KurisuRootImpl::getConfig() const
{
  return *config;
}

KurisuRootImpl::Binding KurisuRootImpl::getTextureLocation(const std::string &textureName, uint32_t blockBinding)
{
  if(blockBinding > KawaiiShader::getUboCount() + 1)
    blockBinding = KawaiiShader::getUboCount() + 1;
  auto el = textureLocations.find({textureName, blockBinding});
  if(el == textureLocations.end())
    {
      uint32_t binding;
      uint32_t set;
      for(size_t i = 0; i < deviceCount(); ++i)
        {
          if(blockBinding == KawaiiShader::getUboCount() + 1)
            set = KawaiiShader::getUboCount();
          else
            set = getPipelineLayout(i).getDescriptorSetLayout(blockBinding).second;
          binding = getPipelineLayout(i).addSamplerBinding(set);
        }
      el = textureLocations.insert({{textureName, blockBinding}, Binding { .binding = binding, .set = set }}).first;
    }

  return el->second;
}

KurisuRootImpl::Binding KurisuRootImpl::getGlobUboLocation(uint32_t number)
{
  auto el = globUboLocations.find(number);
  if(el == globUboLocations.end())
    {
      uint32_t binding; uint32_t set;
      for(size_t i = 0; i < deviceCount(); ++i)
        {
          binding = getPipelineLayout(i).addUboBinding(KawaiiShader::getUboCount());
          set = KawaiiShader::getUboCount();
        }
      el = globUboLocations.insert({number, Binding { .binding = binding, .set = set } }).first;
    }

  return el->second;
}

uint32_t KurisuRootImpl::getDescrSetIndex(uint32_t binding)
{
  return getPipelineLayout(0).getDescriptorSetLayout(binding).second;
}

bool KurisuRootImpl::checkSystem()
{
  const bool dbg_enabled = KawaiiConfig::getInstance().getDebugLevel() > 0;
  auto dbg_prnt = [&dbg_enabled] (const auto &str) {
      if(dbg_enabled)
        std::cerr << str << std::endl;
    };

  VulkanInstanceFactory instFactory;

  if(KawaiiConfig::getInstance().getDebugLevel() > 1)
    {
      instFactory.extensions = { VK_EXT_DEBUG_UTILS_EXTENSION_NAME };
      instFactory.layers = {"VK_LAYER_KHRONOS_validation", "VK_LAYER_LUNARG_standard_validation"};
      QLoggingCategory::setFilterRules(QStringLiteral("qt.vulkan=true"));
      if(KawaiiConfig::getInstance().getDebugLevel() > 2)
        instFactory.layers.push_back("VK_LAYER_MESA_overlay");
    } else
    QLoggingCategory::setFilterRules(QStringLiteral("qt.vulkan=false"));
  instFactory.extensions << QByteArrayList ({"VK_KHR_external_memory_capabilities", "VK_KHR_external_semaphore_capabilities", "VK_KHR_get_physical_device_properties2"});

  std::unique_ptr<VulkanInstance> inst = instFactory.create();

  if(!inst)
    {
      dbg_prnt("KurisuRenderer failed to initialize vulkan instance");
      return false;
    }

  auto devices = std::make_unique<VulkanDeviceManager>(inst->getVkInstance(), instFactory.layers, deviceExtensions);
  if(devices->allDevices().empty())
    {
      dbg_prnt("KurisuRenderer failed to initialize any vulkan device");
      return false;
    }
  for(const auto &i: devices->allDevices())
    if(!i.queueFamilies.transferFamily.has_value())
      {
        dbg_prnt("KurisuRenderer failed to create transfer command pool");
        return false;
      }
  return true;
}

KawaiiBufferHandle *KurisuRootImpl::createBuffer(const void *ptr, size_t n, const std::initializer_list<KawaiiBufferTarget> &availableTargets)
{
  return new KurisuBufferHandle(ptr, n, *this, availableTargets);
}

KawaiiTextureHandle *KurisuRootImpl::createTexture(KawaiiTexture *model)
{
  return new KurisuTextureHandle(*this, model);
}

void KurisuRootImpl::beginOffscreen()
{
  offscreen = true;
}

void KurisuRootImpl::endOffscreen()
{
  offscreen = false;
  currentOffscr = nullptr;
//  for(auto *i: offscreenTasks)
//    i->waitForCompleated();

//  offscreenTasks.clear();
}

void KurisuRootImpl::bindBuffersToDescriptor(std::vector<KurisuRootImpl::BufferBinding> &&buffers, RenderingCommands &cmd, size_t device_index)
{
  if(buffers.empty()) return;

  auto &pipelineLayout = getPipelineLayout(device_index);

  std::vector<VulkanDescriptorPool::BufBinding> bufBindings;
  std::vector<VulkanDescriptorPool::ImgBinding> imgBindings;

  std::pair<VkDescriptorSetLayout, uint32_t> descrSetLayout = {VK_NULL_HANDLE, 0};

  uint64_t name = 0;
  // Multiple buffer bindings are only allowed in global binding (not camera, material, model or surface)
  // In global binding there will be one set of buffers for all command buffers
  if(buffers.size() == 1)
    name = static_cast<uint64_t>(reinterpret_cast<size_t>(buffers.front().buf));

  for(const auto &i: buffers)
    {
      bufBindings.push_back(VulkanDescriptorPool::BufBinding {
                              .buf = &i.buf->buffer(device_index),
                              .descriptorType = i.target,
                              .bindingPoint = i.block
                            });

      auto currentDescrSetLayout = pipelineLayout.getDescriptorSetLayout(i.block);
      Q_ASSERT(!descrSetLayout.first || descrSetLayout == currentDescrSetLayout);
      descrSetLayout = currentDescrSetLayout;

      for(const auto &tex: i.buf->getBindedTextures())
        {
          auto &vkTex = tex.second->getTexture(device_index);

          imgBindings.push_back(VulkanDescriptorPool::ImgBinding {
                                  .descrImg = *vkTex.descriptorInfo(),
                                  .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                                  .bindingPoint = getTextureLocation(tex.first, bufBindings.back().bindingPoint).binding
                                });

          Q_ASSERT(descrSetLayout == pipelineLayout.getDescriptorSetLayout(imgBindings.back().bindingPoint));
        }
    }

  cmd.createDescriptorSet(name, bufBindings, imgBindings, descrSetLayout.first, pipelineLayout.getDescriptorCount(descrSetLayout.second));
  cmd.bindDescriptorSet(name, descrSetLayout.second);
}

void KurisuRootImpl::onChangedBlockBufBinding(KawaiiBufferTarget target, uint32_t block, KurisuBufferHandle *buf)
{
  if(!buf) return;

  VkDescriptorType descrType;
  switch(target)
    {
    case KawaiiBufferTarget::UBO:
      descrType = VkDescriptorType::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
      break;

    case KawaiiBufferTarget::SSBO:
      descrType = VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
      break;

    default: return;
    }

  if(hasActiveCommands())
    bindBuffersToDescriptor({BufferBinding{descrType, block, buf}}, getActiveCommands(), getActiveDevice());
}
