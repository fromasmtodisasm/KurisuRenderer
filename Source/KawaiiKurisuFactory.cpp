#include "KawaiiKurisuFactory.hpp"

#include "KurisuRootImpl.hpp"
#include "Surfaces/KurisuSurfaceImpl.hpp"
#include "KurisuGpuBufImpl.hpp"
#include "Geometry/KurisuMeshImpl.hpp"
#include "Shaders/KurisuShaderImpl.hpp"
#include "Shaders/KurisuProgramImpl.hpp"
#include <KawaiiRenderer/KawaiiBufBindingImpl.hpp>
#include "Geometry/KurisuMeshInstanceImpl.hpp"
#include <KawaiiRenderer/KawaiiMaterialImpl.hpp>
#include <KawaiiRenderer/Shaders/KawaiiSceneImpl.hpp>
#include "KurisuCameraImpl.hpp"
#include <KawaiiRenderer/Textures/KawaiiCubemapImpl.hpp>
#include <KawaiiRenderer/Textures/KawaiiImgImpl.hpp>
#include <KawaiiRenderer/Textures/KawaiiDepthCubemapArrayImpl.hpp>
#include <KawaiiRenderer/Textures/KawaiiDepthTex2dArrayImpl.hpp>
#include "Textures/KurisuFramebufferImpl.hpp"
#include "Textures/KurisuEnvMapImpl.hpp"
#include <KawaiiRenderer/Textures/KawaiiTexBindingImpl.hpp>

#include <QCoreApplication>

KawaiiKurisuFactory *KawaiiKurisuFactory::instance = nullptr;

KawaiiKurisuFactory::KawaiiKurisuFactory()
{
  registerImpl<KawaiiRoot,              KurisuRootImpl>              ();
  registerImpl<KawaiiSurface,           KurisuSurfaceImpl>           ();
  registerImpl<KawaiiGpuBuf,            KurisuGpuBufImpl>            ();
  registerImpl<KawaiiBufBinding,        KawaiiBufBindingImpl>        ();
  registerImpl<KawaiiMesh3D,            KurisuMeshImpl>              ();
  registerImpl<KawaiiProgram,           KurisuProgramImpl>           ();
  registerImpl<KawaiiShader,            KurisuShaderImpl>            ();
  registerImpl<KawaiiMeshInstance,      KurisuMeshInstanceImpl>      ();
  registerImpl<KawaiiMaterial,          KawaiiMaterialImpl>          ();
  registerImpl<KawaiiScene,             KawaiiSceneImpl>             ();
  registerImpl<KawaiiCamera,            KurisuCameraImpl>            ();
  registerImpl<KawaiiCubemap,           KawaiiCubemapImpl>           ();
  registerImpl<KawaiiImage,             KawaiiImgImpl>               ();
  registerImpl<KawaiiDepthCubemapArray, KawaiiDepthCubemapArrayImpl> ();
  registerImpl<KawaiiDepthTex2dArray,   KawaiiDepthTex2dArrayImpl>   ();
  registerImpl<KawaiiEnvMap,            KurisuEnvMapImpl>            ();
  registerImpl<KawaiiFramebuffer,       KurisuFramebufferImpl>       ();
  registerImpl<KawaiiTexBinding,        KawaiiTexBindingImpl>        ();
}

KawaiiImplFactory *KawaiiKurisuFactory::getInstance()
{
  if(!instance)
    createInstance();
  return instance;
}

void KawaiiKurisuFactory::deleteInstance()
{
  if(instance)
    {
      instance->deleteLater();
      instance = nullptr;
    }
}

void KawaiiKurisuFactory::createInstance()
{
  instance = new KawaiiKurisuFactory;
  qAddPostRoutine(&KawaiiKurisuFactory::deleteInstance);
}
