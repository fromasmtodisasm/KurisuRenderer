#ifndef KURISURENDERER_GLOBAL_HPP
#define KURISURENDERER_GLOBAL_HPP

#include <qglobal.h>
#include <glm/mat4x4.hpp>

#if defined(KURISURENDERER_LIBRARY)
#  define KURISURENDERER_SHARED_EXPORT Q_DECL_EXPORT
#else
#  define KURISURENDERER_SHARED_EXPORT Q_DECL_IMPORT
#endif

inline constexpr glm::mat4 vulkanToGlClipCorrectionOffscr(1.0f, 0.0f, 0.0f, 0.0f,
                                                          0.0f, 1.0f, 0.0f, 0.0f,
                                                          0.0f, 0.0f, 0.5f, 0.0f,
                                                          0.0f, 0.0f, 0.5f, 1.0f);

#endif // KURISURENDERER_GLOBAL_HPP
