#ifndef KURISUGPUBUFIMPL_HPP
#define KURISUGPUBUFIMPL_HPP

#include <KawaiiRenderer/KawaiiGpuBufImpl.hpp>
#include "KurisuBufferHandle.hpp"
#include <unordered_map>
#include <qopengl.h>
#include <QPointer>

class KURISURENDERER_SHARED_EXPORT KurisuGpuBufImpl : public KawaiiGpuBufImpl
{
public:
  KurisuGpuBufImpl(KawaiiGpuBuf *model);
  ~KurisuGpuBufImpl();

  // KawaiiGpuBufImpl interface
  void bindTexture(const QString &fieldName, KawaiiTextureImpl *tex) override final;
  void unbindTexture(const QString &fieldName, KawaiiTextureImpl *tex) override final;



  //IMPLEMENT
private:
};

#endif // KURISUGPUBUFIMPL_HPP
