#include "KurisuGpuBufImpl.hpp"
#include "KurisuRootImpl.hpp"
#include <KawaiiRenderer/Textures/KawaiiTextureImpl.hpp>
#include <Kawaii3D/KawaiiConfig.hpp>
#include <cstring>

KurisuGpuBufImpl::KurisuGpuBufImpl(KawaiiGpuBuf *model):
  KawaiiGpuBufImpl(model)
{
}

KurisuGpuBufImpl::~KurisuGpuBufImpl()
{
}

void KurisuGpuBufImpl::bindTexture(const QString &fieldName, KawaiiTextureImpl *tex)
{
  static_cast<KurisuBufferHandle*>(getHandle())->bindTexture(fieldName.toStdString(), static_cast<KurisuTextureHandle*>(tex->getHandle()));
}

void KurisuGpuBufImpl::unbindTexture(const QString &fieldName, KawaiiTextureImpl *tex)
{
  static_cast<KurisuBufferHandle*>(getHandle())->unbindTexture(fieldName.toStdString(), static_cast<KurisuTextureHandle*>(tex->getHandle()));
}
