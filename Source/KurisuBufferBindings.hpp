#ifndef KURISUBUFFERBINDINGS_HPP
#define KURISUBUFFERBINDINGS_HPP

#include <list>
#include <QObject>
#include <unordered_map>
#include <sib_utils/PairHash.hpp>
#include "KurisuBufferHandle.hpp"

class KurisuBufferBindings : public QObject
{
  Q_OBJECT

public:
  explicit KurisuBufferBindings(QObject *parent = nullptr);
  ~KurisuBufferBindings() = default;

  void bindBuffer(KawaiiBufferTarget target, KurisuBufferHandle *buf);
  void unbindBuffer(KawaiiBufferTarget target, KurisuBufferHandle *buf);

  void bindBufferBlock(KawaiiBufferTarget target, uint32_t block, KurisuBufferHandle *buf);
  void unbindBufferBlock(KawaiiBufferTarget target, uint32_t block, KurisuBufferHandle *buf);

  void bindBufferBlockUser(KawaiiBufferTarget target, uint32_t block, KurisuBufferHandle *buf);
  void unbindBufferBlockUser(KawaiiBufferTarget target, uint32_t block, KurisuBufferHandle *buf);

  void unbindBuffer(KurisuBufferHandle *buf);

  KurisuBufferHandle *bindedBuffer(KawaiiBufferTarget target);
  KurisuBufferHandle *bindedBufferBlock(KawaiiBufferTarget target, uint32_t block);
  KurisuBufferHandle *bindedBufferBlockUser(KawaiiBufferTarget target, uint32_t block);

  std::vector<std::tuple<VkDescriptorType, uint32_t, KurisuBufferHandle*>> bindedBufferBlocks();
  std::vector<std::tuple<VkDescriptorType, uint32_t, KurisuBufferHandle*>> bindedBufferBlocksUser();

signals:
  void changedBinding(KawaiiBufferTarget target, KurisuBufferHandle *buf);
  void changedBlockBinding(KawaiiBufferTarget target, uint32_t block, KurisuBufferHandle *buf);
  void changedUserBlockBinding(KawaiiBufferTarget target, uint32_t userBlock, KurisuBufferHandle *buf);

private:
  using PairHash = sib_utils::PairHash<KawaiiBufferTarget, uint32_t>;
  using BindingStack = std::list<KurisuBufferHandle*>;

  std::unordered_map<KawaiiBufferTarget, BindingStack> bindings;
  std::unordered_map<std::pair<KawaiiBufferTarget, uint32_t>, BindingStack, PairHash> blockBindings;
  std::unordered_map<std::pair<KawaiiBufferTarget, uint32_t>, BindingStack, PairHash> userBindings;
};

#endif // KURISUBUFFERBINDINGS_HPP
