#include "KurisuCameraImpl.hpp"
#include "KurisuRootImpl.hpp"
#include "Surfaces/KurisuSurfaceImpl.hpp"
#include <exception>

KurisuCameraImpl::KurisuCameraImpl(KawaiiCamera *model):
  KawaiiCameraImpl(model)
{
}

RenderingCommands &KurisuCameraImpl::getCommands(const KawaiiGpuBuf *sfcUbo, size_t device_index) const
{
  auto *cache = static_cast<CachedRenderpass*>(getCache(sfcUbo));
  if(!cache)
    throw std::invalid_argument("no commands for the surface");

  return *cache->cmd[device_index];
}

void KurisuCameraImpl::updateCache(KawaiiCameraImpl::DrawCache &c, KawaiiGpuBufImpl *sfcUbo, const QRect &viewport)
{
  auto *cache = static_cast<CachedRenderpass*>(&c);
  cache->reset(this);
  cache->draw(this, sfcUbo, viewport);
}

KawaiiCameraImpl::DrawCache* KurisuCameraImpl::createDrawCache(KawaiiGpuBufImpl *sfcUbo, const QRect &viewport)
{
  CachedRenderpass *cache = new CachedRenderpass(root());
  cache->drawFunc = std::bind(&CachedRenderpass::exec, cache, this);
  cache->draw(this, sfcUbo, viewport);

  return cache;
}

KurisuRootImpl *KurisuCameraImpl::root()
{
  return static_cast<KurisuRootImpl*>(getModel()->getScene()->getRoot()->getRendererImpl());
}

KurisuCameraImpl::CachedRenderpass::CachedRenderpass(KurisuRootImpl *root):
  cmd(root->deviceCount())
{
  for(size_t i = 0; i < cmd.size(); ++i)
    cmd[i] = std::make_unique<RenderingCommands>(root->getDevice(i), root->getActiveSurface()->imgCount(), (root->isNowOffscreen() || i != root->getConfig().getComposerGpu()));
}

void KurisuCameraImpl::CachedRenderpass::reset(KurisuCameraImpl *cam)
{
  bool offscr = cam->root()->isNowOffscreen();
  auto sfc = cam->root()->getActiveSurface();
  if(!offscr)
    sfc->waitForRenderFinished();

  for(const auto &i: cmd)
    i->reset();

  if(!offscr)
    sfc->updateRenderingCommands();
}

void KurisuCameraImpl::CachedRenderpass::draw(KurisuCameraImpl *cam, KawaiiGpuBufImpl *sfcUbo, const QRect &viewport)
{
  if(!cam->root()->getActiveSurface()->isReady())
    {
      dirty = true;
      return;
    }

  for(size_t i = 0; i < cmd.size(); ++i)
    {
      auto &rp = cam->root()->getActiveRenderpass(i);
      const auto framebuffers = cam->root()->getActiveFramebuffers(i);

      cmd[i]->setViewport(viewport);
      cmd[i]->startRecording(rp.getRenderpass(), framebuffers, rp.getClearValues(), VK_SUBPASS_CONTENTS_INLINE, 0);

      cam->root()->activateCommands(*cmd[i], i);
      try {
        cam->drawScene(sfcUbo);
        dirty = false;
      } catch(...)
      { dirty = true; }

      if(cmd[i]->isPrimary())
        {
          cmd[i]->endRenderpass();
          if(!cam->root()->isNowOffscreen())
            {
              auto *sfc = cam->root()->getActiveSurface();
              cmd[i]->copyTexture(sfc->getColorFboTex(i), sfc->getColorStagingTex(i), VK_IMAGE_ASPECT_COLOR_BIT);
            }
        }
      cmd[i]->finishRecording();
    }
  cam->root()->releaseCommands();

  this->viewport = viewport;
}

void KurisuCameraImpl::CachedRenderpass::exec(KurisuCameraImpl *cam)
{
  if(!cam->root()->getActiveSurface()->isReady())
    {
      dirty = true;
      return;
    }

  const uint32_t imgIndex = cam->root()->getActiveSurface()->getImgIndex();
  const size_t i = cam->root()->getActiveSurface()->getActiveGpu();
  if(cmd[i]->isPrimary())
    {
      auto &rp = cam->root()->getActiveRenderpass(i);
      rp.exec(*cmd[i], imgIndex);
    }
}
