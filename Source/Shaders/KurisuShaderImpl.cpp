#include "KurisuShaderImpl.hpp"
#include <KurisuRootImpl.hpp>
#include <Kawaii3D/KawaiiConfig.hpp>
#include <QRegularExpression>
#include "Vulkan/GlslCompiler.hpp"

KurisuShaderImpl::KurisuShaderImpl(KawaiiShader *model):
  KawaiiShaderImpl(model),
  modelObj(model),
  kind(GlslCompiler::kind(model->getType())),
  spirv_dirty(true)
{
  initResource();

  switch(model->getType())
    {
    case KawaiiShaderType::Fragment:
      stage = VkShaderStageFlagBits::VK_SHADER_STAGE_FRAGMENT_BIT;
      break;
    case KawaiiShaderType::TesselationControll:
      stage = VkShaderStageFlagBits::VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
      break;
    case KawaiiShaderType::TesselationEvaluion:
      stage = VkShaderStageFlagBits::VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
      break;
    case KawaiiShaderType::Vertex:
      stage = VkShaderStageFlagBits::VK_SHADER_STAGE_VERTEX_BIT;
      break;
    case KawaiiShaderType::Geometry:
      stage = VkShaderStageFlagBits::VK_SHADER_STAGE_GEOMETRY_BIT;
      break;
    case KawaiiShaderType::Compute:
      stage = VkShaderStageFlagBits::VK_SHADER_STAGE_COMPUTE_BIT;
      break;
    default: Q_UNREACHABLE();
    }
}

KurisuShaderImpl::~KurisuShaderImpl()
{
  preDestruct();
}

KawaiiShaderType KurisuShaderImpl::getShaderType() const
{
  return modelObj->getType();
}

QString KurisuShaderImpl::getModuleFName() const
{
  auto result = modelObj->getModuleName();
  switch(modelObj->getType())
    {
    case KawaiiShaderType::Fragment:
      result += ".frag.glsl";
      break;
    case KawaiiShaderType::TesselationControll:
      result += "..tesc.glsl";
      break;
    case KawaiiShaderType::TesselationEvaluion:
      result += ".tese.glsl";
      break;
    case KawaiiShaderType::Vertex:
      result += ".vert.glsl";
      break;
    case KawaiiShaderType::Geometry:
      result += ".geom.glsl";
      break;
    case KawaiiShaderType::Compute:
      result += ".comp.glsl";
      break;
    default: Q_UNREACHABLE();
    }
  return result;
}

std::unique_ptr<glslang::TShader> KurisuShaderImpl::compile() const
{
  return GlslCompiler().compileGlsl(glsl, kind, modelObj->getModuleName().toStdString());
}

const std::vector<uint32_t> &KurisuShaderImpl::getLinkableSpirv()
{
  if(spirv_dirty)
    {
      spirv = GlslCompiler().glslModuleToSpirv(glsl, kind, modelObj->getModuleName().toStdString());
      spirv_dirty = false;
    }

  return spirv;
}

void KurisuShaderImpl::createShader()
{
  recompile();
}

void KurisuShaderImpl::deleteShader()
{
  glsl.clear();
}

void KurisuShaderImpl::recompile()
{
  glsl = preprocessGlsl(modelObj->getCode()).toStdString();
  spirv_dirty = true;
}

QString KurisuShaderImpl::preprocessGlsl(const QString &code)
{
  QString glsl = code;

  QMap<int, QString> injections;

  auto injectTextureHandles = [this, &injections](int blockEnd, const QHash<QString, QString> &textures, uint32_t blockBinding) {
      if(textures.isEmpty())
        return;

      QString global;

      const QString globTerm = QStringLiteral("\nlayout(binding = %3, set = %4) uniform %2 %1;\n");
      for(auto i = textures.begin(); i != textures.end(); ++i)
        {
          auto components = i->split(' ');
          auto texLocation = rootImpl()->getTextureLocation(components.last().toStdString(), blockBinding);
          global += globTerm.arg(components.last(), components.first(), QString::number(texLocation.binding), QString::number(texLocation.set));
        }
      injections.insert(blockEnd+1, global);
    };

  injectTextureHandles(modelObj->getCameraBlockEnd(), modelObj->getCameraTextures(), KawaiiShader::getCameraUboLocation());
  injectTextureHandles(modelObj->getSurfaceBlockEnd(), modelObj->getSurfaceTextures(), KawaiiShader::getSurfaceUboLocation());
  injectTextureHandles(modelObj->getMaterialBlockEnd(), modelObj->getMaterialTextures(), KawaiiShader::getMaterialUboLocation());
  injectTextureHandles(modelObj->getModelBlockEnd(), modelObj->getModelTextures(), KawaiiShader::getModelUboLocation());
  injectTextureHandles(-1, modelObj->getGlobalTextures(), KawaiiShader::getUboCount() + 1);

  for(auto i = injections.end()-1; i != injections.begin()-1; --i)
    glsl.insert(i.key(), i.value());

  static QRegularExpression regBinding(QStringLiteral("binding = (\\d+)\\)"));

  auto i = regBinding.match(glsl);
  while(i.hasMatch())
    {
      const auto captured = i.captured(1);
      const auto descrSetIndex = rootImpl()->getDescrSetIndex(captured.toUInt());

      glsl.replace(i.capturedStart(), i.capturedLength(), QStringLiteral("binding = %1, set = %2)").arg(captured).arg(descrSetIndex));
      i = regBinding.match(glsl);
    }

  static QRegularExpression regVersion(QStringLiteral("#version\\s+\\d+\\s+core\\s*"));
  auto versionMatch = regVersion.match(glsl);
  if(!versionMatch.hasMatch())
    glsl = QStringLiteral("#version 450 core\n") + glsl;

  static QRegularExpression regGlobalUbo(QStringLiteral("GLOBAL_UBO_BINDING_(\\d+)"));
  i = regGlobalUbo.match(glsl);
  while(i.hasMatch())
    {
      const auto uboLocation = rootImpl()->getGlobUboLocation(i.captured(1).toUInt());

      glsl.replace(i.capturedStart(), i.capturedLength(), QStringLiteral("%1, set = %2").arg(uboLocation.binding).arg(uboLocation.set) );
      i = regGlobalUbo.match(glsl);
    }

  glsl.replace("gl_InstanceID", "gl_InstanceIndex");
  glsl.replace("gl_VertexID", "gl_VertexIndex");
  return glsl;
}

KurisuRootImpl *KurisuShaderImpl::rootImpl() const
{
  return static_cast<KurisuRootImpl*>(getRoot());
}
