#ifndef KURISUCONFIG_HPP
#define KURISUCONFIG_HPP

#include <memory>
#include <vector>
#include <QReadWriteLock>
#include "Vulkan/VulkanDeviceManager.hpp"

class KurisuConfig
{
public:
  KurisuConfig(const VulkanDeviceManager &devices);
  ~KurisuConfig();

  uint64_t getComposerGpu() const;
  static uint64_t getMaxCachedStagingBuffers();
  static bool isGlInteropAllowed();


  //IMPLEMENT
private:
  uint64_t composerGpu;

  struct StaticConfig {
    uint64_t preferredComposerGpu;
    uint64_t maxCachedStagingBuffers;
    bool glInterop;

    StaticConfig(uint64_t composerGpu, uint64_t maxCachedStagingBuffers, bool glInterop);
  };

  static StaticConfig sharedCfg;
};

#endif // KURISUCONFIG_HPP
