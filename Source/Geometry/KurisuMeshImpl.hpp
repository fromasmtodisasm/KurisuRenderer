#ifndef KURISUMESHIMPL_HPP
#define KURISUMESHIMPL_HPP

#include <KawaiiRenderer/Geometry/KawaiiMeshImpl.hpp>
#include "../KurisuRenderer_global.hpp"

class KurisuRootImpl;
class VulkanBuffer;
class KURISURENDERER_SHARED_EXPORT KurisuMeshImpl : public KawaiiMeshImpl
{
public:
  KurisuMeshImpl(KawaiiMesh3D *model);
  ~KurisuMeshImpl();

  void drawIndirect(std::vector<VulkanBuffer> &indirectDrawBuf);

  // KawaiiMeshImpl interface
public:
  void draw() const override final;
  void drawInstanced(size_t instances) const override final;

private:
  void createVertexArray() override final;
  void deleteVertexArray() override final;
  void bindBaseAttr() override final;
  void bindIndices() override final;



  //IMPLEMENT
private:
  KurisuRootImpl* root() const;
};

#endif // KURISUMESHIMPL_HPP
